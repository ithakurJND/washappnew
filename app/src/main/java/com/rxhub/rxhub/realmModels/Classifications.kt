package com.rxhub.rxhub.realmModels

import io.realm.RealmObject

open class Classifications(
    var classification: String?,
    var language: Int?,
    var medicine_id: Int?
) : RealmObject()
{
    constructor():this(null,0,0)
}