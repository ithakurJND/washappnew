package com.rxhub.rxhub.realmModels

import io.realm.RealmObject

open class Result(var name: String?, var id: Int?) : RealmObject() {
    constructor() : this( null, 0){}
}