package com.rxhub.rxhub.realmModels

import io.realm.RealmObject

 open class Instructions(
    var designation_id: Int?,
    var instruction: String?,
    var language: Int?,
    var medicine_id: Int?
):RealmObject() {
     constructor() : this(0,null,0,0) {}
}