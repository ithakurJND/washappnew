package com.rxhub.rxhub.realmModels


import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.RealmClass

@RealmClass
open class AllData(
    var brand_name: String? = null,
    var category: String? = null,
    var classification: RealmList<Classifications> = RealmList(),
    var designation_id: Int? = 0,
    var drug_form: String? = null,
    var generic_name: String? = null,
    var id: Int? = 0,
    var med_system_name: String? = null,
    var image: String? = null,
    var indication_name: String? = null,
    var instruction: RealmList<Instructions> = RealmList(),
    var language: Int? = 0,
    var medicine_id: Int? = 0,
    var name: String? = null,
    var drug_form_name: String? = null
) : RealmObject()