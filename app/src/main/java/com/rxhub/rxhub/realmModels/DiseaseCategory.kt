package com.rxhub.rxhub.realmModels

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.RealmClass

@RealmClass
open class DiseaseCategory(
    var count: Int? = 0,
    var result: RealmList<Result> = RealmList()
) : RealmObject() {

}