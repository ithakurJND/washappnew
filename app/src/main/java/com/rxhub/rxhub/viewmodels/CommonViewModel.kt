package com.rxhub.rxhub.viewmodels

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.rxhub.rxhub.realmModels.*
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.BasicResponse
import com.rxhub.rxhub.webServices.model.Message
import com.rxhub.rxhub.webServices.model.SyncDbData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CommonViewModel : ViewModel() {
    var allLiveDataResponse = MutableLiveData<List<AllData>>()
    var genericNameLiveData = MutableLiveData<GenericName>()
    var systemCategoryLiveData = MutableLiveData<SystemCategory>()
    var diseaseCategoryLiveData = MutableLiveData<DiseaseCategory>()
    var brandCategoryLiveData = MutableLiveData<BrandCategory>()
    var syncDbLiveData = MutableLiveData<ArrayList<SyncDbData>>()
    var basicResponseLiveData = MutableLiveData<BasicResponse>()
    lateinit var context: Context
    fun getValue(context: Context) {
        this.context = context
    }

    fun getAllData() {
        CommonMethods.showProgress(context)
        RetrofitClient.getRetrofit().getAllData().enqueue(object : Callback<List<AllData>> {
            override fun onFailure(call: Call<List<AllData>>, t: Throwable) {
                CommonMethods.dismissProgress()
                Toast.makeText(
                    context,
                    "There is some issue in sync please again with good internet connection.",
                    Toast.LENGTH_SHORT
                ).show()
                Log.i("AllData", "failure ${t.message}")
            }

            override fun onResponse(call: Call<List<AllData>>, response: Response<List<AllData>>) {

                if (response.isSuccessful) {
                    Log.i("AllData", "success ${response.body()}")
                    allLiveDataResponse.value = response.body()
                } else {
                    CommonMethods.dismissProgress()
                    Toast.makeText(
                        context,
                        "There is some issue in sync please again with good internet connection.",
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.i(
                        "AllData", " error " +
                                "${response.errorBody()}"
                    )
                }
            }
        })
    }

    fun getGenericData() {
        //CommonMethods.showProgress(context)
        val hashMap = HashMap<String, String>()
        hashMap["language_id"] = "1"
        RetrofitClient.getRetrofit().getGenericDrugs(hashMap)
            .enqueue(object : Callback<GenericName> {
                override fun onFailure(call: Call<GenericName>, t: Throwable) {
                    Log.i("GenericData", "failure ${t.message}")
                    Toast.makeText(
                        context,
                        "There is some issue in sync please again with good internet connection.",
                        Toast.LENGTH_SHORT
                    ).show()
                   CommonMethods.dismissProgress()
                }

                override fun onResponse(call: Call<GenericName>, response: Response<GenericName>) {
                    if (response.isSuccessful) {
                        Log.i("GenericData", "success ${response.body()}")
                        genericNameLiveData.value = response.body()
                    } else {
                        Toast.makeText(
                            context,
                            "There is some issue in sync please again with good internet connection.",
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.i(
                            "GenericData", " error " +
                                    "${response.errorBody()}"
                        )
                    }
                }
            })
    }

    fun getSystemData() {
        //CommonMethods.showProgress(context)
        val hashMap = HashMap<String, String>()
        hashMap["language_id"] = "1"
        RetrofitClient.getRetrofit().getSystemCategory(hashMap)
            .enqueue(object : Callback<SystemCategory> {
                override fun onFailure(call: Call<SystemCategory>, t: Throwable) {
                    Toast.makeText(
                        context,
                        "There is some issue in sync please again with good internet connection.",
                        Toast.LENGTH_SHORT
                    ).show()
                    CommonMethods.dismissProgress()
                    Log.i("SystemCategory", "failure ${t.message}")
                }

                override fun onResponse(
                    call: Call<SystemCategory>,
                    response: Response<SystemCategory>
                ) {
                    if (response.isSuccessful) {
                        Log.i("SystemCategory", "success ${response.body()}")
                        systemCategoryLiveData.value = response.body()
                    } else {
                        Toast.makeText(
                            context,
                            "There is some issue in sync please again with good internet connection.",
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.i(
                            "SystemCategory", " error " +
                                    "${response.errorBody()}"
                        )
                    }
                }
            })
    }

    fun getByIndication() {
        //CommonMethods.showProgress(context)
        val hashMap = HashMap<String, String>()
        hashMap["language_id"] = "1"
        RetrofitClient.getRetrofit().getDiseaseCategory(hashMap)
            .enqueue(object : Callback<DiseaseCategory> {
                override fun onFailure(call: Call<DiseaseCategory>, t: Throwable) {
                    Log.i("DiseaseCategory", "failure ${t.message}")
                    Toast.makeText(
                        context,
                        "There is some issue in sync please again with good internet connection.",
                        Toast.LENGTH_SHORT
                    ).show()
                    CommonMethods.dismissProgress()
                }

                override fun onResponse(
                    call: Call<DiseaseCategory>,
                    response: Response<DiseaseCategory>
                ) {
                    if (response.isSuccessful) {
                        Log.i("DiseaseCategory", "success ${response.body()}")
                        diseaseCategoryLiveData.value = response.body()
                    } else {
                        Toast.makeText(
                            context,
                            "There is some issue in sync please again with good internet connection.",
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.i(
                            "DiseaseCategory", " error " +
                                    "${response.errorBody()}"
                        )
                    }
                }
            })
    }

    fun getByBrand() {
       // CommonMethods.showProgress(context)
        val hashMap = HashMap<String, String>()
        hashMap["language_id"] = "1"
        RetrofitClient.getRetrofit().getBrandCategory(hashMap)
            .enqueue(object : Callback<BrandCategory> {
                override fun onFailure(call: Call<BrandCategory>, t: Throwable) {
                    Log.i("BrandCategory", "failure ${t.message}")
                    Toast.makeText(
                        context,
                        "There is some issue in sync please again with good internet connection.",
                        Toast.LENGTH_SHORT
                    ).show()
                    CommonMethods.dismissProgress()
                }

                override fun onResponse(
                    call: Call<BrandCategory>,
                    response: Response<BrandCategory>
                ) {
                    if (response.isSuccessful) {
                        Log.i("BrandCategory", "success ${response.body()}")
                        brandCategoryLiveData.value = response.body()
                    } else {
                        Toast.makeText(
                            context,
                            "There is some issue in sync please again with good internet connection.",
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.i(
                            "BrandCategory", " error " +
                                    "${response.errorBody()}"
                        )
                    }
                }
            })
    }

    fun getSyncDbData() {
        RetrofitClient.getRetrofit().getSyncData()
            .enqueue(object : Callback<ArrayList<SyncDbData>> {
                override fun onFailure(call: Call<ArrayList<SyncDbData>>, t: Throwable) {
                    Log.i("SyncData", "failure ${t.message}")
                    CommonMethods.dismissProgress()
                }

                override fun onResponse(
                    call: Call<ArrayList<SyncDbData>>,
                    response: Response<ArrayList<SyncDbData>>
                ) {
                    if (response.isSuccessful) {
                        Log.i("SyncData", "success ${response.body()}")
                        syncDbLiveData.value = response.body()
                    } else {
                        Log.i(
                            "SyncData", " error " +
                                    "${response.errorBody()}"
                        )
                    }
                }
            })
    }

    fun postForHistory(id: Int?) {
        RetrofitClient.getRetrofit().postForHistory(id!!)
            .enqueue(object : Callback<Message> {
                override fun onFailure(call: Call<Message>, t: Throwable) {
                    Log.i("postForHistory", "failure ${t.message}")
                }

                override fun onResponse(
                    call: Call<Message>,
                    response: Response<Message>
                ) {
                    if (response.isSuccessful) {
                        Log.i("postForHistory", "success ${response.body()}")
                    } else {
                        Log.i(
                            "postForHistory", " error " +
                                    "${response.errorBody()}"
                        )
                    }
                }
            })
    }

    fun setHistoryStatus(status: Int) {
        CommonMethods.showProgress(context)
        val historyStatus = HashMap<String, Int>()
        historyStatus["is_history_enable"] = status
        RetrofitClient.getRetrofit().setHistoryStatus(historyStatus)
            .enqueue(object : Callback<BasicResponse> {
                override fun onFailure(call: Call<BasicResponse>, t: Throwable) {
                    CommonMethods.dismissProgress()
                    if (CommonMethods.phoneIsOnline(context)) {
                        CommonMethods.toast(context, t.message!!)
                    } else {
                        CommonMethods.toast(context, "Internet connection not available")
                    }
                }

                override fun onResponse(
                    call: Call<BasicResponse>,
                    response: Response<BasicResponse>
                ) {
                    CommonMethods.dismissProgress()
                    if (response.isSuccessful && response.body() != null) {
                        Toast.makeText(context, "${response.body()!!.message}", Toast.LENGTH_LONG)
                            .show()
                        basicResponseLiveData.value = response.body()
                    } else {
                        try {
                            CommonMethods.getErrorMessage(response.errorBody()!!)
                        } catch (e: Exception) {
                        }

                    }
                }
            })
    }

}