package com.rxhub.rxhub.activities

import android.app.AlertDialog
import android.content.*
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentTransaction
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.fragments.*
import com.rxhub.rxhub.utils.*
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import com.rxhub.rxhub.webServices.model.LogOut
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_activity_main_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/*
* This class is used as Home Activity for App
*
* create by GTB & work started on 14/04/2020
 */
class MainActivity : AppCompatActivity(), View.OnClickListener {
    var backPressed = true
    private lateinit var realm: Realm
    lateinit var fragmentTransaction: FragmentTransaction


    private val isOnline = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (!CommonMethods.phoneIsOnline(this@MainActivity))
                tvNetStatus.text = "Offline"
            else
                tvNetStatus.text = "Online"
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        window.decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        super.onCreate(savedInstanceState)
        getUserData()
        setContentView(R.layout.activity_main)
        if (!CommonMethods.phoneIsOnline(this))
            tvNetStatus.text = "Offline"
        else
            tvNetStatus.text = "Online"
        realm = Realm.getDefaultInstance()
        if (intent.hasExtra("medicineId")) {
            Log.i("splacch", " main" + intent.getStringExtra("medicineId"))
            if (intent.getStringExtra("medicineId") != null) {
                val id = intent.getStringExtra("medicineId")
                ApplicationGlobal.preferenceManager.setMedicineId(id!!)
                replaceFragmentNavigationView(DrugsDetailsFragment())
            }
        }

        viewPager.offscreenPageLimit = 2
        viewPager.currentItem = 0
        val adapter = ViewPagerAdapter(supportFragmentManager)
        viewPager.adapter = adapter
        //set cardView background here
        cardViewHome.setBackgroundResource(R.drawable.home_tab_border)
        //these set home tab selected
        ivHome.isSelected = true
        tvHome.isSelected = true
        //all click Listener here
        home.setOnClickListener(this)
        inquiry.setOnClickListener(this)
        faqs.setOnClickListener(this)
        settings.setOnClickListener(this)
//        tvSelectLanguage.setOnClickListener(this)

        registerReceiver(isOnline, IntentFilter("isOnline"))
        profile.setOnClickListener(this)
        about.setOnClickListener(this)
        howWorks.setOnClickListener(this)
        termsUse.setOnClickListener(this)
        disclaimer.setOnClickListener(this)
        faqsMenu.setOnClickListener(this)
        generalInfo.setOnClickListener(this)
        medicineHistory.setOnClickListener(this)
        logOut.setOnClickListener(this)
        headerCardview.setOnClickListener(this)
        llRate.setOnClickListener(this)
        llShare.setOnClickListener(this)
        llSnyc.setOnClickListener(this)

        //if is language changed then restart activity to modify UI changes
        if (ApplicationGlobal.isLanguageChanged) {
            ApplicationGlobal.isLanguageChanged = false
            if (ApplicationGlobal.preferenceManager.isEnglishSelected())
                Localization.applyLanguage(this, "en_US")
            else Localization.applyLanguage(this, "ar_AE")
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
        //set Language Text
//        setLanguageText()

        //this popup window for show language list and set the languages
//        listPopupWindow = ListPopupWindow(this)
//        listPopupWindow.setAdapter(
//            ArrayAdapter(
//                this,
//                android.R.layout.simple_dropdown_item_1line,
//                resources.getStringArray(R.array.language)
//            )
//        )
//        listPopupWindow.anchorView = tvSelectLanguage
//        listPopupWindow.isModal = true
//        listPopupWindow.setOnItemClickListener { parent, view, position, id ->
//            tvSelectLanguage.setText(resources.getStringArray(R.array.language)[position])
////            ApplicationGlobal.preferenceManager.setLanguageChanged(true)
//            ApplicationGlobal.isLanguageChanged = true
//            when (position) {
//                0 -> {
////                    ApplicationGlobal.english = 1
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.ENGLISH)
////                    Localization.applyLanguage(this, "en_US")
////                    ApplicationGlobal.preferenceManager.setEnglishSelected(true, this)
////                    CommonMethods.toast(this, "English")
////                    recreate()
////                    drawer_layout.closeDrawer(GravityCompat.START)
//                }
//                1 -> {
////                    ApplicationGlobal.english = 2
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.ARABIC)
////                    Localization.applyLanguage(this, "ar_AE")
////                    ApplicationGlobal.preferenceManager.setEnglishSelected(false, this)
////                    CommonMethods.toast(this, "Arabic")
////                    recreate()
////                    drawer_layout.closeDrawer(GravityCompat.START)
//                }
//                2 -> {
////                    ur-PK
////                    ApplicationGlobal.english = 3
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.URDU)
////                    drawer_layout.closeDrawer(GravityCompat.START)
//                }
//                3 -> {
////                    hi-IN
////                    ApplicationGlobal.english = 4
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.HINDI)
////                    drawer_layout.closeDrawer(GravityCompat.START)
//                }
//            }
//            listPopupWindow.dismiss()
//        }
    }

    //all Click Listeren set method
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.home -> {
                allTabUnSelected()
                viewPager.currentItem = 0
                ivHome.isSelected = true
                tvHome.isSelected = true
            }
            R.id.inquiry -> {
                allTabUnSelected()
                viewPager.currentItem = 1
                ivInquiry.isSelected = true
                tvInquiryHome.isSelected = true
            }
            R.id.faqs -> {
                allTabUnSelected()
                viewPager.currentItem = 2
                ivFaqs.isSelected = true
                tvFaqs.isSelected = true
            }
            R.id.settings -> {
                allTabUnSelected()
                viewPager.currentItem = 3
                ivSettings.isSelected = true
                tvSettings.isSelected = true
            }
            R.id.about -> {
                replaceFragmentNavigationView(AboutUsFragment("Main", Constant.ABOUT))
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.howWorks -> {
//                replaceFragmentNavigationView(
//                    HowItWorksFragment()
//                )
                replaceFragmentNavigationView(AboutUsFragment("Main", Constant.WORK))
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.termsUse -> {
//                replaceFragmentNavigationView(
//                    TermsOfUseFragment("Menu")
//                )
                replaceFragmentNavigationView(AboutUsFragment("Main", Constant.TERMS_OF_USE))
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.disclaimer -> {
                // replaceFragmentNavigationView(DisclaimerFragment())
                replaceFragmentNavigationView(AboutUsFragment("Main", Constant.DISCLAIMER))
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.faqsMenu -> {
                val fragment = supportFragmentManager.findFragmentById(R.id.coordinatorLayout)
                if (fragment != null) {
                    fragmentTransaction = supportFragmentManager.beginTransaction()
                    fragmentTransaction.remove(fragment)
                        .commit()
                }
                faqs.performClick()
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.generalInfo -> {
//                replaceFragmentNavigationView(
//                    GeneralInstructionFragment("Menu")
//                )
                replaceFragmentNavigationView(AboutUsFragment("Main", Constant.INSTRUCTIONS))
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.profile -> {
                drawer_layout.closeDrawer(GravityCompat.START)
                Handler().postDelayed({
                    startActivity(
                        Intent(this, SettingContainerActivity::class.java)
                            .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .putExtra("SettingScreen", "myProfile")
                    )
                }, 250)
//                replaceFragmentNavigationView(MyProfileFragment(), supportFragmentManager)
//                Handler().postDelayed({ drawer_layout.closeDrawer(GravityCompat.START) }, 200)
            }
            R.id.medicineHistory -> {
                replaceFragmentNavigationView(MedicineSearchHistoryFragment())
                drawer_layout.closeDrawer(GravityCompat.START)
            }
            R.id.llRate -> {
                rateApp()
            }
            R.id.llShare -> {
                shareApp()
            }
            R.id.llSnyc -> {
                if (CommonMethods.phoneIsOnline(this)) {
                    tvNetStatus.text = "Online"
                    sendBroadcast(Intent("sync"))
                    drawer_layout.closeDrawer(GravityCompat.START)
                } else {
                    Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show()
                    tvNetStatus.text = "Offline"
                }
            }
            R.id.logOut -> {
                val dialogBuilder = AlertDialog.Builder(this)
                dialogBuilder.setMessage("Are you sure you want to logout?")
                    .setCancelable(false)
                    .setPositiveButton("Logout", DialogInterface.OnClickListener { dialog, id ->
                        CommonMethods.showProgress(this)
                        RetrofitClient.getRetrofit().logout()
                            .enqueue(object : Callback<LogOut> {
                                override fun onFailure(call: Call<LogOut>, t: Throwable) {
                                    CommonMethods.dismissProgress()
                                    if (CommonMethods.phoneIsOnline(this@MainActivity)) {
                                        CommonMethods.toast(this@MainActivity, t.message!!)
                                    } else {
                                        CommonMethods.toast(
                                            this@MainActivity,
                                            "Internet connection not available"
                                        )
                                    }
                                }

                                override fun onResponse(
                                    call: Call<LogOut>,
                                    response: Response<LogOut>
                                ) {
                                    CommonMethods.dismissProgress()
                                    if (response.isSuccessful) {
                                        CommonMethods.toast(
                                            this@MainActivity,
                                            response.body()!!.message.toString()
                                        )
                                        ApplicationGlobal.preferenceManager!!.logOut()
                                        realm.executeTransaction { realm.deleteAll() }
                                        val intent =
                                            Intent(this@MainActivity, SplashActivity::class.java)
                                        intent.putExtra("Logout", "logout")
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                                        startActivity(intent)
                                        finish()
                                    } else {
                                        CommonMethods.getErrorMessageAndSessionExpire(
                                            this@MainActivity,
                                            response.errorBody()!!
                                        )
//                                CommonMethods.toast(
//                                    this@MainActivity,
//                                    CommonMethods.getErrorMessageError(response.errorBody()!!)!!
//                                )
//                                        try {
//                                            val jObjError =
//                                                JSONObject(response.errorBody()!!.string())
//                                            Toast.makeText(
//                                                this@MainActivity,
//                                                jObjError.getJSONObject("error")
//                                                    .getString("message"),
//                                                Toast.LENGTH_LONG
//                                            ).show()
//                                        } catch (e: java.lang.Exception) {
//                                            Toast.makeText(
//                                                this@MainActivity,
//                                                e.message,
//                                                Toast.LENGTH_LONG
//                                            )
//                                                .show()
//                                        }
                                    }
                                }
                            })
                    })
                    .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                        dialog.cancel()
                    })
                val alert = dialogBuilder.create()
                alert.show()

            }
            R.id.headerCardview -> {
                drawer_layout.closeDrawer(GravityCompat.START)
                Handler().postDelayed({
                    startActivity(
                        Intent(this, SettingContainerActivity::class.java)
                            .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .putExtra("SettingScreen", "myProfile")
                    )
                }, 230)

//                replaceFragmentNavigationView(MyProfileFragment(), supportFragmentManager)
//                Handler().postDelayed({ drawer_layout.closeDrawer(GravityCompat.START) }, 200)
            }
//            R.id.tvSelectLanguage -> {
//                listPopupWindow.show()
//            }
        }
    }

    //view pager Adapter Class
    inner class ViewPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(
        fragmentManager,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
    ) {
        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return HomeFragment()
                1 -> return InquiryFragment()
                2 -> return FAQsFragment()
                else -> return SettingsFragment()
            }
        }

        override fun getCount(): Int {
            return 4
        }
    }

    //in this method set all tab unSelected
    fun allTabUnSelected() {
        ivHome.isSelected = false
        tvHome.isSelected = false
        ivInquiry.isSelected = false
        tvInquiryHome.isSelected = false
        ivFaqs.isSelected = false
        tvFaqs.isSelected = false
        ivSettings.isSelected = false
        tvSettings.isSelected = false
    }

    //this method handle backPressed button on home screen
    override fun onBackPressed() {
        val fragment = supportFragmentManager.findFragmentById(R.id.coordinatorLayout)
        if (fragment != null) {
            fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.remove(fragment)
                .commit()
        } else if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else if (viewPager.currentItem != 0) {
            home.performClick()
        } else if (backPressed) {
            backPressed = false
            CommonMethods.toast(this, "Press again to exit")
            Handler().postDelayed(Runnable { backPressed = true }, 2000)
        } else super.onBackPressed()
    }

    //this method replaces the fragments in container
    private fun replaceFragmentNavigationView(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.coordinatorLayout, fragment)
            .commit()
    }

    fun closeDrawer() {
        drawer_layout.closeDrawer(GravityCompat.START)
    }

    fun removeFragment() {
        val fragment = supportFragmentManager.findFragmentById(R.id.coordinatorLayout)
        if (fragment != null) {
            fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.remove(fragment)
                .commit()
        }
    }

    fun openDrawer() {
        drawer_layout.openDrawer(GravityCompat.START)
    }

    //this method get the User data and set in side menu
    fun getUserData() {
        RetrofitClient.getRetrofit().getUserData()
            .enqueue(object : Callback<BasicInfoResp> {
                override fun onFailure(call: Call<BasicInfoResp>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<BasicInfoResp>,
                    response: Response<BasicInfoResp>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        ApplicationGlobal.preferenceManager.setBasicDetails(
                            Gson().toJson(response.body()!!)
                        )
                        val res = response.body()
                        try {
                            if (res!!.name != null && res.name!!.isNotEmpty()) {
                                tvUserNameNav.setText(res.name)
                                //  tvUserName.setText(res.name)
                            }
                        } catch (e: Exception) {
                        }
                        if (res!!.email != null) {
                            tvUserEmail.setText(res.email)
                        }
                        if (response.body()!!.profile_pic!!.isNotEmpty()) {
                            if (response.body()!!.profile_pic!!.startsWith("http")) {
                                CommonMethods.setUrlImage(
                                    this@MainActivity,
                                    response.body()!!.profile_pic.toString(),
                                    ivNavProfile
                                )
                            } else {
                                CommonMethods.setServerImage(
                                    this@MainActivity,
                                    response.body()!!.profile_pic.toString(),
                                    ivNavProfile
                                )
                            }
                        } else {
                            GlideApp.with(this@MainActivity)
                                .load(R.drawable.placeholder_large)
                                .circleCrop()
                                .into(ivNavProfile)
                        }
                    } else {

                    }
                }

            })
    }

    override fun attachBaseContext(newBase: Context?) {
        if (!ApplicationGlobal.preferenceManager.isEnglishSelected())
            super.attachBaseContext(Localization.applyLanguage(newBase!!, "ar_AE"))
        else
            super.attachBaseContext(Localization.applyLanguage(newBase!!, "en_US"))
    }

    override fun onResume() {
        super.onResume()
        getUserData()
//        setLanguageText()
        Log.i("asdfasdfasdf", "onResume")
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(isOnline)
    }

    //this method set Language text in textView and call in two places
//    fun setLanguageText() {
//        val list = resources.getStringArray(R.array.language)
//        for (i in list.indices) {
//            tvSelectLanguage.setText(list[ApplicationGlobal.preferenceManager.getLanguageId() - 1])
//        }
//    }
    fun rateApp() {
        val uri: Uri = Uri.parse("market://details?id=$packageName")
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(
            Intent.FLAG_ACTIVITY_NO_HISTORY or
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        )
        try {
            startActivity(goToMarket)
        } catch (e: ActivityNotFoundException) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=$packageName")
                )
            )
        }
    }

    private fun shareApp() {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(
            Intent.EXTRA_TEXT, "Hey, checkout Rxhub app on playstore " +
                    "\nhttps://play.google.com/store/apps/details?id=" + packageName
        )
        sendIntent.type = "text/plain"
        startActivity(sendIntent)
    }
}
