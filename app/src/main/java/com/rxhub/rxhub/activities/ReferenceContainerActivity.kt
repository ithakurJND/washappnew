package com.rxhub.rxhub.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.rxhub.rxhub.R
import com.rxhub.rxhub.fragments.ReferenceFragment
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant

class ReferenceContainerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reference_container)
        CommonMethods.addFragmentToReferenceContainer(
            supportFragmentManager, ReferenceFragment(
                Constant.REFERENCE, 1
            )
        )
    }
}
