package com.rxhub.rxhub.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.rxhub.rxhub.R
import com.rxhub.rxhub.fragments.LogInFragment
import com.rxhub.rxhub.fragments.SplashScreenFragment
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import io.branch.referral.Branch
import org.json.JSONException


/*
* This class is used as Container for Splash Screen and Tutorial fragment
*
* created by GTB on 14/04/2020
*/
class SplashActivity : AppCompatActivity() {
    val obje = SplashScreenFragment()
    override fun onCreate(savedInstanceState: Bundle?) {
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        try {
            val getIntent = intent.getStringExtra("Logout")
            if (getIntent == "logout") {
                CommonMethods.addFragmentToSplashScreen(supportFragmentManager, LogInFragment())
            } else {
                if (intent.data != null) {
                    check()
                    Log.i("splacch", " not empty" + intent.data)
                } else {
                    Log.i("splacch", " empty" + intent.data)
                    CommonMethods.addFragmentToSplashScreen(
                        supportFragmentManager,
                        SplashScreenFragment()
                    )
                }
            }
            Log.i("languageID", "" + ApplicationGlobal.preferenceManager.getLanguageId())
        } catch (e: Exception) {
        }
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        this.intent = intent
    }

    private fun check() {
        val intent = Intent(this, MainActivity::class.java)

        val bundle = Bundle()
        Branch.getInstance().initSession({ referringParams, error ->
            if (error == null) {
                Log.i("BranchCofig", "" + referringParams.toString())
                try {
                    Log.i("adsfaf", "" + referringParams.getString("detailId"))
                    when {
                        referringParams.has("detailId") -> bundle.putString(
                            "medicineId", referringParams.getString("detailId")
                        )
                    }
                    obje.arguments = bundle
                    CommonMethods.addFragmentToSplashScreen(
                        supportFragmentManager,
                        obje
                    )
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } else {
                Log.i("BRANCH SDK", error.message)
            }
        }, this.intent.data, this)
    }
}
