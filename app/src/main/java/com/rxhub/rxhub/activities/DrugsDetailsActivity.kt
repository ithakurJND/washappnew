package com.rxhub.rxhub.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.rxhub.rxhub.R
import com.rxhub.rxhub.fragments.*
import com.rxhub.rxhub.utils.CommonMethods

/*
* This class is used as Container for fragments
*
* create by GTB on 16/04/2020
 */
class DrugsDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drugs_details)
        val getIntent = intent.getStringExtra("whichScreen")
        when {
            getIntent!! == "drugsAToZ" -> {
                CommonMethods.addFragmentInDrugsDetail(
                    DrugsAlphabetFragment(),
                    supportFragmentManager
                )
            }
            getIntent == "symptom" -> {
                CommonMethods.addFragmentInDrugsDetail(
                    SymptomCheckerFragment(),
                    supportFragmentManager
                )
            }
            getIntent == "askQuestion" -> {
                CommonMethods.addFragmentInDrugsDetail(
                    AskQuestionFragment(),
                    supportFragmentManager
                )
            }
            getIntent == "profile" -> {
                CommonMethods.addFragmentInDrugsDetail(
                    WebViewFragment("Profile"),
                    supportFragmentManager
                )
            }
            getIntent == "drugsSystem" -> {
                CommonMethods.addFragmentInDrugsDetail(
                    SystemMedicineFragment(),
                    supportFragmentManager
                )
            }
            getIntent == "Disease" -> {
                CommonMethods.addFragmentInDrugsDetail(
                    IndicationMedicineFragment(),
                    supportFragmentManager
                )
            }
            getIntent == "Brand" -> {
                CommonMethods.addFragmentInDrugsDetail(
                    BrandMedicineFragment(),
                    supportFragmentManager
                )
            }
            getIntent == "Generic" -> {
                CommonMethods.addFragmentInDrugsDetail(
                    GenericMedicineFragment(),
                    supportFragmentManager
                )
            }
            else -> {
                CommonMethods.addFragmentInDrugsDetail(
                    DrugsDetailsFragment(),
                    supportFragmentManager
                )
            }
        }
    }
}
