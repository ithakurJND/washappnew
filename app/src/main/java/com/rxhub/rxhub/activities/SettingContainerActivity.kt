package com.rxhub.rxhub.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.rxhub.rxhub.R
import com.rxhub.rxhub.fragments.*
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant

/*
* This class is used for contain setting fragments click(change password) and contain for
*  Drugs by categories and My profile screen and home search screen for App
*
* create by GTB & work started on 15/04/2020
*/
class SettingContainerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting_container)
        val getIntent = intent.getStringExtra("SettingScreen")
        if (getIntent!!.equals("ChangePassword")) {
            CommonMethods.addFragmentToSetting(supportFragmentManager, ChangePasswordFragment())
        } else if (getIntent.equals("HomeSearch")) {
            CommonMethods.addFragmentToSetting(
                supportFragmentManager,
                HomeSearchFragment()
            )
        } else if (getIntent.equals("System")) {
            CommonMethods.addFragmentToSetting(
                supportFragmentManager,
                DrugsByTypesFragment(getIntent)
            )
        } else if (getIntent.equals("Disease")) {
            CommonMethods.addFragmentToSetting(
                supportFragmentManager,
                DrugsByTypesFragment(getIntent)
            )
        } else if (getIntent.equals("Brand")) {
            CommonMethods.addFragmentToSetting(
                supportFragmentManager,
                DrugsByTypesFragment(getIntent)
            )
        } else if (getIntent.equals("Generic")) {
            CommonMethods.addFragmentToSetting(
                supportFragmentManager,
                DrugsByTypesFragment(getIntent)
            )
        } else if (getIntent.equals("myProfile")) {
            CommonMethods.addFragmentToSetting(supportFragmentManager, MyProfileFragment())
        } else if (getIntent.equals("AboutUs")) {
            //CommonMethods.addFragmentToSetting(supportFragmentManager, AboutUsFragment("Setting"))
            CommonMethods.addFragmentToSetting(
                supportFragmentManager,
                ReferenceFragment(Constant.ABOUT, 0)
            )
        } else if (getIntent.equals("Reference")) {
            CommonMethods.addFragmentToSetting(
                supportFragmentManager,
                ReferenceFragment(Constant.REFERENCE, 0)
            )
        } else if (getIntent.equals("Acknowledgement")) {
            CommonMethods.addFragmentToSetting(
                supportFragmentManager,
                ReferenceFragment(Constant.ACKNOWLEDGEMENT, 0)
            )
        } else if (getIntent == "Terms") {
//            CommonMethods.addFragmentToSetting(supportFragmentManager, TermsAndConditions())
            CommonMethods.addFragmentToSetting(
                supportFragmentManager,
                ReferenceFragment(Constant.TERMS, 0)
            )
        } else {
//            CommonMethods.addFragmentToSetting(
//                supportFragmentManager,
//                PrivacyPolicyFragment()
//            )
            CommonMethods.addFragmentToSetting(
                supportFragmentManager,
                ReferenceFragment(Constant.PRIVACY, 0)
            )
        }
    }
}
