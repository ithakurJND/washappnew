package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.MedicineHistoryAdapter
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.ResultSearchedMed
import com.rxhub.rxhub.webServices.model.SearchedMedicineResponse
import kotlinx.android.synthetic.main.fragment_medicine_search_history.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/*
* This class fragment_medicine_search_history show that medicine list which search by user.
*
* created by GTB on 07/05/2020
*/
class MedicineSearchHistoryFragment : BaseFragment() {
    val hashMap = HashMap<String, String>()
    var pageCount = 0
    private var loading = true
    val medicineList = ArrayList<ResultSearchedMed>()
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_medicine_search_history
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //click Listener
        ivBackArrow.setOnClickListener {
            activity!!.onBackPressed()
        }
        //set Layout Manager for Recycler View
        val layoutManager = LinearLayoutManager(activity!!)
        recyclerHistory.layoutManager = layoutManager
        //get searched data from server
        if (CommonMethods.phoneIsOnline(activity!!))
        getSearchedMedicine(0, true)
        else{
            recyclerHistory.visibility = View.GONE
            tvNoDataHistory.visibility = View.VISIBLE
        }
        //this is pagination for recycler View
//        recyclerHistory.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                if (dy > 0) {
//                    val visibleItemCount = layoutManager.getChildCount()
//                    val totalItemCount = layoutManager.getItemCount()
//                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
//                    if (loading) {
//                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
//                            loading = false
//                            Log.v("...", "Last Item Wow !")
//                            //Do pagination.. i.e. fetch new data
//                            progress_bar_history.visibility = View.VISIBLE
//                            getSearchedMedicine(pageCount, false)
//                        }
//                    }
//                }
//            }
//        })
   }

    //method for get Searched List from server
    fun getSearchedMedicine(page: Int, boolean: Boolean) {
        if (boolean) {
            CommonMethods.showProgress(activity!!)
        }
        hashMap.put("language_id", ApplicationGlobal.english.toString().trim())
        hashMap.put("page_count", page.toString().trim())
        RetrofitClient.getRetrofit().getSearchedMedicines(hashMap)
            .enqueue(object : Callback<SearchedMedicineResponse> {
                override fun onFailure(call: Call<SearchedMedicineResponse>, t: Throwable) {
                    CommonMethods.dismissProgress()
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.toast(activity!!, t.message!!)
                    } else {
                        CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                }

                override fun onResponse(
                    call: Call<SearchedMedicineResponse>,
                    response: Response<SearchedMedicineResponse>
                ) {
                    try {


                    CommonMethods.dismissProgress()
                    if (response.isSuccessful && response.body() != null) {
                        if (page == 0) {
                            if (response.body()!!.count > 0) {
                                recyclerHistory.visibility = View.VISIBLE
                                tvNoDataHistory.visibility = View.GONE
                                val medicines = response.body()!!.result
                                for (i in medicines.indices) {
                                    medicineList.add(medicines[i])
                                }
                                recyclerHistory.adapter =
                                    MedicineHistoryAdapter(activity!!, medicineList)
                                if (medicineList.size == Constant.TOTAL_DATA) {
                                    loading = true
                                    pageCount++
                                }
                            } else {
                                recyclerHistory.visibility = View.GONE
                                tvNoDataHistory.visibility = View.VISIBLE
                            }
                        } else {
                            val newData = response.body()!!.result
                            medicineList.addAll(newData)
                            recyclerHistory.adapter!!.notifyDataSetChanged()
                            Log.i("paginatoins", "" + medicineList.size)
                            if (medicineList.size == (page + 1) * Constant.TOTAL_DATA) {
                                pageCount++
                                loading = true
                            }
                        }
                        progress_bar_history.visibility = View.GONE
                    } else {
                        CommonMethods.getErrorMessageAndSessionExpire(
                            activity!!,
                            response.errorBody()!!
                        )
//                        CommonMethods.toast(
//                            activity!!, "" +
//                                    CommonMethods.getErrorMessage(response.errorBody()!!)
//                        )
                    }
                    }catch (e:Exception){}
                }
            })
    }
}