package com.rxhub.rxhub.fragments

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.util.Linkify
import android.view.Gravity
import android.widget.Toast
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.SettingContainerActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.AcknowledgementResponse
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import kotlinx.android.synthetic.main.fragment_about_us.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AboutUsFragment(val from: String, val name: String) : BaseFragment() {
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_about_us
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivBackArrowAboutUs.setOnClickListener { activity!!.onBackPressed() }
        ivProfileAbout.setOnClickListener {
            if (from == "Main") {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "myProfile")
                )
            } else {
                CommonMethods.replaceFragmentInSetting(
                    fragmentManager!!,
                    MyProfileFragment()
                )
            }
        }
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails.profile_pic,
                    ivProfileAbout
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfileAbout
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfileAbout)
        }
        when (name) {
            Constant.REFERENCE -> {
                tvPrivacy.text = activity!!.resources.getString(R.string.reference)
                tvAboutUsText.linksClickable = true
                tvAboutUsText.autoLinkMask = Linkify.ALL
            }
            Constant.ACKNOWLEDGEMENT -> {
                tvPrivacy.text = activity!!.resources.getString(R.string.acknowledgement)
                tvAboutUsText.gravity = Gravity.START
            }
            Constant.TERMS -> {
                tvPrivacy.text = activity!!.resources.getString(R.string.termsandConditions)
                tvAboutUsText.gravity = Gravity.START
            }
            Constant.PRIVACY -> {
                tvPrivacy.text = activity!!.resources.getString(R.string.privacyPolicy)
                tvAboutUsText.gravity = Gravity.START
            }
            Constant.ABOUT -> {
                tvPrivacy.text = activity!!.resources.getString(R.string.about_us)
                tvAboutUsText.gravity = Gravity.START
            }
            Constant.TERMS_OF_USE -> {
                tvPrivacy.text = activity!!.resources.getString(R.string.termsofUse)
                tvAboutUsText.gravity = Gravity.START
            }
            Constant.WORK -> {
                tvPrivacy.text = activity!!.resources.getString(R.string.howitworks)
                tvAboutUsText.gravity = Gravity.START
            }
            Constant.DISCLAIMER -> {
                tvPrivacy.text = activity!!.resources.getString(R.string.disclaimer)
                tvAboutUsText.gravity = Gravity.START
            }
            Constant.INSTRUCTIONS -> {
                tvPrivacy.text = activity!!.resources.getString(R.string.generalInstructions)
                tvAboutUsText.gravity = Gravity.START
            }
        }
        CommonMethods.showProgress(activity!!)
        RetrofitClient.getRetrofit().getDynamicData(name)
            .enqueue(object : Callback<AcknowledgementResponse> {
                override fun onFailure(call: Call<AcknowledgementResponse>, t: Throwable) {
                    CommonMethods.dismissProgress()
                    Toast.makeText(requireActivity(),"No internet connection", Toast.LENGTH_LONG).show()
                }

                override fun onResponse(
                    call: Call<AcknowledgementResponse>,
                    response: Response<AcknowledgementResponse>
                ) {
                    CommonMethods.dismissProgress()
                    if (response.isSuccessful) {
                        when (name) {
                            Constant.TERMS -> {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvAboutUsText.setText(
                                        Html.fromHtml(
                                            response.body()!!.terms_and_conditions,
                                            Html.FROM_HTML_MODE_COMPACT
                                        )
                                    )
                                } else {
                                    tvAboutUsText.setText(Html.fromHtml(response.body()!!.terms_and_conditions))
                                }
                            }
                            Constant.PRIVACY -> {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvAboutUsText.setText(
                                        Html.fromHtml(
                                            response.body()!!.privacy_and_policy,
                                            Html.FROM_HTML_MODE_COMPACT
                                        )
                                    )
                                } else {
                                    tvAboutUsText.setText(Html.fromHtml(response.body()!!.privacy_and_policy))
                                }
                            }
                            Constant.ABOUT -> {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvAboutUsText.setText(
                                        Html.fromHtml(
                                            response.body()!!.about,
                                            Html.FROM_HTML_MODE_COMPACT
                                        )
                                    )
                                } else {
                                    tvAboutUsText.setText(Html.fromHtml(response.body()!!.about))
                                }
                            }
                            Constant.TERMS_OF_USE -> {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvAboutUsText.setText(
                                        Html.fromHtml(
                                            response.body()!!.terms_of_use,
                                            Html.FROM_HTML_MODE_COMPACT
                                        )
                                    )
                                } else {
                                    tvAboutUsText.setText(Html.fromHtml(response.body()!!.terms_of_use))
                                }
                            }
                            Constant.WORK -> {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvAboutUsText.setText(
                                        Html.fromHtml(
                                            response.body()!!.how_it_works,
                                            Html.FROM_HTML_MODE_COMPACT
                                        )
                                    )
                                } else {
                                    tvAboutUsText.setText(Html.fromHtml(response.body()!!.how_it_works))
                                }
                            }
                            Constant.DISCLAIMER -> {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvAboutUsText.setText(
                                        Html.fromHtml(
                                            response.body()!!.disclaimer,
                                            Html.FROM_HTML_MODE_COMPACT
                                        )
                                    )
                                } else {
                                    tvAboutUsText.setText(Html.fromHtml(response.body()!!.disclaimer))
                                }
                            }
                            Constant.INSTRUCTIONS -> {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    tvAboutUsText.setText(
                                        Html.fromHtml(
                                            response.body()!!.general_instructions,
                                            Html.FROM_HTML_MODE_COMPACT
                                        )
                                    )
                                } else {
                                    tvAboutUsText.setText(Html.fromHtml(response.body()!!.general_instructions))
                                }
                            }
                        }
                    } else {
                        Toast.makeText(requireActivity(),"No internet connection",Toast.LENGTH_LONG).show()
                        CommonMethods.getErrorMessageAndSessionExpire(
                            activity!!,
                            response.errorBody()!!
                        )
                    }
                }
            })
    }
}