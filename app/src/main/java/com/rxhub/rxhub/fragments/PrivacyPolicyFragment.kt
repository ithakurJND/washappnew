package com.rxhub.rxhub.fragments

import android.os.Bundle
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import kotlinx.android.synthetic.main.fragment_privacy_policy.*

/*
* This class fragment_privacy_policy show the Privacy and Policy screen
*
* created by GTB on 20/04/2020
*/
class PrivacyPolicyFragment : BaseFragment() {
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_privacy_policy
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivBackArrow.setOnClickListener {
            activity!!.onBackPressed()
        }
        ivProfile.setOnClickListener {
            CommonMethods.replaceFragmentInSetting(
                fragmentManager!!,
                MyProfileFragment()
            )
        }
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails.profile_pic,
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
    }
}