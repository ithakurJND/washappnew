package com.rxhub.rxhub.fragments

import android.content.Intent
import android.os.Bundle
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.SettingContainerActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import kotlinx.android.synthetic.main.fragment_disclaimer.*

class DisclaimerFragment : BaseFragment() {
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_disclaimer
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivBackArrow.setOnClickListener { activity!!.onBackPressed() }
        ivProfile.setOnClickListener {
            startActivity(
                Intent(activity!!, SettingContainerActivity::class.java)
                    .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    .putExtra("SettingScreen", "myProfile")
            )
        }

        //get user data
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails.profile_pic,
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
    }
}