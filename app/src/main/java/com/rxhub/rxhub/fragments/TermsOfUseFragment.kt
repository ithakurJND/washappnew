package com.rxhub.rxhub.fragments

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.SettingContainerActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.AcknowledgementResponse
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import kotlinx.android.synthetic.main.fragment_about_us.*
import kotlinx.android.synthetic.main.fragment_terms_of_use.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TermsOfUseFragment(val from: String, val hitFor: String) : BaseFragment() {
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_terms_of_use
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivBackArrow.setOnClickListener { activity!!.onBackPressed() }
        if (hitFor == Constant.DISCLAIMER)
            tvHeader.text = activity!!.resources.getString(R.string.disclaimer)
        else if (hitFor == Constant.INSTRUCTIONS)
            tvHeader.text = activity!!.resources.getString(R.string.generalInstructions)
        else
            tvHeader.text = activity!!.resources.getString(R.string.termsofUse)
        if (from == "Menu") {
            ivProfile.setOnClickListener {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "myProfile")
                )
            }

            //get user data
            val getUserDetails = Gson().fromJson(
                ApplicationGlobal.preferenceManager.getBasicDetails(),
                BasicInfoResp::class.java
            )
            if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
                if (getUserDetails.profile_pic!!.startsWith("http")) {
                    CommonMethods.setUrlImage(
                        activity!!,
                        getUserDetails.profile_pic,
                        ivProfile
                    )
                } else {
                    CommonMethods.setServerImage(
                        activity!!,
                        getUserDetails.profile_pic.toString(),
                        ivProfile
                    )
                }
            } else {
                GlideApp.with(activity!!)
                    .load(R.drawable.placeholder_small)
                    .circleCrop()
                    .into(ivProfile)
            }
        } else {
            ivProfile.visibility = View.GONE



            CommonMethods.showProgress(requireActivity())
            RetrofitClient.getRetrofit().getDynamicData(hitFor)
                .enqueue(object : Callback<AcknowledgementResponse> {
                    override fun onFailure(call: Call<AcknowledgementResponse>, t: Throwable) {
                        CommonMethods.dismissProgress()
                        Toast.makeText(requireActivity(),"No internet connection",Toast.LENGTH_LONG).show()
                    }

                    override fun onResponse(
                        call: Call<AcknowledgementResponse>,
                        response: Response<AcknowledgementResponse>
                    ) {
                        CommonMethods.dismissProgress()
                        if (response.isSuccessful) {
                            when (hitFor) {
                                Constant.TERMS_OF_USE -> {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        tvAboutUsTextDes.setText(
                                            Html.fromHtml(
                                                response.body()!!.terms_of_use,
                                                Html.FROM_HTML_MODE_COMPACT
                                            )
                                        )
                                    } else {
                                        tvAboutUsTextDes.setText(Html.fromHtml(response.body()!!.terms_of_use))
                                    }
                                }
                                Constant.DISCLAIMER -> {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        tvAboutUsTextDes.setText(
                                            Html.fromHtml(
                                                response.body()!!.disclaimer,
                                                Html.FROM_HTML_MODE_COMPACT
                                            )
                                        )
                                    } else {
                                        tvAboutUsTextDes.setText(Html.fromHtml(response.body()!!.disclaimer))
                                    }
                                }
                                Constant.INSTRUCTIONS -> {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        tvAboutUsTextDes.setText(
                                            Html.fromHtml(
                                                response.body()!!.general_instructions,
                                                Html.FROM_HTML_MODE_COMPACT
                                            )
                                        )
                                    } else {
                                        tvAboutUsTextDes.setText(Html.fromHtml(response.body()!!.general_instructions))
                                    }
                                }
                            }

                        } else {
                            Toast.makeText(requireActivity(),"No internet connection",Toast.LENGTH_LONG).show()
                            CommonMethods.getErrorMessageAndSessionExpire(
                                activity!!,
                                response.errorBody()!!
                            )
                        }
                    }
                })
        }
    }

}