package com.rxhub.rxhub.fragments

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Color
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.InputType.TYPE_NULL
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.gson.Gson
import com.hbb20.CountryCodePicker
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.MainActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.*
import kotlinx.android.synthetic.main.fragment_basic_information.*
import kotlinx.android.synthetic.main.fragment_my_profile.*
import kotlinx.android.synthetic.main.fragment_my_profile.counterCode
import kotlinx.android.synthetic.main.fragment_my_profile.etCompanyName
import kotlinx.android.synthetic.main.fragment_my_profile.etPhoneNumber
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.util.*
import kotlin.collections.HashMap

/*
* This class fragment_my_profile show user data and user edit their profile in this screen.
*
* created by GTB on 21/04/2020
*/
class MyProfileFragment : BaseFragment(), View.OnClickListener {
    var getUserDetails: BasicInfoResp? = null
    val hashMap = HashMap<String, Any>()
    lateinit var listPopupWindowDesig: ListPopupWindow
    var companyDesignationList = arrayListOf<Result>()
    val designationName = arrayListOf<String>()
    var designationId = ""
    var pageCount = 0
    var selected_country_code: String = ""
    var serverImageId = ""
    lateinit var listPopupWindowDesignationCat: ListPopupWindow
    val designationCategoryName = arrayListOf<String>()
    var designationCatId = ""
    val designationCatList = arrayListOf<ResultCat>()

    val TAG = "ProfileScreen"
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_my_profile
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //set popup window list for Select gender
        CommonMethods.dropDownList(activity!!, Constant.GENDER_LIST, etGender)
        //set user data
        try {
            getUserDetails = Gson().fromJson(
                ApplicationGlobal.preferenceManager.getBasicDetails(),
                BasicInfoResp::class.java
            )

        } catch (e: Exception) {
        }
        try {
            //set user data
            if (getUserDetails!!.name != null) {
                etName.setText(getUserDetails!!.name)
            } else {
                etName.setText("")
            }
            if (getUserDetails!!.email != null) {
                etEmail.setText(getUserDetails!!.email)
            } else {
                etEmail.setText("")
            }
            if (getUserDetails!!.phone_no != null) {
                etPhoneNumber.setText(getUserDetails!!.phone_no)
            }
            if (getUserDetails!!.age != null) {
                etAge.setText(getUserDetails!!.age.toString())
            }
            if (getUserDetails!!.designation_id != null) {
                designationId = getUserDetails!!.designation_id.toString()
            }
            if (getUserDetails!!.designation_category_id != null) {
                designationCatId = getUserDetails!!.designation_category_id.toString()
            }
            if (getUserDetails!!.gender != null) {
                if (getUserDetails!!.gender == 2) {
                    etGender.setText(resources.getString(R.string.male))
                } else {
                    etGender.setText(resources.getString(R.string.female))
                }
            }
            if (getUserDetails!!.company_name != null) {
                etCompanyName.setText(getUserDetails!!.company_name)
            }
            if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
                if (getUserDetails!!.profile_pic!!.startsWith("http")) {
                    CommonMethods.setUrlImage(
                        activity!!,
                        getUserDetails!!.profile_pic.toString(),
                        ivUserProfile
                    )
                } else {
                    CommonMethods.setServerImage(
                        activity!!,
                        getUserDetails!!.profile_pic.toString(),
                        ivUserProfile
                    )
                }
            } else {
                GlideApp.with(activity!!)
                    .load(R.drawable.placeholder_large)
                    .circleCrop()
                    .into(ivUserProfile)
            }
            if (getUserDetails!!.country_code != null) {
                counterCode.setCountryForPhoneCode(getUserDetails!!.country_code!!.toInt())
            }

        } catch (e: Exception) {
            Log.i(TAG, "" + e.message!!)
        }
        //click listener
        etGender.setOnClickListener(this)
        etDesignation.setOnClickListener(this)
        tvEditProfile.setOnClickListener(this)
        ivBackArrow.setOnClickListener(this)
        cameraCardView.setOnClickListener(this)
        tvDesignationCategory.setOnClickListener(this)
        //method for isEnable= false
        editTextEditTable(false, "EDIT PROFILE", "My Profile")

        //this is for get Country Code ClickListener
        selected_country_code = counterCode.selectedCountryCodeWithPlus
        counterCode.setOnCountryChangeListener(object : CountryCodePicker.OnCountryChangeListener {
            override fun onCountrySelected() {
                selected_country_code = counterCode.getSelectedCountryCodeWithPlus()
            }
        })



        RetrofitClient.getRetrofit().getDesignationCategory()
            .enqueue(object : Callback<DesignationCategoryResponse> {
                override fun onFailure(call: Call<DesignationCategoryResponse>, t: Throwable) {
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.toast(activity!!, t.message!!)
                    } else {
                        CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                }

                override fun onResponse(
                    call: Call<DesignationCategoryResponse>,
                    response: Response<DesignationCategoryResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        try {
                            val list = response.body()!!.result
                            Log.i("asdfasdfasfdasfd", "" + list)
                            for (i in list.indices) {
                                designationCatList.add(list[i])
                                designationCategoryName.add(list[i].name)
                                if (list[i].id.toString() == designationCatId) {
                                    tvDesignationCategory.setText(list[i].name.toString())
                                }
                            }
                        } catch (e: Exception) {
                        }
                    } else {
                        CommonMethods.getErrorMessageAndSessionExpire(
                            activity!!,
                            response.errorBody()!!
                        )
                    }
                }
            })

        getDesignation(designationCatId)
        //this is for Designation Category
        listPopupWindowDesignationCat = ListPopupWindow(activity!!)
        listPopupWindowDesignationCat.setAdapter(
            ArrayAdapter(
                activity!!,
                android.R.layout.simple_dropdown_item_1line,
                designationCategoryName
            )
        )
        listPopupWindowDesignationCat.anchorView = tvDesignationCategory
        listPopupWindowDesignationCat.isModal = true
        listPopupWindowDesignationCat.setOnItemClickListener { parent, view, position, id ->
            tvDesignationCategory.setText(designationCategoryName[position])
            designationCatId = designationCatList[position].id.toString()
            etDesignation.text.clear()
            CommonMethods.showProgress(activity!!)
            getDesignation(designationCatId)
            listPopupWindowDesignationCat.dismiss()
        }

        //set designation list in ListPopupWindow
        listPopupWindowDesig = ListPopupWindow(activity!!)
        listPopupWindowDesig.setAdapter(
            ArrayAdapter(
                activity!!,
                android.R.layout.simple_dropdown_item_1line,
                designationName
            )
        )
        listPopupWindowDesig.anchorView = etDesignation
        listPopupWindowDesig.isModal = true
        listPopupWindowDesig.setOnItemClickListener { parent, view, position, id ->
            etDesignation.setText(designationName[position])
            designationId = companyDesignationList[position].id.toString()
            listPopupWindowDesig.dismiss()
        }
    }

    //this method handle that all editText and textView are clickable or not and set text in TextViews
    //and change Edit Profile design
    fun editTextEditTable(boolean: Boolean, text: String, textTitle: String) {
        etPhoneNumber.isEnabled = boolean
        etEmail.isEnabled = false
        etAge.isEnabled = boolean
        etGender.isEnabled = boolean
        etCompanyName.isEnabled = boolean
        etName.isEnabled = boolean
        etDesignation.isEnabled = boolean
        tvDesignationCategory.isEnabled = boolean
        etName.clearFocus()
        etAge.clearFocus()
        etEmail.clearFocus()
        etGender.clearFocus()
        etDesignation.clearFocus()
        etCompanyName.clearFocus()
        etPhoneNumber.clearFocus()
        etGender.inputType = TYPE_NULL
        etDesignation.inputType = TYPE_NULL
        counterCode.isClickable = false
        counterCode.isEnabled = false

        tvEditProfile.text = text
        tvTitle.text = textTitle

        if (text.equals("EDIT PROFILE")) {
            tvEditProfile.setBackgroundResource(R.drawable.textview_border)
            tvEditProfile.setTextColor(resources.getColor(R.color.colorGreen))
            cameraCardView.visibility = View.GONE
        } else {
            tvEditProfile.setBackgroundResource(R.drawable.editprofile_save_button)
            tvEditProfile.setTextColor(Color.WHITE)
            cameraCardView.visibility = View.VISIBLE
        }
    }

    //method for all click Listener
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.tvDesignationCategory -> {
                CommonMethods.hideKeyboard(activity!!)
                listPopupWindowDesignationCat.show()
            }
            R.id.etDesignation -> {
                CommonMethods.hideKeyboard(activity!!)
                if (CommonMethods.phoneIsOnline(activity!!)) {
                    listPopupWindowDesig.show()
                } else {
                    CommonMethods.toast(activity!!, "Internet connection not available")
                }
            }
            R.id.cameraCardView -> {
                selectImage()
            }
            R.id.etGender -> {
                CommonMethods.hideKeyboard(activity!!)
                CommonMethods.showDropDownList()
            }
            R.id.ivBackArrow -> {
                if (tvEditProfile.text.equals("SAVE CHANGES")) {
                    editTextEditTable(false, "EDIT PROFILE", "My Profile")
                } else {
                    activity!!.onBackPressed()
                }
            }
            //this for edit the profile and save the data
            R.id.tvEditProfile -> {
                if (CommonMethods.phoneIsOnline(activity!!)) {
                    if (tvEditProfile.text.equals("EDIT PROFILE")) {
                        editTextEditTable(true, "SAVE CHANGES", "Edit Profile")
                    } else {
                        var genderId = 0
                        // try {

//                        } catch (e: Exception) {
//                        }
                        if (etName.text.toString().trim().isEmpty()) {
                            etName.requestFocus()
                            etName.error = "Name is required."
                            // CommonMethods.toast(activity!!, "Name is required")

                        } else if (etPhoneNumber.text.trim().isEmpty()) {
                            etPhoneNumber.requestFocus()
                            etPhoneNumber.error = "Phone number is required."
                            //  CommonMethods.toast(activity!!, "Phone number is required")
                        } else if (!CommonMethods.isValidPhone(
                                etPhoneNumber.text.toString().trim()
                            )
                        ) {
                            etPhoneNumber.requestFocus()
                            etPhoneNumber.error = "Phone number is not valid."
                            //CommonMethods.toast(activity!!, "Phone number is not valid")
                        } else if (etAge.text.trim().isEmpty()) {
                            etAge.requestFocus()
                            etAge.error = "Please enter age."
                        }
                        else if (etGender.text.isEmpty())
                        {
                            etGender.requestFocus()
                            etGender.error = "Please select gender."
                        }

                        else if (tvDesignationCategory.text.trim().isEmpty()) {
                            tvDesignationCategory.requestFocus()
                            tvDesignationCategory.error = "Please Select Profession"
                            //CommonMethods.toast(activity!!, "Please Select Profession")
                        } else {
                            if (etGender.text.toString().equals("Male")) {
                                genderId = 2
                            } else {
                                genderId = 1
                            }
                            if (!etDesignation.text.toString().trim().isEmpty())
                                hashMap.put("designation_id", designationId.trim().toInt())
                            CommonMethods.showProgress(activity!!)
                            hashMap.put("profile_pic", serverImageId.trim())
                            hashMap.put("name", etName.text.toString().trim())
                            hashMap.put("email", etEmail.text.toString().trim())
                            hashMap.put("mobile_number", etPhoneNumber.text.toString().trim())
                            if (etAge.text.toString().isNotEmpty())
                                hashMap.put("age", etAge.text.toString().trim().toInt())
                            hashMap.put("gender", genderId.toString().trim().toInt())
                            hashMap.put("designation_category_id", designationCatId.trim().toInt())
                            hashMap.put("country_code", selected_country_code.trim())
                            if (etCompanyName.text.toString().isNotEmpty())
                                hashMap.put("company_name", etCompanyName.text.toString().trim())
                            RetrofitClient.getRetrofit().setBasicInfo(hashMap)
                                .enqueue(object : Callback<BasicResponse> {
                                    override fun onFailure(
                                        call: Call<BasicResponse>,
                                        t: Throwable
                                    ) {
                                        CommonMethods.dismissProgress()
                                        if (CommonMethods.phoneIsOnline(activity!!)) {
                                            CommonMethods.toast(activity!!, t.message!!)
                                        } else {
                                            CommonMethods.toast(
                                                activity!!,
                                                "Internet connection not available"
                                            )
                                        }
                                    }

                                    override fun onResponse(
                                        call: Call<BasicResponse>,
                                        response: Response<BasicResponse>
                                    ) {

                                        if (response.isSuccessful && response.body() != null) {
                                            ApplicationGlobal.hitAllApis = true
                                            ApplicationGlobal.isProfileUpdated = true
                                            Log.i("aasdfasdf", "" + response.body()!!)
//                                        CommonMethods.toast(activity!!, response.body()!!.message)
                                            editTextEditTable(false, "EDIT PROFILE", "My Profile")
                                            RetrofitClient.getRetrofit().getUserData()
                                                .enqueue(object : Callback<BasicInfoResp> {
                                                    override fun onFailure(
                                                        call: Call<BasicInfoResp>,
                                                        t: Throwable
                                                    ) {
                                                        CommonMethods.dismissProgress()
                                                    }

                                                    override fun onResponse(
                                                        call: Call<BasicInfoResp>,
                                                        response: Response<BasicInfoResp>
                                                    ) {
                                                        if (response.isSuccessful) {
                                                            CommonMethods.toast(
                                                                activity!!,
                                                                "Profile Updated!"
                                                            )
                                                            ApplicationGlobal.preferenceManager.setBasicDetails(
                                                                Gson().toJson(response.body()!!)
                                                            )
                                                            if (response.body()!!.profile_pic != null) {
                                                                ApplicationGlobal.profile_pic =
                                                                    response.body()!!.profile_pic.toString()
                                                            }
                                                            CommonMethods.dismissProgress()
                                                            activity!!.startActivity(
                                                                Intent(
                                                                    activity,
                                                                    MainActivity::class.java
                                                                )
                                                            )
                                                            activity!!.finishAffinity()
                                                        } else {
                                                            CommonMethods.dismissProgress()
                                                        }
                                                    }
                                                })

                                        } else {
                                            CommonMethods.dismissProgress()
                                            CommonMethods.getErrorMessageAndSessionExpire(
                                                activity!!,
                                                response.errorBody()!!
                                            )
//                                        CommonMethods.sessionExpire(
//                                            activity!!,
//                                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
//                                        )
                                        }
//                                    CommonMethods.dismissProgress()
                                    }
                                })
                        }
                    }
                } else
                    Toast.makeText(activity, "No internet connection", Toast.LENGTH_LONG).show()
            }
        }
    }

    //this method open Dialog box to choose options gallery/camera
    fun selectImage() {
        val items = arrayOf("Gallery", "Camera", "Cancel")
        val alertDialog = AlertDialog.Builder(activity)
        alertDialog.setTitle("Add Profile")
            .setCancelable(false)
            .setItems(items) { dialogInterface, positon ->
                if (items[positon].equals("Gallery")) {
                    if (ActivityCompat.checkSelfPermission(
                            activity!!,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            activity!!, arrayOf(
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ), Constant.SELECT_IMAGE
                        )
                    } else {
                        val intent =
                            Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                        intent.type = "image/*"
                        startActivityForResult(
                            Intent.createChooser(intent, "Select File"), Constant.SELECT_IMAGE
                        )
                    }
                } else if (items[positon].equals("Camera")) {
                    if (ActivityCompat.checkSelfPermission(
                            activity!!,
                            Manifest.permission.CAMERA
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            activity!!, arrayOf(
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ), Constant.OPEN_CAMERA
                        )
                    } else {
                        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        startActivityForResult(intent, Constant.OPEN_CAMERA)
                    }
                } else if (items[positon].equals("Cancel")) {
                    dialogInterface.dismiss()
                }
            }
        alertDialog.show()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constant.OPEN_CAMERA -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // related task you need to do.

                    activity!!.ivOpenGallery.performClick()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }
            Constant.SELECT_IMAGE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // related task you need to do.

                    activity!!.ivOpenGallery.performClick()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
//        if (requestCode == SELECT_IMAGE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            activity!!.ivOpenGallery.performClick()
//            CommonMethods.toast(activity!!, "Setect Image")
//        } else if (requestCode == OPEN_CAMERA && grantResults.size > 0
//            && grantResults[0] == PackageManager.PERMISSION_GRANTED
//        ) {
//            ivOpenGallery.performClick()
//            CommonMethods.toast(activity!!, "open camera")
//        }
    }

    //on this method we get the selected image from gallery and camera.
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.SELECT_IMAGE && resultCode == Activity.RESULT_OK &&
            data != null && data.data != null
        ) {
            val selectedImage = data.data!!
            val realPath = getRealPath(selectedImage)
            GlideApp.with(activity!!)
                .load(realPath)
                .circleCrop()
                .into(ivUserProfile)
            uploadImage(realPath)
        } else if (requestCode == Constant.OPEN_CAMERA && resultCode == Activity.RESULT_OK
        ) {
            if (data != null) {
                val thumbnai = data.extras!!["data"] as Bitmap
                GlideApp.with(activity!!)
                    .load(thumbnai)
                    .circleCrop()
                    .into(ivUserProfile)
                uploadImage(saveImage(thumbnai).toString())
            }
        }
    }

    //this method get real path from the phone gallery
    fun getRealPath(contentUri: Uri): String {
        val path = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor? = context!!.contentResolver.query(
            contentUri, path,
            null, null, null
        )
        val column = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        return cursor.getString(column)
    }

    //this method used to upload the image to the server after select image
    fun uploadImage(image: String) {
        CommonMethods.showProgress(activity!!)
        val file = File(image.trim())
        Log.i("file", "" + image)
        // creates RequestBody instance from file
        val requestFile =
            RequestBody.create(MediaType.parse("multipart/form-data"), file)
        // MultipartBody.Part is used to send also the actual filename
        val body =
            MultipartBody.Part.createFormData("file", "rxImage", requestFile)
        // adds another part within the multipart request
        val descriptionString = "Sample description"
        val description =
            RequestBody.create(MediaType.parse("multipart/form-data"), descriptionString)

        RetrofitClient.getRetrofit().uploadImage(body, description)
            .enqueue(object : Callback<ImageResponse> {
                override fun onFailure(call: Call<ImageResponse>, t: Throwable) {
                    CommonMethods.dismissProgress()
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.toast(activity!!, t.message!!)
                    } else {
                        CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                }

                override fun onResponse(
                    call: Call<ImageResponse>,
                    response: Response<ImageResponse>
                ) {
                    CommonMethods.dismissProgress()
                    if (response.isSuccessful && response.body() != null) {
                        Log.i("uploadImage", response.body()!!.file_name)
                        serverImageId = response.body()!!.file_name
                    } else {
                        CommonMethods.getErrorMessageAndSessionExpire(
                            activity!!,
                            response.errorBody()!!
                        )
//                        CommonMethods.toast(
//                            activity!!, "" +
//                                    CommonMethods.getErrorMessage(response.errorBody()!!)
//                        )
                    }
                }
            })
    }

    //this method is used to save the camera clicked image into the gallery
    fun saveImage(myBitmap: Bitmap): String? {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val wallpaperDirectory =
            File(Environment.getExternalStorageDirectory().toString() + "/RxHub")
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs()
        }
        val f = File(
            wallpaperDirectory, Calendar.getInstance()
                .timeInMillis.toString() + ".jpg"
        )
        f.createNewFile()
        val fo = FileOutputStream(f)
        fo.write(bytes.toByteArray())
        MediaScannerConnection.scanFile(
            activity,
            arrayOf(f.path),
            arrayOf("image/jpeg"),
            null
        )
        fo.close()
        Log.d("TAG", "File Saved::---&gt;" + f.absolutePath)
        return f.absolutePath
    }


    fun getDesignation(categoryId: String) {
        //this for getting the designation List
        CommonMethods.showProgress(activity!!)
        hashMap.put("designation_category_id", categoryId)
        hashMap.put("language_id", ApplicationGlobal.english.toString().trim())
        hashMap.put("page_count", pageCount.toString().trim())
        RetrofitClient.getRetrofit().getDesignation(hashMap)
            .enqueue(object : Callback<Designations> {
                override fun onFailure(call: Call<Designations>, t: Throwable) {
                    CommonMethods.dismissProgress()
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.toast(activity!!, t.message!!)
                    } else {
                        CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                }

                override fun onResponse(
                    call: Call<Designations>,
                    response: Response<Designations>
                ) {
                    CommonMethods.dismissProgress()
                    if (response.isSuccessful && response.body() != null) {
                        try {
                            val desi = response.body()!!.result
                            companyDesignationList.clear()
                            designationName.clear()
                            for (i in desi.indices) {
                                companyDesignationList.add(desi[i])
                                designationName.add(desi[i].name)
                                if (desi[i].id.toString() == designationId) {
                                    etDesignation.setText(desi[i].name)
                                }
                            }
                        } catch (e: Exception) {
                        }

                    } else {
                        Log.i(
                            "getDesignations",
                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
                        )
                    }
                }
            })
    }
}