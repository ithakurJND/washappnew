package com.rxhub.rxhub.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.HomeSearchAdapter
import com.rxhub.rxhub.realmModels.AllData
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.fragment_home_search.*


/*
* This class fragment_home_search show Search screen for home search button
*
* create by GTB on 30/04/2020
 */
class HomeSearchFragment : BaseFragment(), View.OnClickListener {
    val hashMapSearch = HashMap<String, String>()
    var pageCount = 0
    private var loading = true
    lateinit var word: String
    val demoList = ArrayList<AllData>()
    val TAG = "searchListData"
    lateinit var realm: Realm
    lateinit var adapter: HomeSearchAdapter
    var searchList = ArrayList<AllData>()
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_home_search
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        etHomeSearch.requestFocus()
        val imm =
            activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.showSoftInput(etHomeSearch, InputMethodManager.SHOW_IMPLICIT);
        realm = Realm.getDefaultInstance()
        demoList.clear()
        val allData = realm.where<AllData>().findAllAsync()
        demoList.addAll(allData)
        ivBackArrowSearch.setOnClickListener(this)
        ivProfileSearch.setOnClickListener(this)

        word = ApplicationGlobal.preferenceManager.getHomeSearchText().toString()
        etHomeSearch.setText(word)

        val getUserDetails =
            Gson().fromJson(
                ApplicationGlobal.preferenceManager.getBasicDetails(),
                BasicInfoResp::class.java
            )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfileSearch
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfileSearch
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfileSearch)
        }

        val layoutManager = GridLayoutManager(activity!!, 2)
        recyclerViewSearchItem.layoutManager = layoutManager
        adapter = HomeSearchAdapter(activity!!, demoList!!)
        recyclerViewSearchItem.adapter = adapter
//        if (searchList!!.size == 0) {
//            //    search(0, word)
//            Log.i(TAG, " 0 " + searchList!!.size)
//        } else {
//            Log.i(TAG, " 1  " + searchList!!.size.toString())
//        }
        etHomeSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s!!.length > 0) {
                    recyclerViewSearchItem.visibility = View.VISIBLE
                    searchFilter(s.toString())
                }
                else {
                    recyclerViewSearchItem.visibility = View.GONE
                    adapter.filter(demoList)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//                if (s!!.length > 0) {
//                    //search(0, s.toString())
//                } else {
//                    //search(0, word)
//                }

            }
        })
        //  recyclerViewSearchItem.isNestedScrollingEnabled = false
//        //this for pagination
//        recyclerViewSearchItem.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                if (dy > 0) //check for scroll down
//                {
//                    val visibleItemCount = layoutManager.getChildCount()
//                    val totalItemCount = layoutManager.getItemCount()
//                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
//                    if (loading) {
//                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
//                            loading = false
//                            Log.v("...", "Last Item Wow !")
//                            Log.i("paginasdgas",""+pageCount)
//                            //Do pagination.. i.e. fetch new data
//                            progress_barHomeSearch.visibility = View.VISIBLE
//                            search(++pageCount, etHomeSearch.text.toString())
//                        }
//                    }
//                }
//            }
//        })

//        nesteScroll.setOnScrollChangeListener(object : NestedScrollView.OnScrollChangeListener {
//            override fun onScrollChange(
//                v: NestedScrollView?,
//                scrollX: Int,
//                scrollY: Int,
//                oldScrollX: Int,
//                oldScrollY: Int
//            ) {
//                if (scrollY > 0) //check for scroll down
//                {
//                    val visibleItemCount = layoutManager.getChildCount()
//                    val totalItemCount = layoutManager.getItemCount()
//                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
//                    if (loading) {
//                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
//                            loading = false
//                            Log.v("...", "Last Item Wow !")
//                            Log.i("paginasdgas", "" + pageCount)
//                            //Do pagination.. i.e. fetch new data
//                            progress_barHomeSearch.visibility = View.VISIBLE
//                            search(++pageCount, etHomeSearch.text.toString())
//                        }
//                    }
//                }
//            }
//        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivBackArrowSearch -> {
                activity!!.onBackPressed()
            }
            R.id.ivProfileSearch -> {
                CommonMethods.replaceFragmentInSetting(fragmentManager!!, MyProfileFragment())
            }
        }
    }


    fun searchFilter(word: String) {
        searchList.clear()
        for (data in demoList) {
            if (data.name!!.toLowerCase().startsWith(word.toLowerCase()))
                searchList.add(data)
        }
//        if (searchList.isEmpty())
//            tvNoDataHomeSearch.visibility = View.VISIBLE
//        else {
//            tvNoDataHomeSearch.visibility = View.GONE
//        }
        adapter.filter(searchList)
    }

//    fun search(page: Int, word: String) {
//        hashMapSearch.put("language_id", ApplicationGlobal.english.toString().trim())
//        hashMapSearch.put("page_count", page.toString().trim())
//        hashMapSearch.put("search", word.trim())
//        RetrofitClient.getRetrofit().getMedicines(hashMapSearch)
//            .enqueue(object : Callback<SystemResp> {
//                override fun onFailure(call: Call<SystemResp>, t: Throwable) {
//                    if (CommonMethods.phoneIsOnline(activity!!)) {
//                        CommonMethods.toast(activity!!, t.message!!)
//                    } else {
//                        CommonMethods.toast(activity!!, "Internet connection not available")
//                    }
//                }
//
//                override fun onResponse(call: Call<SystemResp>, response: Response<SystemResp>) {
//                    Log.i(TAG, " 2response  " + searchList!!.size.toString())
//                    if (response.isSuccessful && response.body() != null) {
//                        if (page == 0) {
//                            if (response.body()!!.count > 0) {
//                                pageCount = 0
//                                tvNoDataHomeSearch.visibility = View.GONE
//                                recyclerViewSearchItem.visibility = View.VISIBLE
//                                searchList!!.clear()
//                                val a = response.body()!!.result
//                                for (i in a.indices) {
//                                    searchList!!.add(a[i])
//                                }
//                                recyclerViewSearchItem.adapter =
//                                    HomeSearchAdapter(
//                                        activity!!,
//                                        searchList!!
//                                    )
//                            } else {
//                                recyclerViewSearchItem.visibility = View.GONE
//                                tvNoDataHomeSearch.visibility = View.VISIBLE
//                            }
//                        } else {
//                            progress_barHomeSearch.visibility = View.GONE
//                            val newData = response.body()!!.result
//                            searchList!!.addAll(newData)
//                            recyclerViewSearchItem.adapter!!.notifyDataSetChanged()
//                            loading = true
//                        }
//                    } else {
//                        CommonMethods.getErrorMessageAndSessionExpire(
//                            activity!!,
//                            response.errorBody()!!
//                        )
////                        CommonMethods.toast(
////                            activity!!, "" +
////                                    CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
//                    }
//                }
//            })
//    }
}