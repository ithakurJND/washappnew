package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.DrugsBySystemAdapter
import com.rxhub.rxhub.realmModels.AllData
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.fragment_generic_medicine.*

/*
* This class fragment_generic_medicine show Generic Drugs.
*
* create by GTB on 20/04/2020
 */
class GenericMedicineFragment : BaseFragment(), View.OnClickListener {
    val hashMap = HashMap<String, String>()
    var pageCount = 0
    var getId = ""
    private var loading = true
    lateinit var realm: Realm
    val genericMedList = ArrayList<AllData>()
    lateinit var adapter: DrugsBySystemAdapter
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_generic_medicine
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        realm = Realm.getDefaultInstance()
        //click Listener
        ivBackArrowGeneric.setOnClickListener(this)
        ivProfileGeneric.setOnClickListener(this)
        //get drug id
        getId = ApplicationGlobal.preferenceManager.getDrugsId().toString()
        tvDrugsTypeName.text = ApplicationGlobal.preferenceManager.getDrugsName()
        Log.i("asdfasdf", "" + getId)
        //set layout Manager
        val layoutManager = GridLayoutManager(activity!!, 2)
        recyclerViewDrugsGeneric.layoutManager = layoutManager
        adapter = DrugsBySystemAdapter(genericMedList, activity!!)
        recyclerViewDrugsGeneric.adapter = adapter

        //get Medicine data
        // getdata(0, true, "")

        //this for search (Edit Text).
        etSearchGeneric.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                  if (s!!.length > 0) filter(s.toString()) else adapter.filter(genericMedList)
            }
        })
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfileGeneric
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfileGeneric
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfileGeneric)
        }

//        //this for pagination
//        recyclerViewDrugsGeneric.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                if (dy > 0) //check for scroll down
//                {
//                    val visibleItemCount = layoutManager.getChildCount()
//                    val totalItemCount = layoutManager.getItemCount()
//                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
//                    if (loading) {
//                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
//                            loading = false
//                            Log.v("...", "Last Item Wow !")
//                            //Do pagination.. i.e. fetch new data
//                            progress_bar_generic.visibility = View.VISIBLE
//                            if (etSearchGeneric.text.trim().isEmpty()) {
//                                // getdata(pageCount, false, "")
//                            } else {
//                                //getdata(pageCount, false, etSearchGeneric.text.trim().toString())
//                            }
//                        }
//                    }
//                }
//            }
//        })
        showAllByGeneric()
    }

    //method for click Listener
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivBackArrowGeneric -> {
                activity!!.onBackPressed()
            }
            R.id.ivProfileGeneric -> {
                CommonMethods.replaceFragmentInDrugsDetail(
                    fragmentManager!!,
                    MyProfileFragment()
                )
            }
        }
    }

    private fun showAllByGeneric() {
        genericMedList.clear()
        val demoList = ArrayList<AllData>()
        val allData = realm.where<AllData>().findAllAsync()
        demoList.addAll(allData)
        for (data in demoList) {
            if (data.generic_name != null)
                if (data.generic_name!!.equals(ApplicationGlobal.preferenceManager.getDrugsName()))
                    genericMedList.add(data)
        }
        adapter.notifyDataSetChanged()
    }
    fun filter(word: String) {
        val temArrayList = ArrayList<AllData>()
        for (d in genericMedList) {
            word.apply {
                if (d.name!!.toLowerCase().startsWith(word.toLowerCase()))
                    temArrayList.add(d)
            }
        }
        adapter.filter(temArrayList)
    }
    //get Generic Drugs
//    fun getdata(page: Int, boolean: Boolean, word: String) {
//        if (boolean) CommonMethods.showProgress(activity!!)
//        hashMap.put("language_id", ApplicationGlobal.english.toString().trim())
//        hashMap.put("page_count", page.toString().trim())
//        hashMap.put("generic_id", getId.toString().trim())
//        hashMap.put("search", word.trim())
//        RetrofitClient.getRetrofit().getMedicines(hashMap)
//            .enqueue(object : Callback<SystemResp> {
//                override fun onFailure(call: Call<SystemResp>, t: Throwable) {
//                    CommonMethods.dismissProgress()
//                    if (CommonMethods.phoneIsOnline(activity!!)) {
//                        CommonMethods.toast(activity!!, t.message!!)
//                    } else {
//                        CommonMethods.toast(activity!!, "Internet connection not available")
//                    }
//                }
//
//                override fun onResponse(call: Call<SystemResp>, response: Response<SystemResp>) {
//                    CommonMethods.dismissProgress()
//                    if (response.isSuccessful && response.body() != null) {
//                        if (response.body()!!.count > 0) {
//                            recyclerViewDrugsGeneric.visibility = View.VISIBLE
//                            tvNoDataGeneric.visibility = View.GONE
//                            if (page == 0) {
//                                if (response.body()!!.count > 0) {
//                                    pageCount = 0
//                                    recyclerViewDrugsGeneric.visibility = View.VISIBLE
//                                    tvNoDataGeneric.visibility = View.GONE
//                                    genericMedList.clear()
//                                    val a = response.body()!!.result
//                                    for (i in a.indices) {
//                                        genericMedList.add(a[i])
//                                    }
//                                    recyclerViewDrugsGeneric.adapter =
//                                        DrugsBySystemAdapter(
//                                            genericMedList,
//                                            activity!!
//                                        )
//                                    if (genericMedList.size == Constant.TOTAL_DATA) {
//                                        loading = true
//                                        pageCount++
//                                    }
//                                } else {
//                                    recyclerViewDrugsGeneric.visibility = View.GONE
//                                    tvNoDataGeneric.visibility = View.VISIBLE
//                                }
//                            } else {
//                                val newData = response.body()!!.result
//                                genericMedList.addAll(newData)
//                                recyclerViewDrugsGeneric.adapter!!.notifyDataSetChanged()
//                                progress_bar_generic.visibility = View.GONE
//                                Log.i("paginatoins", "" + genericMedList.size)
//                                if (genericMedList.size == (page + 1) * Constant.TOTAL_DATA) {
//                                    pageCount++
//                                    loading = true
//                                }
//                            }
//                        } else {
//                            recyclerViewDrugsGeneric.visibility = View.GONE
//                            tvNoDataGeneric.visibility = View.VISIBLE
//                        }
//                    } else {
//                        CommonMethods.getErrorMessageAndSessionExpire(
//                            activity!!,
//                            response.errorBody()!!
//                        )
////                        CommonMethods.toast(
////                            activity!!, "" +
////                                    CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
//                    }
//                }
//            })
    // }
}