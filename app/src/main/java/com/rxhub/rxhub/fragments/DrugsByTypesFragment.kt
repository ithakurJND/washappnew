package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.CategoryAdapter
import com.rxhub.rxhub.realmModels.*
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import com.rxhub.rxhub.webServices.model.ResultS
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.fragment_drugs_by_types.*
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/*
* This class is fragment_drugs_by_types container for all Drugs by categories
*
* created by GTB on 02/05/2020
*/
class DrugsByTypesFragment(val string: String) : BaseFragment() {
    val hashMap = HashMap<String, String>()
    var pageCount = 0
    lateinit var realm: Realm
    private var loading = false
    private var systemByList = ArrayList<ResultS>()
    private var diseaseByList = ArrayList<ResultS>()
    private var brandByList = ArrayList<ResultS>()
    private var genericByList = ArrayList<Result>()
    private lateinit var adapter: CategoryAdapter
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_drugs_by_types
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        realm = Realm.getDefaultInstance()
        val layoutManager = GridLayoutManager(activity!!, 2)
        recyclerViewDrugsByType.layoutManager = layoutManager



        Log.i("languageID", "" + ApplicationGlobal.preferenceManager.getLanguageId())
        if (string.equals("System")) {
            getByGenericNameFromRealm("System")
        } else if (string.equals("Disease")) {
            tvDrugsTypeName.setText(activity!!.resources.getString(R.string.drugsbyDisease))
            getByGenericNameFromRealm("Disease")
        } else if (string.equals("Brand")) {
            tvDrugsTypeName.setText(activity!!.resources.getString(R.string.drugsByBrand))
            getByGenericNameFromRealm("Brand")
        } else {
            tvDrugsTypeName.setText(activity!!.resources.getString(R.string.genericDrugs))
            //   getGeneric(0, "", true)
            getByGenericNameFromRealm("Generic")
        }
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        try {
            if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
                if (getUserDetails.profile_pic!!.startsWith("http")) {
                    CommonMethods.setUrlImage(
                        activity!!,
                        getUserDetails.profile_pic.toString(),
                        ivProfile
                    )
                } else {
                    CommonMethods.setServerImage(
                        activity!!,
                        getUserDetails.profile_pic.toString(),
                        ivProfile
                    )
                }
            } else {
                GlideApp.with(activity!!)
                    .load(R.drawable.placeholder_small)
                    .circleCrop()
                    .into(ivProfile)
            }
        } catch (e: Exception) {
        }
        etSearchByType.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    filter(s.toString())
//                    val searchText = s.toString()
//                    if (string.equals("System")) {
//                        filter(s.toString())
//                        //  getBySystem(0, searchText, false)
//                    } else if (string.equals("Disease")) {
//
//                        //getDisease(0, searchText, false)
//                    } else if (string.equals("Brand")) {
//
//                        //getBrand(0, searchText, false)
//                    } else {
//                        //  getGeneric(0, searchText, false)
//                    }
                } else {
                    adapter.filter(genericByList)
//                    if (string.equals("System")) {
//
//                        //getBySystem(0, "", false)
//                    } else if (string.equals("Disease")) {
//
//                        //getDisease(0, "", false)
//                    } else if (string.equals("Brand")) {
//
//                        // getBrand(0, "", false)
//                    } else {
//
//                        //getGeneric(0, "", false)
//                    }
                }
            }

        })
        //this for pagination
//        recyclerViewDrugsByType.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                if (dy > 0) //check for scroll down
//                {
//                    val visibleItemCount = layoutManager.getChildCount()
//                    val totalItemCount = layoutManager.getItemCount()
//                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
//                    if (loading) {
//                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
//                            loading = false
//                            Log.v("...", "Last Item Wow !")
//                            //Do pagination.. i.e. fetch new data
//                            progress_barSystem.visibility = View.VISIBLE
//                            if (etSearchByType.text.trim().isEmpty()) {
//                                if (string.equals("System")) {
////                                    progress_barSystem.visibility = View.VISIBLE
//                                    //getBySystem(pageCount, "", false)
//                                } else if (string.equals("Disease")) {
////                                    progress_barSystem.visibility = View.VISIBLE
//                                    //getDisease(pageCount, "", false)
//                                } else if (string.equals("Brand")) {
////                                    progress_barSystem.visibility = View.VISIBLE
//                                    //getBrand(pageCount, "", false)
//                                } else {
////                                    progress_barSystem.visibility = View.VISIBLE
//                                    //getGeneric(pageCount, "", false)
//                                }
//                            } else {
//                                if (string.equals("System")) {
////                                    progress_barSystem.visibility = View.VISIBLE
//                                    //getBySystem(pageCount, etSearchByType.text.toString(), false)
//                                } else if (string.equals("Disease")) {
////                                    progress_barSystem.visibility = View.VISIBLE
//                                    //getDisease(pageCount, etSearchByType.text.toString(), false)
//                                } else if (string.equals("Brand")) {
////                                    progress_barSystem.visibility = View.VISIBLE
//                                    // getBrand(pageCount, etSearchByType.text.toString(), false)
//                                } else {
////                                    progress_barSystem.visibility = View.VISIBLE
//                                    // getGeneric(pageCount, etSearchByType.text.toString(), false)
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        })
        ivBackArrow.setOnClickListener { activity!!.onBackPressed() }
        ivProfile.setOnClickListener {
            CommonMethods.replaceFragmentInSetting(
                fragmentManager!!,
                MyProfileFragment()
            )
        }

    }

//    fun getBySystem(page: Int, word: String, boolean: Boolean) {
//        if (boolean) {
//            CommonMethods.showProgress(activity!!)
//        }
//        hashMap.put("language_id", ApplicationGlobal.english.toString())
//        hashMap.put("page_count", page.toString().trim())
//        hashMap.put("search", word)
//
//        RetrofitClient.getRetrofit().getSystemCategory(
//            hashMap
//        )
//            .enqueue(object : Callback<SystemResp> {
//                override fun onFailure(call: Call<SystemResp>, t: Throwable) {
//                    CommonMethods.dismissProgress()
//                    if (CommonMethods.phoneIsOnline(activity!!)) {
//                        CommonMethods.toast(activity!!, t.message!!)
//                    } else {
//                        CommonMethods.toast(activity!!, "Internet connection not available")
//                    }
//                }
//
//                override fun onResponse(
//                    call: Call<SystemResp>,
//                    response: Response<SystemResp>
//                ) {
//                    CommonMethods.dismissProgress()
//                    if (response.isSuccessful) {
//                        try {
//                            if (page == 0) {
//                                if (response.body()!!.count > 0) {
//                                    pageCount = 0
//                                    recyclerViewDrugsByType.visibility = View.VISIBLE
//                                    tvNoData.visibility = View.GONE
//                                    systemByList.clear()
//                                    val res = response.body()!!.result
//                                    for (i in res.indices) {
//                                        systemByList.add(res[i])
//                                    }
//                                    recyclerViewDrugsByType.adapter =
//                                        CategoryAdapter(
//                                            systemByList,
//                                            null,
//                                            activity!!
//                                            , "System"
//                                        )
//                                    Log.i("adsfajsdfl;k", "" + systemByList)
//                                    if (systemByList.size == Constant.TOTAL_DATA) {
//                                        loading = true
//                                        pageCount++
//                                    }
//                                } else {
//                                    recyclerViewDrugsByType.visibility = View.GONE
//                                    tvNoData.visibility = View.VISIBLE
//                                }
//                            } else {
//                                val newData = response.body()!!.result
//                                systemByList.addAll(newData)
//                                recyclerViewDrugsByType.adapter!!.notifyDataSetChanged()
//                                progress_barSystem.visibility = View.GONE
//                                if (systemByList.size == (page + 1) * Constant.TOTAL_DATA) {
//                                    pageCount++
//                                    loading = true
//                                }
//                            }
//                        } catch (e: Exception) {
//                        }
//                    } else {
//                        Log.i("adsfajsdfl;k", "Failed")
//                        CommonMethods.getErrorMessageAndSessionExpire(
//                            activity!!,
//                            response.errorBody()!!
//                        )
////                        CommonMethods.sessionExpire(
////                            activity!!,
////                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
////                        CommonMethods.toast(
////                            activity!!,
////                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
//                    }
//                }
//
//            })
//    }

//    fun getDisease(page: Int, word: String, boolean: Boolean) {
//        if (boolean) CommonMethods.showProgress(activity!!)
//        hashMap.put("page_count", page.toString().trim())
//        hashMap.put("language_id", ApplicationGlobal.english.toString())
//        hashMap.put("search", word)
//        RetrofitClient.getRetrofit().getDiseaseCategory(hashMap)
//            .enqueue(object : Callback<SystemResp> {
//                override fun onFailure(call: Call<SystemResp>, t: Throwable) {
//                    CommonMethods.dismissProgress()
//                    if (CommonMethods.phoneIsOnline(activity!!)) {
//                        CommonMethods.toast(activity!!, t.message!!)
//                    } else {
//                        CommonMethods.toast(activity!!, "Internet connection not available")
//                    }
//                }
//
//                override fun onResponse(
//                    call: Call<SystemResp>,
//                    response: Response<SystemResp>
//                ) {
//                    try {
//                        CommonMethods.dismissProgress()
//                        if (response.isSuccessful) {
//                            if (page == 0) {
//                                if (response.body()!!.count > 0) {
//                                    pageCount = 0
//                                    recyclerViewDrugsByType.visibility = View.VISIBLE
//                                    tvNoData.visibility = View.GONE
//                                    diseaseByList.clear()
//                                    val res = response.body()!!.result
//                                    for (i in res.indices) {
//                                        diseaseByList.add(res[i])
//                                    }
//                                    recyclerViewDrugsByType.adapter =
//                                        CategoryAdapter(
//                                            diseaseByList,
//                                            null,
//                                            activity!!
//                                            , "Disease"
//                                        )
//                                    if (diseaseByList.size == Constant.TOTAL_DATA) {
//                                        loading = true
//                                        pageCount++
//                                    }
//                                } else {
//                                    recyclerViewDrugsByType.visibility = View.GONE
//                                    tvNoData.visibility = View.VISIBLE
//                                }
//                            } else {
//                                val newdata = response.body()!!.result
//                                diseaseByList.addAll(newdata)
//                                recyclerViewDrugsByType.adapter!!.notifyDataSetChanged()
//                                progress_barSystem.visibility = View.GONE
//                                if (diseaseByList.size == (page + 1) * Constant.TOTAL_DATA) {
//                                    pageCount++
//                                    loading = true
//                                }
//                            }
//                        } else {
////                        CommonMethods.toast(
////                            activity!!,
////                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
////                        CommonMethods.sessionExpire(
////                            activity!!,
////                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
//                            CommonMethods.getErrorMessageAndSessionExpire(
//                                activity!!,
//                                response.errorBody()!!
//                            )
//                        }
//                    } catch (e: Exception) {
//                    }
//                }
//            })
//    }

//    fun getBrand(page: Int, word: String, boolean: Boolean) {
//        Log.i("paginatoins", " pageCount " + page)
//        if (boolean) CommonMethods.showProgress(activity!!)
//        hashMap.put("page_count", page.toString().trim())
//        hashMap.put("language_id", ApplicationGlobal.english.toString())
//        hashMap.put("search", word)
//        RetrofitClient.getRetrofit().getBrandCategory(hashMap)
//            .enqueue(object : Callback<SystemResp> {
//                override fun onFailure(call: Call<SystemResp>, t: Throwable) {
//                    CommonMethods.dismissProgress()
//                    if (CommonMethods.phoneIsOnline(activity!!)) {
//                        CommonMethods.toast(activity!!, t.message!!)
//                    } else {
//                        CommonMethods.toast(activity!!, "Internet connection not available")
//                    }
//                }
//
//                override fun onResponse(
//                    call: Call<SystemResp>,
//                    response: Response<SystemResp>
//                ) {
//                    CommonMethods.dismissProgress()
//                    if (response.isSuccessful && response.body() != null) {
//                        try {
//                            if (page == 0) {
//                                if (response.body()!!.count > 0) {
//                                    pageCount = 0
//                                    recyclerViewDrugsByType.visibility = View.VISIBLE
//                                    tvNoData.visibility = View.GONE
//                                    brandByList.clear()
//                                    val res = response.body()!!.result
//                                    for (i in res.indices) {
//                                        brandByList.add(res[i])
//                                    }
//                                    recyclerViewDrugsByType.adapter =
//                                        CategoryAdapter(
//                                            brandByList,
//                                            null,
//                                            activity!!
//                                            , "Brand"
//                                        )
//                                    Log.i("paginatoins", "" + brandByList.size)
//                                    if (brandByList.size == Constant.TOTAL_DATA) {
//                                        loading = true
//                                        pageCount++
//                                    }
//                                } else {
//                                    recyclerViewDrugsByType.visibility = View.GONE
//                                    tvNoData.visibility = View.VISIBLE
//                                }
//                            } else {
//                                progress_barSystem.visibility = View.GONE
//                                val newData = response.body()!!.result
//                                brandByList.addAll(newData)
//                                recyclerViewDrugsByType.adapter!!.notifyDataSetChanged()
//                                Log.i("paginatoins", "" + brandByList.size)
//                                if (brandByList.size == (page + 1) * Constant.TOTAL_DATA) {
//                                    pageCount++
//                                    loading = true
//                                }
//                            }
//                        } catch (e: Exception) {
//                        }
//                    } else {
////                        CommonMethods.toast(
////                            activity!!,
////                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
////                        CommonMethods.sessionExpire(
////                            activity!!,
////                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
//                        CommonMethods.getErrorMessageAndSessionExpire(
//                            activity!!,
//                            response.errorBody()!!
//                        )
//                    }
//                }
//            })
//    }

//    fun getGeneric(page: Int, word: String, boolean: Boolean) {
//        if (boolean) CommonMethods.showProgress(activity!!)
//        hashMap.put("page_count", page.toString().trim())
//        hashMap.put("language_id", ApplicationGlobal.english.toString())
//        hashMap.put("search", word)
//        RetrofitClient.getRetrofit().getGenricDrugs(hashMap)
//            .enqueue(object : Callback<SystemResp> {
//                override fun onFailure(call: Call<SystemResp>, t: Throwable) {
//                    CommonMethods.dismissProgress()
//                    if (CommonMethods.phoneIsOnline(activity!!)) {
//                        CommonMethods.toast(activity!!, t.message!!)
//                    } else {
//                        CommonMethods.toast(activity!!, "Internet connection not available")
//                    }
//                }
//
//                override fun onResponse(
//                    call: Call<SystemResp>,
//                    response: Response<SystemResp>
//                ) {
//                    try {
//                        CommonMethods.dismissProgress()
//                        if (response.isSuccessful) {
//                            if (page == 0
//                            ) {
//                                if (response.body()!!.count > 0
//                                ) {
//                                    pageCount = 0
//                                    recyclerViewDrugsByType.visibility = View.VISIBLE
//                                    tvNoData.visibility = View.GONE
//                                    genericByList.clear()
//                                    val res = response.body()!!.result
//                                    for (i in res.indices) {
//                                        genericByList.add(res[i])
//                                    }
//                                    recyclerViewDrugsByType.adapter =
//                                        CategoryAdapter(
//                                            genericByList,
//                                            activity!!
//                                            , "Generic"
//                                        )
//                                    if (genericByList.size == Constant.TOTAL_DATA) {
//                                        loading = true
//                                        pageCount++
//                                    }
//                                } else {
//                                    recyclerViewDrugsByType.visibility = View.GONE
//                                    tvNoData.visibility = View.VISIBLE
//                                }
//                            } else {
//                                val newData = response.body()!!.result
//                                genericByList.addAll(newData)
//                                recyclerViewDrugsByType.adapter!!.notifyDataSetChanged()
//                                progress_barSystem.visibility = View.GONE
//                                Log.i("paginatoins", "" + genericByList.size)
//                                if (genericByList.size == (page + 1) * Constant.TOTAL_DATA) {
//                                    pageCount++
//                                    loading = true
//                                }
//                            }
//                        } else {
////                        CommonMethods.toast(
////                            activity!!,
////                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
////                        CommonMethods.sessionExpire(
////                            activity!!,
////                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
//                            CommonMethods.getErrorMessageAndSessionExpire(
//                                activity!!,
//                                response.errorBody()!!
//                            )
//                        }
//                    } catch (e: Exception) {
//                    }
//                }
//            })
//    }

    private fun getByGenericNameFromRealm(drug: String) {
        genericByList.clear()
        val genericNameData = realm.where<GenericName>().findAllAsync()
        val systemCategory = realm.where<SystemCategory>().findAllAsync()
        val diseaseCategory = realm.where<DiseaseCategory>().findAllAsync()
        val brandCategory = realm.where<BrandCategory>().findAllAsync()
        try {
            when (drug) {
                "System" -> {
                    genericByList.addAll(systemCategory[0]!!.result)
                }
                "Generic" -> {
                    genericByList.addAll(genericNameData[0]!!.result)
                }
                "Disease" -> {
                    genericByList.addAll(diseaseCategory[0]!!.result)
                }
                "Brand" -> {
                    genericByList.addAll(brandCategory[0]!!.result)
                }
            }
        } catch (e: Exception) {
            Log.i("IndicationNameList","${e.message}")
        }
            Collections.sort(genericByList, Comparator { obj1, obj2 ->
                // ## Ascending order
                obj1.name!!.trim().toLowerCase().compareTo(obj2.name!!.trim().toLowerCase(), false) // To compare string values
                // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
                // ## Descending order
                // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
                // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
            })
        adapter = CategoryAdapter(
            genericByList,
            activity!!
            , drug
        )
        recyclerViewDrugsByType.adapter = adapter
    }

    fun filter(word: String) {
        val temArrayList = ArrayList<Result>()
        for (d in genericByList) {
            word.apply {
                if (d.name!!.toLowerCase().startsWith(word.toLowerCase()))
                    temArrayList.add(d)
            }
        }
        Log.i("FilteredList", "${temArrayList}")
        adapter.filter(temArrayList)
    }
}