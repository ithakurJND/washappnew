package com.rxhub.rxhub.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.rxhub.rxhub.R
import com.rxhub.rxhub.utils.CommonMethods
import kotlinx.android.synthetic.main.fragment_tutorial.*

/*
* This is Tutorial screens container
*
* created by GTB on 14/04/2020
*/
class TutorialFragment : BaseFragment() {
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_tutorial
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivFirstDot.setImageResource(R.drawable.tutorial_dot_layout)
        btnNext.setOnClickListener {
            if (btnNext.text.equals("Next")) {
                viewPager.setCurrentItem(viewPager.currentItem + 1, true)
            } else {
                CommonMethods.replaceFragmentInSplashScreen(fragmentManager!!, LogInFragment())
            }
        }
        with(viewPager) {
            adapter = ViewPagerAdapter(childFragmentManager)
            addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {

                }

                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                }

                override fun onPageSelected(position: Int) {
                    when (position) {
                        0 -> {
                            setDefaultRing()
                            ivFirstDot.setImageResource(R.drawable.tutorial_dot_layout)
                            btnNext.setText(R.string.next)
                        }
                        1 -> {
                            setDefaultRing()
                            ivSecondDot.setImageResource(R.drawable.tutorial_dot_layout)
                            btnNext.setText(R.string.next)
                        }
                        2 -> {
                            setDefaultRing()
                            ivThirdDot.setImageResource(R.drawable.tutorial_dot_layout)
                            btnNext.setText(R.string.getstarted)
                        }
                    }
                }
            })
        }
    }

    inner class ViewPagerAdapter(fragmentManager: FragmentManager) :
        FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        override fun getItem(position: Int): Fragment {
            return TutorialScreenFragment(position)
        }

        override fun getCount(): Int {
            return 3
        }
    }

    fun setDefaultRing() {
        ivFirstDot.setImageResource(R.drawable.tutorial_ring_layout)
        ivSecondDot.setImageResource(R.drawable.tutorial_ring_layout)
        ivThirdDot.setImageResource(R.drawable.tutorial_ring_layout)
    }
}