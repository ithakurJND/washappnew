package com.rxhub.rxhub.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.SettingContainerActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.viewmodels.CommonViewModel
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import com.rxhub.rxhub.webServices.model.BasicResponse
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_settings.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/*
* This class is Settings screen Fragment
*
* created by GTB on 15/04/2020
*/
class SettingsFragment : BaseFragment(), View.OnClickListener {
    val hashMap = HashMap<String, Any>()

    //0 for on 1 for off
    var saveHistory = 0
    var commonViewModel = CommonViewModel()
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_settings
    }

    val historyStatus = Observer<BasicResponse>
    {

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        try {
            val userData = Gson().fromJson(
                ApplicationGlobal.preferenceManager.getBasicDetails(),
                BasicInfoResp::class.java
            )
            if (userData.is_notification_enable != null) {
                if (userData.is_notification_enable == 1) {
                    ivNotification.setImageResource(R.drawable.not_active)
                } else {
                    ivNotification.setImageResource(R.drawable.active)
                }
            }
            if (userData.is_history_enable != null) {
                if (userData.is_history_enable == 1) {
                    ivMedicineHistory.setImageResource(R.drawable.not_active)
                } else {
                    ivMedicineHistory.setImageResource(R.drawable.active)
                }
            }

            Log.i("userdata", "" + userData)
            if (userData.fb_id!!.isNotEmpty() || userData.google_id!!.isNotEmpty()) {
                tvChangePass.visibility = View.GONE
                viewSetting.visibility = View.GONE
            }
        } catch (e: Exception) {
        }
        commonViewModel = ViewModelProvider(this).get(CommonViewModel::class.java)
        commonViewModel.basicResponseLiveData.observe(viewLifecycleOwner, historyStatus)
        commonViewModel.getValue(activity!!)


        //click Listener
        tvAbout.setOnClickListener(this)
        tvPrivacy.setOnClickListener(this)
        tvTerms.setOnClickListener(this)
        tvChangePass.setOnClickListener(this)
        ivNotification.setOnClickListener(this)
        ivProfile.setOnClickListener(this)
        ivMenu.setOnClickListener(this)
        tvAcknowledgement.setOnClickListener(this)
        tvReference.setOnClickListener(this)
        ivMedicineHistory.setOnClickListener(this)
        //set profile image
        setImage()
    }

    //this method for all click Listener
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.tvAbout -> {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "AboutUs")
                )
            }
            R.id.tvPrivacy -> {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "privay")
                )
            }
            R.id.tvTerms -> {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "Terms")
                )
            }
            R.id.tvChangePass -> {
                if (CommonMethods.phoneIsOnline(activity!!))
                    startActivity(
                        Intent(activity!!, SettingContainerActivity::class.java)
                            .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .putExtra("SettingScreen", "ChangePassword")
                    )
                else
                    Toast.makeText(activity, "No internet connection", Toast.LENGTH_LONG).show()
            }
            R.id.ivNotification -> {
                if (CommonMethods.phoneIsOnline(activity!!)) {
                    if (ivNotification.drawable.constantState ==
                        activity!!.resources.getDrawable(R.drawable.not_active).constantState
                    ) {
                        ivNotification.setImageResource(R.drawable.active)
                        val enable = 0
                        Toast.makeText(activity, "Notification status updated", Toast.LENGTH_LONG).show()
                        setNotifications(enable)
                    } else {
                        ivNotification.setImageResource(R.drawable.not_active)
                        val disable = 1
                        Toast.makeText(activity, "Notification status updated", Toast.LENGTH_LONG).show()
                        setNotifications(disable)
                    }
                } else
                    Toast.makeText(activity, "No internet connection", Toast.LENGTH_LONG).show()
            }
            R.id.ivMedicineHistory -> {
                if (CommonMethods.phoneIsOnline(activity!!)) {
                    if (ivMedicineHistory.drawable.constantState ==
                        activity!!.resources.getDrawable(R.drawable.not_active).constantState
                    ) {
                        ivMedicineHistory.setImageResource(R.drawable.active)
                        // 0 for on
                        commonViewModel.setHistoryStatus(0)
                    } else {
                        ivMedicineHistory.setImageResource(R.drawable.not_active)
                         // 1 for off
                        commonViewModel.setHistoryStatus(1)
                    }
                } else
                    Toast.makeText(activity, "No internet connection", Toast.LENGTH_LONG).show()
            }
            R.id.ivProfile -> {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "myProfile")
                )
            }
            R.id.ivMenu -> {
                activity!!.drawer_layout.openDrawer(GravityCompat.START)
            }
            R.id.tvAcknowledgement -> {
                if (CommonMethods.phoneIsOnline(activity!!))
                    startActivity(
                        Intent(activity!!, SettingContainerActivity::class.java)
                            .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .putExtra("SettingScreen", "Acknowledgement")
                    )
                else
                    Toast.makeText(activity, "No internet connection", Toast.LENGTH_LONG).show()
            }
            R.id.tvReference -> {
                if (CommonMethods.phoneIsOnline(activity!!))
                    startActivity(
                        Intent(activity!!, SettingContainerActivity::class.java)
                            .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            .putExtra("SettingScreen", "Reference")
                    )
                else
                    Toast.makeText(activity, "No internet connection", Toast.LENGTH_LONG).show()
            }

        }
    }

    //this method for on and off notification
    fun setNotifications(notification: Int) {
        hashMap.put("is_notification_enable", notification)
        RetrofitClient.getRetrofit().setBasicInfo(hashMap)
            .enqueue(object : Callback<BasicResponse> {
                override fun onFailure(call: Call<BasicResponse>, t: Throwable) {
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.toast(activity!!, t.message!!)
                    } else {
                        CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                }

                override fun onResponse(
                    call: Call<BasicResponse>,
                    response: Response<BasicResponse>
                ) {
                    if (response.isSuccessful) {
                        Log.i("notificationResponse", response.body()!!.message)
//                        CommonMethods.toast(activity!!, response.message())
                    } else {
                        CommonMethods.getErrorMessageAndSessionExpire(
                            activity!!,
                            response.errorBody()!!
                        )
                    }
                }

            })
    }

    fun setImage() {
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails!!.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails!!.profile_pic.toString(),
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails!!.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
    }

    override fun onResume() {
        super.onResume()
        setImage()
    }
}