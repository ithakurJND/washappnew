package com.rxhub.rxhub.fragments

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import com.hbb20.CountryCodePicker
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.MainActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.*
import kotlinx.android.synthetic.main.fragment_basic_information.*
import kotlinx.android.synthetic.main.fragment_basic_information.counterCode
import kotlinx.android.synthetic.main.fragment_basic_information.etCompanyName
import kotlinx.android.synthetic.main.fragment_basic_information.etPhoneNumber
import kotlinx.android.synthetic.main.fragment_my_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/*
* This class is use to add Basic Information of user
*
*created by GTB on 15/04/2020
*/
class BasicInformationFragment : BaseFragment(), View.OnClickListener {
    lateinit var listPopupWindowDesig: ListPopupWindow
    lateinit var listPopupWindowDesignationCat: ListPopupWindow
    lateinit var getArgumentA: String
    lateinit var getArgumentName: String
    var selected_country_code = ""
    var companyDesignationList = arrayListOf<Result>()
    val hashMap = HashMap<String, Any>()
    val hashMapDesignations = HashMap<String, Any>()
    val designationName = arrayListOf<String>()
    var designationId = ""
    var pageCount = 0
    val designationCategoryName = arrayListOf<String>()
    var designationCatId = ""
    val designationCatList = arrayListOf<ResultCat>()

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_basic_information
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getArgumentA = arguments!!.getString("SignUp").toString()
        getArgumentName = arguments!!.getString("UserName").toString()

        tvSelectGender.setOnClickListener(this)
        tvSelectDesignation.setOnClickListener(this)
        tvSkip.setOnClickListener(this)
        btnSubmit.setOnClickListener(this)
        tvSelectDesignationCategory.setOnClickListener(this)
        tvbasicInfo.paintFlags = tvbasicInfo.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        CommonMethods.dropDownList(activity!!, Constant.GENDER_LIST, tvSelectGender)
        try {
            CommonMethods.showProgress(activity!!)
            RetrofitClient.getRetrofit().getDesignationCategory()
                .enqueue(object : Callback<DesignationCategoryResponse> {
                    override fun onFailure(call: Call<DesignationCategoryResponse>, t: Throwable) {
                        CommonMethods.dismissProgress()
                        if (CommonMethods.phoneIsOnline(activity!!)) {
                            CommonMethods.toast(activity!!, t.message!!)
                        } else {
                            CommonMethods.toast(activity!!, "Internet connection not available")
                        }
                    }

                    override fun onResponse(
                        call: Call<DesignationCategoryResponse>,
                        response: Response<DesignationCategoryResponse>
                    ) {
                        CommonMethods.dismissProgress()
                        if (response.isSuccessful && response.body() != null) {
                            val list = response.body()!!.result
                            Log.i("asdfasdfasfdasfd", "" + list)
                            for (i in list.indices) {
                                designationCatList.add(list[i])
                                designationCategoryName.add(list[i].name)
                            }
                        } else {
                            CommonMethods.getErrorMessageAndSessionExpire(
                                activity!!,
                                response.errorBody()!!
                            )
                        }
                    }
                })
        } catch (e: Exception) {
        }

        listPopupWindowDesig = ListPopupWindow(activity!!)
        listPopupWindowDesig.setAdapter(
            ArrayAdapter(
                activity!!,
                android.R.layout.simple_dropdown_item_1line,
                designationName
            )
        )
        listPopupWindowDesig.anchorView = tvSelectDesignation
        listPopupWindowDesig.isModal = true
        listPopupWindowDesig.setOnItemClickListener { parent, view, position, id ->
            tvSelectDesignation.setText(designationName[position])
            designationId = companyDesignationList[position].id.toString()
            listPopupWindowDesig.dismiss()
        }
//this is for Designation Category
        listPopupWindowDesignationCat = ListPopupWindow(activity!!)
        listPopupWindowDesignationCat.setAdapter(
            ArrayAdapter(
                activity!!,
                android.R.layout.simple_dropdown_item_1line,
                designationCategoryName
            )
        )
        listPopupWindowDesignationCat.anchorView = tvSelectDesignationCategory
        listPopupWindowDesignationCat.isModal = true
        listPopupWindowDesignationCat.setOnItemClickListener { parent, view, position, id ->
            tvSelectDesignationCategory.setText(designationCategoryName[position])
            designationCatId = designationCatList[position].id.toString()
            tvSelectDesignation.text = ""
            try {
            CommonMethods.showProgress(activity!!)
            hashMapDesignations.put("designation_category_id", designationCatId)
            hashMapDesignations.put("language_id", ApplicationGlobal.english.toString())
            hashMapDesignations.put("page_count", pageCount.toString().trim())
            RetrofitClient.getRetrofit().getDesignation(hashMapDesignations)
                .enqueue(object : Callback<Designations> {
                    override fun onFailure(call: Call<Designations>, t: Throwable) {
                        CommonMethods.dismissProgress()
                        if (CommonMethods.phoneIsOnline(activity!!)) {
                            CommonMethods.toast(activity!!, t.message!!)
                        } else {
                            CommonMethods.toast(activity!!, "Internet connection not available")
                        }
                    }

                    override fun onResponse(
                        call: Call<Designations>,
                        response: Response<Designations>
                    ) {
                        CommonMethods.dismissProgress()
                        if (response.isSuccessful) {
                            val desi = response.body()!!.result
                            companyDesignationList.clear()
                            designationName.clear()
                            Log.i("asdfalsd", "" + response.body()!!)
                            for (i in desi.indices) {
                                companyDesignationList.add(desi[i])
                                designationName.add(desi[i].name)
                            }
                        } else {
                        CommonMethods.toast(
                            activity!!,
                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
                        )
                        }
                    }
                })
            }catch (e:Exception){}
            listPopupWindowDesignationCat.dismiss()
        }
        //this is for get Country Code ClickListener
        selected_country_code = counterCode.selectedCountryCodeWithPlus
        counterCode.setOnCountryChangeListener(object : CountryCodePicker.OnCountryChangeListener {
            override fun onCountrySelected() {
                //Alert.showMessage(RegistrationActivity.this, ccp.getSelectedCountryCodeWithPlus());
                selected_country_code = counterCode.getSelectedCountryCodeWithPlus()
                Log.i("countryCode", "" + selected_country_code)
            }
        })
        Log.i("authority", "" + ApplicationGlobal.preferenceManager.getSessionId())
    }


    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.tvSelectGender -> {
                CommonMethods.hideKeyboard(activity!!)
                clearFocus()
                CommonMethods.showDropDownList()
            }
            R.id.tvSelectDesignation -> {
                if (tvSelectDesignationCategory.text.trim().isNotEmpty()) {
                    CommonMethods.hideKeyboard(activity!!)
                    clearFocus()
                    listPopupWindowDesig.show()
                } else {
                    CommonMethods.toast(activity!!, "Please Select Designation Category")
                }
            }
            R.id.btnSubmit -> {
                CommonMethods.hideKeyboard(activity!!)
                clearFocus()
                Log.i("accesstoken", "" + ApplicationGlobal.preferenceManager.getSessionId())
                val genderId: Int

                if (selected_country_code.trim().isEmpty()) {
                    CommonMethods.toast(activity!!, "Country Code is Required")
                } else if (etPhoneNumber.text.toString().trim().isEmpty()) {
                    etPhoneNumber.requestFocus()
                    etPhoneNumber.error = "Phone number is required"
                    //CommonMethods.toast(activity!!, "Phone number is required")
                } else if (!CommonMethods.isValidPhone(etPhoneNumber.text.toString().trim())) {
                    etPhoneNumber.requestFocus()
                    etPhoneNumber.error = "Phone number is not valid"
                   // CommonMethods.toast(activity!!, "Phone number is not valid")
                } else if (etBasicInfoAge.text.toString().trim().isEmpty()) {
                    etBasicInfoAge.requestFocus()
                    etBasicInfoAge.error = "Please Enter Age"
                    CommonMethods.toast(activity!!, "Please Enter Age")
                } else if (tvSelectGender.text.toString().trim().isEmpty()) {
                    tvSelectGender.requestFocus()
                    tvSelectGender.error = "Please Select Gender"
                   // CommonMethods.toast(activity!!, "Please Select Gender")
                }
                else if (tvSelectDesignationCategory.text.toString().trim().isEmpty()) {
                    tvSelectDesignationCategory.requestFocus()
                    tvSelectDesignationCategory.error = "Please Select Profession"
                   // CommonMethods.toast(activity!!, "Please Select Profession")
            }

//            else if (tvSelectDesignationCategory.text.toString().trim().isEmpty()) {
//                    CommonMethods.toast(activity!!, "Company Name is required")
//                }

                else {
                    if (tvSelectGender.text.equals("Male")) {
                        genderId = 2
                    } else {
                        genderId = 1
                    }
                    CommonMethods.showProgress(activity!!)
                    if (tvSelectDesignation.text.toString().isNotEmpty())
                        hashMap.put("designation_id", designationId.trim())
                    hashMap.put("name", getArgumentName)
                    hashMap.put("mobile_number", etPhoneNumber.text.toString().trim())
                    hashMap.put("age", etBasicInfoAge.text.toString().trim().toInt())
                    hashMap.put("gender", genderId.toString().trim().toInt())
                    hashMap.put("designation_category_id", designationCatId.trim().toInt())
                    hashMap.put("country_code", selected_country_code.trim())
                    if (etCompanyName.text.toString().isNotEmpty())
                    hashMap.put("company_name", etCompanyName.text.toString().trim())

                    RetrofitClient.getRetrofit().setBasicInfo(hashMap)
                        .enqueue(object : Callback<BasicResponse> {
                            override fun onFailure(call: Call<BasicResponse>, t: Throwable) {
                                CommonMethods.dismissProgress()
                                if (CommonMethods.phoneIsOnline(activity!!)) {
                                    CommonMethods.toast(activity!!, t.message!!)
                                } else {
                                    CommonMethods.toast(
                                        activity!!,
                                        "Internet connection not available"
                                    )
                                }
                            }

                            override fun onResponse(
                                call: Call<BasicResponse>,
                                response: Response<BasicResponse>
                            ) {
                                CommonMethods.dismissProgress()
                                if (response.isSuccessful) {
                                    Log.i("aasdfasdf", "" + response.body()!!)
                                    val intent = Intent(activity!!, MainActivity::class.java)
                                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                    startActivity(intent)
                                    activity!!.finish()
                                } else {
//                                    CommonMethods.getErrorMessageAndSessionExpire(
//                                        activity!!,
//                                        response.errorBody()!!
//                                    )
                                    CommonMethods.toast(
                                        activity!!,
                                        "${CommonMethods.getErrorMessage(response.errorBody()!!)}"
                                    )
//                                    try {
//                                        val jObjError =
//                                            JSONObject(response.errorBody()!!.string())
//                                        Toast.makeText(
//                                            context,
//                                            jObjError.getJSONObject("error").getString("message"),
//                                            Toast.LENGTH_LONG
//                                        ).show()
//                                    } catch (e: java.lang.Exception) {
//                                        Toast.makeText(context, e.message, Toast.LENGTH_LONG)
//                                            .show()
//                                    }
                                }
                            }

                        })
                }
            }
            R.id.tvSkip -> {
                val intent = Intent(activity!!, MainActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                startActivity(intent)
                activity!!.finish()
            }
            R.id.tvSelectDesignationCategory -> {
                CommonMethods.hideKeyboard(activity!!)
                clearFocus()
                listPopupWindowDesignationCat.show()
            }
        }
    }

    fun clearFocus() {
        etPhoneNumber.clearFocus()
        etCompanyName.clearFocus()
        etBasicInfoAge.clearFocus()
    }
}