package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.webkit.WebViewClient
import com.rxhub.rxhub.R
import kotlinx.android.synthetic.main.fragment_web_view.*

/*
* This class fragment_web_view show Web View in app
*
* create by GTB on 21/04/2020
 */
class WebViewFragment(private val title: String) : BaseFragment() {
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_web_view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivBackArrow.setOnClickListener {
            activity!!.onBackPressed()
        }
        tvWebView.text = title
        webView.webViewClient = WebViewClient()
        webView.loadUrl("https://www.google.in")
    }
}