package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.DrugsBySystemAdapter
import com.rxhub.rxhub.realmModels.AllData
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.fragment_brand_medicine.*
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/*
* This class fragment_brand_medicine show the Medicines by selected Brand.
*
* create by GTB on 30/04/2020
 */
class BrandMedicineFragment : BaseFragment(), View.OnClickListener {
    val hashMap = HashMap<String, String>()
    var pageCount = 0
    var getId = ""
    private var loading = false
    lateinit var realm: Realm
    lateinit var adapter: DrugsBySystemAdapter
    val brandMedList = ArrayList<AllData>()
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_brand_medicine
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        realm = Realm.getDefaultInstance()
        //all click listeners
        ivProfile.setOnClickListener(this)
        ivBackArrow.setOnClickListener(this)
        //get Brand id
        getId = ApplicationGlobal.preferenceManager.getDrugsId().toString()
        //set Brand Name
        tvDrugsTypeName.text = ApplicationGlobal.preferenceManager.getDrugsName()

        //set Layout Manager for Recycler View
        val layoutManager = GridLayoutManager(activity!!, 2)
        recyclerViewDrugsBrand.layoutManager = layoutManager

        //get Medicine By Brand
        //getMedicine(0, true, "")

        //this is search box Edit Text
        etSearchBrand.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty())
                    filter(s.toString())
                // getMedicine(0, false, s.toString())
                else {
                    adapter.filter(brandMedList)
                }
                //getMedicine(0, false, "")
            }
        })

        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails!!.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails!!.profile_pic.toString(),
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails!!.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
        //this for pagination
//        recyclerViewDrugsBrand.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                if (dy > 0) //check for scroll down
//                {
//                    val visibleItemCount = layoutManager.getChildCount()
//                    val totalItemCount = layoutManager.getItemCount()
//                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
//                    if (loading) {
//                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
//                            loading = false
//                            Log.v("...", "Last Item Wow !")
//                            //Do pagination.. i.e. fetch new data
//                            progress_barBrand.visibility = View.VISIBLE
//                            if (etSearchBrand.text.trim().isEmpty())
//                                getMedicine(pageCount, false, "")
//                            else getMedicine(
//                                pageCount,
//                                false,
//                                etSearchBrand.text.trim().toString()
//                            )
//                        }
//                    }
//                }
//            }
//        })
        showAllByBrand()
    }

    //click listener Method
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivProfile -> {
                CommonMethods.replaceFragmentInDrugsDetail(
                    fragmentManager!!,
                    MyProfileFragment()
                )
            }
            R.id.ivBackArrow -> {
                activity!!.onBackPressed()
            }
        }
    }

    private fun showAllByBrand() {
        brandMedList.clear()
        val demoList = ArrayList<AllData>()
        val allData = realm.where<AllData>().findAllAsync()
        demoList.addAll(allData)
        for (data in demoList) {
            if (data.brand_name != null)
                if (data.brand_name!!.equals(ApplicationGlobal.preferenceManager.getDrugsName()))
                    brandMedList.add(data)
        }
        Log.i("AllDrugsIn","$brandMedList")
        Collections.sort(brandMedList, Comparator { obj1, obj2 ->
            // ## Ascending order
            obj1.name!!.toLowerCase().compareTo(obj2.name!!.toLowerCase(), false) // To compare string values
            // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
            // ## Descending order
            // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
            // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
        })
        adapter = DrugsBySystemAdapter(brandMedList, activity!!)
        recyclerViewDrugsBrand.adapter = adapter
    }

    fun filter(word: String) {
        val temArrayList = ArrayList<AllData>()
        for (d in brandMedList) {
            word.apply {
                if (d.name!!.toLowerCase().startsWith(word.toLowerCase()))
                    temArrayList.add(d)
            }
        }
        adapter.filter(temArrayList)
    }
    //this method get Medicines by Brands
//    fun getMedicine(page: Int, boolean: Boolean, word: String) {
//        if (boolean) {
//            CommonMethods.showProgress(activity!!)
//        }
//        hashMap.put("language_id", ApplicationGlobal.english.toString())
//        hashMap.put("page_count", page.toString().trim())
//        hashMap.put("brand_id", getId.toString().trim())
//        hashMap.put("search", word.trim())
//        RetrofitClient.getRetrofit().getMedicines(hashMap)
//            .enqueue(object : Callback<SystemResp> {
//                override fun onFailure(call: Call<SystemResp>, t: Throwable) {
//                    CommonMethods.dismissProgress()
//                    if (CommonMethods.phoneIsOnline(activity!!)) {
//                        CommonMethods.handelInternet(activity!!, t.message!!)
//                    } else {
//                        CommonMethods.toast(activity!!, "Internet connection not available")
//                    }
//
//                }
//
//                override fun onResponse(call: Call<SystemResp>, response: Response<SystemResp>) {
//                    try {
//
//                        if (response.isSuccessful && response.body() != null) {
//                            if (page == 0) {
//                                if (response.body()!!.count > 0) {
//                                    pageCount = 0
//                                    tvNoDataBrand.visibility = View.GONE
//                                    recyclerViewDrugsBrand.visibility = View.VISIBLE
//                                    brandMedList.clear()
//                                    val a = response.body()!!.result
//                                    for (i in a.indices) {
//                                        brandMedList.add(a[i])
//                                    }
//                                    recyclerViewDrugsBrand.adapter =
//                                        DrugsBySystemAdapter(
//                                            brandMedList,
//                                            activity!!
//                                        )
//                                    if (brandMedList.size == Constant.TOTAL_DATA) {
//                                        loading = true
//                                        pageCount++
//                                    }
//                                } else {
//                                    tvNoDataBrand.visibility = View.VISIBLE
//                                    recyclerViewDrugsBrand.visibility = View.GONE
//                                }
//                            } else {
//
//                                val newData = response.body()!!.result
//                                brandMedList.addAll(newData)
//                                recyclerViewDrugsBrand.adapter!!.notifyDataSetChanged()
//                                progress_barBrand.visibility = View.GONE
//                                Log.i("paginatoins", "" + brandMedList.size)
//                                if (brandMedList.size == (page + 1) * Constant.TOTAL_DATA) {
//                                    pageCount++
//                                    loading = true
//                                }
//                            }
//                        } else {
//                            CommonMethods.getErrorMessageAndSessionExpire(
//                                activity!!,
//                                response.errorBody()!!
//                            )
////                        CommonMethods.toast(
////                            activity!!, "" +
////                                    CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
//                        }
//                        CommonMethods.dismissProgress()
//                    } catch (e: Exception) {
//                    }
//                }
//            })
}
