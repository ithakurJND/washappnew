package com.rxhub.rxhub.fragments

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.DrugsDetailsActivity
import com.rxhub.rxhub.activities.SettingContainerActivity
import com.rxhub.rxhub.adapters.NewAddedDrugs
import com.rxhub.rxhub.realmModels.*
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.viewmodels.CommonViewModel
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import com.rxhub.rxhub.webServices.model.HomeImageResp
import com.rxhub.rxhub.webServices.model.SyncAllDbData
import com.rxhub.rxhub.webServices.model.SyncDbData
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/*
* This class fragment_home is show App Home screen
*
* created by GTB on 15/04/2020
*/
class HomeFragment : BaseFragment(), View.OnClickListener {
    val hashMap = HashMap<String, String>()
    val hashMapNewDrugs = HashMap<String, String>()
    val newDrugsList = ArrayList<Result>()
    var getUserDetails: BasicInfoResp? = null
    val pageCount = 0
    var hitApisAgain = false
    var homeImageResp: HomeImageResp? = null
    var syncDbData = ArrayList<SyncDbData>()
    lateinit var newAddedDrugsAdapter: NewAddedDrugs
    var commonViewModel = CommonViewModel()
    private lateinit var realm: Realm
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_home
    }


    private val genericName = Observer<GenericName> { list ->
        realm.executeTransaction {
            val genericNameData = realm.where<GenericName>().findAllAsync()
            realm.where(GenericName::class.java).findAll().deleteAllFromRealm()
            it.insert(list)
        }
        commonViewModel.getSystemData()
    }
    private val systemCategory = Observer<SystemCategory> { list ->
        realm.executeTransaction {
            val systemCategory = realm.where<SystemCategory>().findAllAsync()
            realm.where(SystemCategory::class.java).findAll().deleteAllFromRealm()
            it.insert(list)
        }
        commonViewModel.getByIndication()
    }
    private val diseaseCategory = Observer<DiseaseCategory> { list ->
        realm.executeTransaction {
            val diseaseCategory = realm.where<DiseaseCategory>().findAllAsync()
            realm.where(DiseaseCategory::class.java).findAll().deleteAllFromRealm()
            it.insert(list)
        }
        commonViewModel.getByBrand()
    }
    private val brandCategory = Observer<BrandCategory> { list ->
        realm.executeTransaction {
            val brandCategory = realm.where<BrandCategory>().findAllAsync()
            realm.where(BrandCategory::class.java).findAll().deleteAllFromRealm()
            it.insert(list)
        }
        commonViewModel.getAllData()
    }
    private val allData = Observer<List<AllData>> { list ->
        CommonMethods.dismissProgress()
        ApplicationGlobal.hitAllApis = false
        swipeRefresh.isRefreshing = false
        Log.i("Observer", "in all data observer")
        realm.executeTransaction {
            val allData = realm.where<AllData>().findAllAsync()
            realm.where(AllData::class.java).findAll().deleteAllFromRealm()
            it.insert(list)
        }
    }


    private val syncData = Observer<ArrayList<SyncDbData>> {
        if (ApplicationGlobal.preferenceManager.getDbTimes().isNullOrEmpty()) {
            //realm.executeTransaction { realm.deleteAll() }
            ApplicationGlobal.preferenceManager.saveDbTimes(Gson().toJson(it))
            CommonMethods.showProgress(activity!!)
            commonViewModel.getGenericData()
//            commonViewModel.getSystemData()
//            commonViewModel.getByIndication()
//            commonViewModel.getByBrand()
            //commonViewModel.getAllData()
        } else {
            syncDbData = Gson().fromJson(
                ApplicationGlobal.preferenceManager.getDbTimes(),
                SyncAllDbData::class.java
            )
            for (i in syncDbData.indices) {
                if (!syncDbData[i].updated_at.equals(it[i].updated_at)) {
                    Log.i("NotEqual", "${it[i]}")
                    hitApisAgain = true
                    break
                } else
                    Log.i("NotEqual", " nothing to update")
            }
            if (hitApisAgain) {
               // realm.executeTransaction { realm.deleteAll() }
                hitApisAgain = false
                ApplicationGlobal.preferenceManager.saveDbTimes(Gson().toJson(it))
                CommonMethods.showProgress(activity!!)
                commonViewModel.getGenericData()
//                commonViewModel.getSystemData()
//                commonViewModel.getByIndication()
//                commonViewModel.getByBrand()
               // commonViewModel.getAllData()
            }
        }
    }
    private val sync = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            CommonMethods.showProgress(activity!!)
            //realm.executeTransaction { realm.deleteAll() }
            commonViewModel.getGenericData()
            getNewAddedDrugs(false)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
        realm = Realm.getDefaultInstance()


        //Observer
        commonViewModel = ViewModelProvider(this).get(CommonViewModel::class.java)
        commonViewModel.allLiveDataResponse.observe(viewLifecycleOwner, allData)
        commonViewModel.genericNameLiveData.observe(viewLifecycleOwner, genericName)
        commonViewModel.systemCategoryLiveData.observe(viewLifecycleOwner, systemCategory)
        commonViewModel.diseaseCategoryLiveData.observe(viewLifecycleOwner, diseaseCategory)
        commonViewModel.brandCategoryLiveData.observe(viewLifecycleOwner, brandCategory)
        commonViewModel.syncDbLiveData.observe(viewLifecycleOwner, syncData)
        commonViewModel.getValue(activity!!)

        //reciever
        activity!!.registerReceiver(sync, IntentFilter("sync"))
        //this for clear notification dot from launcher App icon.
        val notificationManager =
            activity!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?
        notificationManager!!.cancelAll()

        //click Listeners
        drugsAToZ.setOnClickListener(this)
        symptom.setOnClickListener(this)
        drugsByBrand.setOnClickListener(this)
        drugsByDisease.setOnClickListener(this)
        drugsBySystem.setOnClickListener(this)
        genericDrugs.setOnClickListener(this)
        btnSearch.setOnClickListener(this)
        ivMenu.setOnClickListener(this)
        ivProfile.setOnClickListener(this)
        etSearch.setOnClickListener(this)
        rlSearch.setOnClickListener(this)
        Log.i("Greeting", "${CommonMethods.getGreetingText()}")
        tvGreeting.text = CommonMethods.getGreetingText()

        //set Layout Manager for Recycler View
        val layoutManager = GridLayoutManager(activity!!, 2)
        recyclerNewAddedDrugs.layoutManager = layoutManager
        newAddedDrugsAdapter = NewAddedDrugs(activity!!, newDrugsList)
        recyclerNewAddedDrugs.adapter = newAddedDrugsAdapter

        //get New Added Drugs

//        if (realm.isEmpty) {
//            commonViewModel.getGenericData()
//            commonViewModel.getSystemData()
//            commonViewModel.getByIndication()
//            commonViewModel.getByBrand()
//            CommonMethods.showProgress(activity!!)
//            commonViewModel.getAllData()
//        }
        //  getNewAddedDrugs(true)
        swipeRefresh.setOnRefreshListener {
            if (CommonMethods.phoneIsOnline(activity!!)) {
//                realm.executeTransaction { realm.deleteAll() }
//                commonViewModel.getGenericData()
//                commonViewModel.getSystemData()
//                commonViewModel.getByIndication()
//                commonViewModel.getByBrand()
//                CommonMethods.showProgress(activity!!)
//                commonViewModel.getAllData()
                commonViewModel.getSyncDbData()
                getNewAddedDrugs(false)
            } else {
                swipeRefresh.isRefreshing = false
                Toast.makeText(activity, "No internet connection", Toast.LENGTH_LONG).show()
            }

        }
        homeImageResp = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBannerImages(),
            HomeImageResp::class.java
        )
        if (homeImageResp == null)
            RetrofitClient.getRetrofit().getHomeImages()
                .enqueue(object : Callback<HomeImageResp> {
                    override fun onFailure(call: Call<HomeImageResp>, t: Throwable) {

                    }

                    override fun onResponse(
                        call: Call<HomeImageResp>,
                        response: Response<HomeImageResp>
                    ) {
                        if (response.isSuccessful && response.body() != null) {
                            Log.i("homeImages", response.body()!!.toString())
                            ApplicationGlobal.preferenceManager.saveBannerImages(
                                Gson().toJson(
                                    response.body()
                                )
                            )
                            if (response.body()!!.image_system != null) {
                                GlideApp.with(activity!!)
                                    .load(Constant.IMAGE_URL_MEDIUM + response.body()!!.image_system)
                                    .centerCrop()
                                    .error(R.drawable.img_one)
                                    .into(ivSystem)
                            }

                            if (!response.body()!!.image_a_z.isNullOrEmpty()) {
                                GlideApp.with(activity!!)
                                    .load(Constant.IMAGE_URL_MEDIUM + response.body()!!.image_a_z)
                                    .centerCrop()
                                    .error(R.drawable.img_two)
                                    .into(ivAToZ)
                            }

                            if (!response.body()!!.image_indication.isNullOrEmpty()) {
                                GlideApp.with(activity!!)
                                    .load(Constant.IMAGE_URL_MEDIUM + response.body()!!.image_indication.toString())
                                    .centerCrop()
                                    .error(R.drawable.img_three)
                                    .into(ivIndication)
                            }

                            if (!response.body()!!.image_brand.isNullOrEmpty()) {
                                GlideApp.with(activity!!)
                                    .load(Constant.IMAGE_URL_MEDIUM + response.body()!!.image_brand)
                                    .centerCrop()
                                    .error(R.drawable.img_four)
                                    .into(ivBrandName)
                            }

                            if (!response.body()!!.image_generic.isNullOrEmpty()) {
                                GlideApp.with(activity!!)
                                    .load(Constant.IMAGE_URL_MEDIUM + response.body()!!.image_generic)
                                    .centerCrop()
                                    .error(R.drawable.img_five)
                                    .into(ivGeneric)
                            }

                            if (!response.body()!!.image_symtom.isNullOrEmpty()) {
                                GlideApp.with(activity!!)
                                    .load(Constant.IMAGE_URL_MEDIUM + response.body()!!.image_symtom)
                                    .centerCrop()
                                    .error(R.drawable.img_six)
                                    .into(ivSymptom)
                            }
                        } else {
                            CommonMethods.getErrorMessageAndSessionExpire(
                                activity!!,
                                response.errorBody()!!
                            )
                        }
                    }
                })
        else {
            if (homeImageResp!!.image_system != null) {
                GlideApp.with(activity!!)
                    .load(Constant.IMAGE_URL_MEDIUM + homeImageResp!!.image_system)
                    .centerCrop()
                    .error(R.drawable.img_one)
                    .into(ivSystem)
            }

            if (!homeImageResp!!.image_a_z.isNullOrEmpty()) {
                GlideApp.with(activity!!)
                    .load(Constant.IMAGE_URL_MEDIUM + homeImageResp!!.image_a_z)
                    .centerCrop()
                    .error(R.drawable.img_two)
                    .into(ivAToZ)
            }

            if (!homeImageResp!!.image_indication.isNullOrEmpty()) {
                GlideApp.with(activity!!)
                    .load(Constant.IMAGE_URL_MEDIUM + homeImageResp!!.image_indication.toString())
                    .centerCrop()
                    .error(R.drawable.img_three)
                    .into(ivIndication)
            }

            if (!homeImageResp!!.image_brand.isNullOrEmpty()) {
                GlideApp.with(activity!!)
                    .load(Constant.IMAGE_URL_MEDIUM + homeImageResp!!.image_brand)
                    .centerCrop()
                    .error(R.drawable.img_four)
                    .into(ivBrandName)
            }

            if (!homeImageResp!!.image_generic.isNullOrEmpty()) {
                GlideApp.with(activity!!)
                    .load(Constant.IMAGE_URL_MEDIUM + homeImageResp!!.image_generic)
                    .centerCrop()
                    .error(R.drawable.img_five)
                    .into(ivGeneric)
            }

//            if (!homeImageResp!!.image_symtom.isNullOrEmpty()) {
//                GlideApp.with(activity!!)
//                    .load(Constant.IMAGE_URL_MEDIUM + homeImageResp!!.image_symtom)
//                    .centerCrop()
//                    .error(R.drawable.img_six)
//                    .into(ivSymptom)
//            }
            getUserData()
        }
    }

    //for all Click Listener method
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.etSearch -> {
                CommonMethods.hideKeyboard(activity!!)
//                ApplicationGlobal.preferenceManager.setHomeSearchText(
//                    etSearch.text.toString().trim()
//                )
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "HomeSearch")
                )
            }
            R.id.btnSearch -> {
                CommonMethods.hideKeyboard(activity!!)
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "HomeSearch")
                )
            }
            R.id.drugsAToZ -> {
                val intent = Intent(activity!!, DrugsDetailsActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                intent.putExtra("whichScreen", "drugsAToZ")
                startActivity(intent)
            }
            R.id.symptom -> {
                val intent = Intent(activity!!, DrugsDetailsActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                intent.putExtra("whichScreen", "symptom")
                startActivity(intent)
            }
            R.id.drugsBySystem -> {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "System")
                )
            }
            R.id.drugsByBrand -> {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "Brand")
                )
            }
            R.id.genericDrugs -> {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "Generic")
                )
            }
            R.id.drugsByDisease -> {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "Disease")
                )
            }
            R.id.ivMenu -> {
                activity!!.drawer_layout.openDrawer(GravityCompat.START)
                activity!!.sendBroadcast(Intent("isOnline"))
            }
            R.id.ivProfile -> {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "myProfile")
                )
//                CommonMethods.replaceFragmentNavigationView(
//                    MyProfileFragment(),
//                    fragmentManager!!
//                )
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val allData = realm.where<AllData>().findAllAsync()
        val demoList = ArrayList<AllData>()
        demoList.addAll(allData)
//        if (demoList.isEmpty()) {
//            Toast.makeText(
//                requireActivity(),
//                "There is some issue in sync please again with good internet connection.",
//                Toast.LENGTH_SHORT
//            ).show()
//            //commonViewModel.getAllData()
//        }
        commonViewModel.getSyncDbData()
        if (ApplicationGlobal.hitAllApis) {
//            realm.executeTransaction { realm.deleteAll() }
            CommonMethods.showProgress(activity!!)
            commonViewModel.getGenericData()
//            commonViewModel.getSystemData()
//            commonViewModel.getByIndication()
//            commonViewModel.getByBrand()
            //commonViewModel.getAllData()
        }
        try {
            getUserDetails =
                Gson().fromJson(
                    ApplicationGlobal.preferenceManager.getBasicDetails(),
                    BasicInfoResp::class.java
                )

        } catch (e: Exception) {
            Log.i("errorrrrr", "msg ${e.message}")
        }
        if (!ApplicationGlobal.isProfileUpdated && getUserDetails != null) {
            try {
                if (getUserDetails!!.name != null) {
                    tvPersonName.text = "${getUserDetails!!.name}!"
                } else {
                    tvPersonName.text = "User"
                }
                if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
                    if (getUserDetails!!.profile_pic!!.startsWith("http")) {
                        CommonMethods.setUrlImage(
                            activity!!,
                            getUserDetails!!.profile_pic.toString(),
                            ivProfile
                        )
                    } else {
                        CommonMethods.setServerImage(
                            activity!!,
                            getUserDetails!!.profile_pic.toString(),
                            ivProfile
                        )
                    }
                } else {
                    GlideApp.with(activity!!)
                        .load(R.drawable.placeholder_small)
                        .circleCrop()
                        .into(ivProfile)
                }
            } catch (e: Exception) {
                Log.i("errorrrrr", "msg ${e.message}")
            }
        } else
            getUserData()
        if (CommonMethods.phoneIsOnline(activity!!))
            getNewAddedDrugs(false)
        else {
            newDrugsList.clear()
            val newAddedDrugs =
                realm.where<com.rxhub.rxhub.realmModels.NewAddedDrugs>().findAllAsync()
            try {
                newDrugsList.addAll(newAddedDrugs[0]!!.result)
            } catch (e: java.lang.Exception) {
            }
            newAddedDrugsAdapter.notifyDataSetChanged()
        }
        if (etSearch.isFocused) {
            etSearch.text.clear()
            etSearch.clearFocus()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        activity!!.unregisterReceiver(sync)
    }

    //this method get user data.
    private fun getUserData() {
        RetrofitClient.getRetrofit().getUserData()
            .enqueue(object : Callback<BasicInfoResp> {
                override fun onFailure(call: Call<BasicInfoResp>, t: Throwable) {
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.toast(activity!!, t.message!!)
                    } else {
                        //    CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                }

                override fun onResponse(
                    call: Call<BasicInfoResp>,
                    response: Response<BasicInfoResp>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        Log.i("ProfileImage", "${response.body()!!.profile_pic!!}")
                        ApplicationGlobal.isProfileUpdated = false
//                        if (response.body()!!.is_active == Constent.ACTIVE) {
                        ApplicationGlobal.preferenceManager.setBasicDetails(
                            Gson().toJson(response.body()!!)
                        )
                        try {
                            if (response.body()!!.name != null) {

                                tvPersonName.text = "${response.body()!!.name}!"
                            } else {
                                Log.i("userName", "in else")
                                tvPersonName.text = "User"
                            }

                            if (response.body()!!.profile_pic!!.isNotEmpty()) {
                                if (response.body()!!.profile_pic!!.startsWith("http")) {
                                    CommonMethods.setUrlImage(
                                        activity!!,
                                        response.body()!!.profile_pic.toString(),
                                        ivProfile
                                    )
                                } else {
                                    CommonMethods.setServerImage(
                                        activity!!,
                                        response.body()!!.profile_pic.toString(),
                                        ivProfile
                                    )
                                }
                            } else {
                                GlideApp.with(activity!!)
                                    .load(R.drawable.placeholder_small)
                                    .circleCrop()
                                    .into(ivProfile)
                            }
                        } catch (e: Exception) {
                            Log.i("ErrorOnHome", "${e.message}")
                        }
//                        } else {
//                            ApplicationGlobal.preferenceManager!!.logOut()
//                            val intent =
//                                Intent(activity!!, SplashActivity::class.java)
//                            intent.putExtra("Logout", "logout")
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
//                            startActivity(intent)
//                            activity!!.finish()
//                        }
                    } else {

                    }
                }
            })

    }

    //this method get New Added Drugs
    private fun getNewAddedDrugs(boolean: Boolean) {
        // if (boolean) CommonMethods.showProgress(activity!!)
        hashMapNewDrugs.put("language_id", ApplicationGlobal.english.toString())
        hashMapNewDrugs.put("page_count", pageCount.toString().trim())
        hashMapNewDrugs.put("new", Constant.NEW_ADDED_DRUGS.toString().trim())
        RetrofitClient.getRetrofit().getMedicines(hashMapNewDrugs)
            .enqueue(object : Callback<com.rxhub.rxhub.realmModels.NewAddedDrugs> {
                override fun onFailure(
                    call: Call<com.rxhub.rxhub.realmModels.NewAddedDrugs>,
                    t: Throwable
                ) {
                    //  CommonMethods.dismissProgress()
                    swipeRefresh.isRefreshing = false
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.handelInternet(activity!!, "No internet connection")
                    } else {
                        // CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                    Log.i("asdfasdfadsfasf", "" + t.message!!)
                }

                override fun onResponse(
                    call: Call<com.rxhub.rxhub.realmModels.NewAddedDrugs>,
                    response: Response<com.rxhub.rxhub.realmModels.NewAddedDrugs>
                ) {
                    try {
                        //  CommonMethods.dismissProgress()
                        swipeRefresh.isRefreshing = false
                        if (response.isSuccessful && response.body() != null) {
                            if (response.body()!!.count!! > 0) {
                                newDrugsTitle.visibility = View.VISIBLE
                                recyclerNewAddedDrugs.visibility = View.VISIBLE

                                newDrugsList.clear()
                                val newDrugs = response.body()!!.result
                                for (i in newDrugs.indices) {
                                    newDrugsList.add(newDrugs[i]!!)
                                    Log.i("NewAdded", "${newDrugsList}")
                                }

                                newAddedDrugsAdapter.notifyDataSetChanged()
                                realm.executeTransaction {
                                    it.delete(com.rxhub.rxhub.realmModels.NewAddedDrugs::class.java)
                                    it.insert(response.body()!!)
                                }

                            } else {
                                newDrugsTitle.visibility = View.GONE
                                recyclerNewAddedDrugs.visibility = View.GONE
                            }
                        } else {
//                            CommonMethods.getErrorMessageAndSessionExpire(
//                                activity!!,
//                                response.errorBody()!!
//                            )
//                        CommonMethods.sessionExpire(
//                            activity!!,
//                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
//                        )
                        }
                    } catch (e: Exception) {
                    }
                }
            })
    }
}