package com.rxhub.rxhub.fragments

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.ReferenceContainerActivity
import com.rxhub.rxhub.realmModels.AllData
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.viewmodels.CommonViewModel
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import io.branch.indexing.BranchUniversalObject
import io.branch.referral.util.ContentMetadata
import io.branch.referral.util.LinkProperties
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.fragment_drugs_details.*
import java.util.regex.Matcher
import java.util.regex.Pattern

/*
* This class fragment_drugs_details show all medicines Details with medicine id.
*
* created by GTB on 16/04/2020
*/
class DrugsDetailsFragment : BaseFragment(), View.OnClickListener {
    val hashMap = HashMap<String, String>()
    lateinit var listPopupWindowLanguage: ListPopupWindow
    lateinit var listPopupWindowClassification: ListPopupWindow
    lateinit var medId: String
    private lateinit var userDetails: BasicInfoResp
    private var branchUniversalObject: BranchUniversalObject? = null
    var medicine_name = ""
    var medicine_id = ""
    private val HTML_PATTERN = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>"
    private val pattern = Pattern.compile(HTML_PATTERN)
    val specialNoteLanguageList = ArrayList<String>()
    val classificationLanguageList = ArrayList<String>()
    val specialNoteTextList = ArrayList<String>()
    val classificationTextList = ArrayList<String>()
    var allData: AllData? = null
    var commonViewModel = CommonViewModel()
    lateinit var realm: Realm
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_drugs_details
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        realm = Realm.getDefaultInstance()
        //get medicine Id
        medId = ApplicationGlobal.preferenceManager.getMedicineId().toString()
        Log.i("medicineId", "" + medId)
        getMedById(medId.toInt())

        //click Listener here
        ivBackArrow.setOnClickListener(this)
        ivShareDetails.setOnClickListener(this)
        cvIconSpecial.setOnClickListener(this)
        cvIconClassification.setOnClickListener(this)

//        userDetails = Gson().fromJson(
//            ApplicationGlobal.preferenceManager.getBasicDetails(),
//            BasicInfoResp::class.java
//        )
        //method for get Medicine Details
        // getMedicineDetails(0)
        //method for get Language List
        languageList()

        //spannableString in bottom of layout
        val string =
            SpannableString(resources.getString(R.string.this_special_note_is_based_in_the_available_reference_guide))
        val clickableSpan = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }

            override fun onClick(p0: View) {
                startActivity(
                    Intent(activity!!, ReferenceContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                )
            }
        }
        string.setSpan(clickableSpan, string.length - 16, string.length - 7, 0)
        string.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorFbButton)),
            string.length - 16,
            string.length - 7,
            0
        )
        tvNoteText.movementMethod = LinkMovementMethod.getInstance()
        tvNoteText.text = string
    }

//    fun getMedicineDetails(lang: Int) {
//        //get details from server
//        CommonMethods.showProgress(activity!!)
//        var langId = 0
//        if (lang == 0) {
//            langId = ApplicationGlobal.preferenceManager.getLanguageId()
//        } else {
//            langId = lang
//        }
//        RetrofitClient.getRetrofit().getMedicineDetails(
//            medId.toInt(),
//            langId
//        )
//            .enqueue(object : Callback<MedicineDetailsResponse> {
//                override fun onFailure(call: Call<MedicineDetailsResponse>, t: Throwable) {
//                    CommonMethods.dismissProgress()
//                    if (CommonMethods.phoneIsOnline(activity!!)) {
//                        CommonMethods.toast(activity!!, t.message!!)
//                    } else {
//                        CommonMethods.toast(activity!!, "Internet connection not available")
//                    }
//                }
//
//                override fun onResponse(
//                    call: Call<MedicineDetailsResponse>,
//                    response: Response<MedicineDetailsResponse>
//                ) {
//                    try {
//                        CommonMethods.dismissProgress()
//                        if (response.isSuccessful && response.body() != null) {
//                            Log.i("delailsResponse", "" + response.body()!!)
//                            if (response.body()!!.generic_name != null) {
//                                tvGenricName1.text = response.body()!!.generic_name
//                                tvGenricName2.text = response.body()!!.brand_name
////                            tvSpecialNoteText.text = response.body()!!.instruction
//                                tvIndicationText.text = response.body()!!.indication_name
////                                tvClassificationText.text = response.body()!!.classification
//                                tvDrugName.text = response.body()!!.name
//                                medicine_name = response.body()!!.name.toString()
//                                medicine_id = response.body()!!.medicine_id.toString()
//
////                                if (hasHTMLTags(response.body()!!.instruction.toString())) {
////                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
////                                        Log.i("specailnote", " true n ")
////                                        tvSpecialNoteText.setText(
////                                            Html.fromHtml(
////                                                response.body()!!.instruction.toString(),
////                                                Html.FROM_HTML_MODE_LEGACY
////                                            )
////                                        )
////                                    } else {
////                                        Log.i("specailnote", " true not n ")
////                                        tvSpecialNoteText.setText(Html.fromHtml(response.body()!!.instruction.toString()))
////                                    }
////                                } else {
////                                    tvSpecialNoteText.text = response.body()!!.instruction
////                                }
//                            }
//
//
//                            val specialNote = response.body()!!.instruction
//                            for (i in specialNote.indices) {
//                                if (hasHTMLTags(specialNote[0].instruction)) {
//                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                                        tvSpecialNoteText.setText(
//                                            Html.fromHtml(
//                                                specialNote[0].instruction,
//                                                Html.FROM_HTML_MODE_LEGACY
//                                            )
//                                        )
//                                    } else {
//                                        tvSpecialNoteText.setText(Html.fromHtml(specialNote[0].instruction))
//                                    }
//                                } else {
//                                    tvSpecialNoteText.text = specialNote[0].instruction
//                                }
//                                if (specialNote[i].instruction.isNotEmpty()) {
//                                    specialNoteLanguageList.add(resources.getStringArray(R.array.language)[i])
//                                    specialNoteTextList.add(specialNote[i].instruction)
//                                }
//                            }
//                            val classification = response.body()!!.classification
//                            for (i in classification.indices) {
//                                tvClassificationText.text = classification[0].classification
//                                if (classification[i].classification.isNotEmpty()) {
//                                    classificationLanguageList.add(resources.getStringArray(R.array.language)[i])
//                                    classificationTextList.add(classification[i].classification)
//                                }
//                            }
//
//                            if (specialNoteLanguageList.size <= 1)
//                                cvIconSpecial.visibility = View.GONE
//                            else
//                                cvIconSpecial.visibility = View.VISIBLE
//                            if (classificationLanguageList.size <= 1)
//                                cvIconClassification.visibility = View.GONE
//                            else cvIconClassification.visibility = View.VISIBLE
//
//                            if (response.body()!!.image!!.isNotEmpty()) {
//                                GlideApp.with(activity!!)
//                                    .load(Constant.IMAGE_URL_MEDIUM + response.body()!!.image)
//                                    .error(R.drawable.shutterstock)
//                                    .into(ivTopImage)
//                            } else {
//                                GlideApp.with(activity!!)
//                                    .load(R.drawable.shutterstock)
//                                    .into(ivTopImage)
//                            }
//                        } else {
//                            CommonMethods.getErrorMessageAndSessionExpire(
//                                activity!!,
//                                response.errorBody()!!
//                            )
//                        }
//                    } catch (e: Exception) {
//
//                    }
//                }
//            })
//    }

    //all click Listener method
    @RequiresApi(Build.VERSION_CODES.N)
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.ivBackArrow -> {
                activity!!.onBackPressed()
            }
            R.id.ivShareDetails -> {
                try {
                    Log.i("brnchlink", "id send" + medicine_id)
                    branchUniversalObject(medId.toInt(), medicine_name, "", "medicine")
                        .generateShortUrl(
                            activity!!,
                            linkProperties()
                        ) { url, _ ->
                            try {
                                Log.i("branchlink", "" + url)
                                var sAux: String? = null
                                sAux = "\nCheck out this medicine on Rxhub.\n\n$url"
                                val i = Intent(Intent.ACTION_SEND)
                                i.type = "text/plain"
                                i.putExtra(Intent.EXTRA_SUBJECT, "Rxhub")
                                i.putExtra(Intent.EXTRA_TEXT, sAux)
                                startActivity(Intent.createChooser(i, "choose one"))
                            } catch (e: Exception) {
                                e.message
                            }
                        }
                } catch (e: Exception) {
                }
            }
            R.id.cvIconSpecial -> {
                listPopupWindowLanguage.show()
            }
            R.id.cvIconClassification -> {
                listPopupWindowClassification.show()
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    fun linkProperties(): LinkProperties {
        return LinkProperties()
            .setChannel("app")
            .setFeature("sharing")
            .addControlParameter("custom", "data")
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun branchUniversalObject(
        id: Int, name: String, url: String?, isProfile: String
    ): BranchUniversalObject {
        val buo = BranchUniversalObject()
            .setCanonicalIdentifier("content/12345")
            .setCanonicalUrl("https://dwlpt.app.link/YJYTy0EB45")
            .setTitle(getString(R.string.app_name))
            .setContentDescription(name)
            .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
            .setContentMetadata(ContentMetadata().addCustomMetadata("detailId", id.toString()))
        return buo
    }


    fun languageList() {
        //this popup window for show language list and set the languages
        listPopupWindowLanguage = ListPopupWindow(activity!!)
        listPopupWindowLanguage.setAdapter(
            ArrayAdapter(
                activity!!,
                android.R.layout.simple_dropdown_item_1line,
                specialNoteLanguageList
//                resources.getStringArray(R.array.language)
            )
        )
        listPopupWindowLanguage.anchorView = cvIconSpecial
        listPopupWindowLanguage.setContentWidth(252)
        listPopupWindowLanguage.isModal = true
        listPopupWindowLanguage.setOnItemClickListener { parent, view, position, id ->

            if (hasHTMLTags(specialNoteTextList[position])) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Log.i("specailnote", " true n ")
                    tvSpecialNoteText.setText(
                        Html.fromHtml(
                            specialNoteTextList[position],
                            Html.FROM_HTML_MODE_LEGACY
                        )
                    )
                } else {
                    Log.i("specailnote", " true not n ")
                    tvSpecialNoteText.setText(Html.fromHtml(specialNoteTextList[position]))
                }
            } else {
                tvSpecialNoteText.text = specialNoteTextList[position].toString()
            }
//            ApplicationGlobal.isLanguageChanged = true
//            when (position) {
//                0 -> {
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.ENGLISH)
//                    getMedicineDetails(Constant.ENGLISH)
//                }
//                1 -> {
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.ARABIC)
//                    getMedicineDetails(Constant.ARABIC)
//                }
//                2 -> {
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.URDU)
//                    getMedicineDetails(Constant.URDU)
//                }
//                3 -> {
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.HINDI)
//                    getMedicineDetails(Constant.HINDI)
//                }
//            }

            listPopupWindowLanguage.dismiss()
        }


        //this popup window for show language list and set the languages
        listPopupWindowClassification = ListPopupWindow(activity!!)
        listPopupWindowClassification.setAdapter(
            ArrayAdapter(
                activity!!,
                android.R.layout.simple_dropdown_item_1line,
                classificationLanguageList
//                resources.getStringArray(R.array.language)
            )
        )
        listPopupWindowClassification.anchorView = cvIconClassification
        listPopupWindowClassification.setContentWidth(252)
        listPopupWindowClassification.isModal = true
        listPopupWindowClassification.setOnItemClickListener { parent, view, position, id ->
            tvClassificationText.text = classificationTextList[position]
//            ApplicationGlobal.isLanguageChanged = true
//            when (position) {
//                0 -> {
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.ENGLISH)
//                    getMedicineDetails(Constant.ENGLISH)
//                }
//                1 -> {
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.ARABIC)
//                    getMedicineDetails(Constant.ARABIC)
//                }
//                2 -> {
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.URDU)
//                    getMedicineDetails(Constant.URDU)
//                }
//                3 -> {
//                    ApplicationGlobal.preferenceManager.setLanguageId(Constant.HINDI)
//                    getMedicineDetails(Constant.HINDI)
//                }
//            }
            listPopupWindowClassification.dismiss()
        }
    }

    fun hasHTMLTags(text: String?): Boolean {
        val matcher: Matcher = pattern.matcher(text!!)
        return matcher.find()
    }

    private fun getMedById(medId: Int) {
        specialNoteLanguageList.clear()
        specialNoteTextList.clear()
        classificationLanguageList.clear()
        classificationTextList.clear()
//        genericMedList.clear()
        val demoList = ArrayList<AllData>()
        val allDatas = realm.where<AllData>().findAllAsync()
        demoList.addAll(allDatas)
        for (data in demoList) {
            if (data.id != null)
                if (data.id!!.equals(medId))
                    allData = data
        }
        if (allData == null) {
            for (data in demoList) {
                if (data.id != null)
                    if (data.medicine_id!!.equals(medId))
                        allData = data
            }
        }
        Log.i("AllData", "${allData}")
        if (allData != null) {
            tvDrugName.text = allData!!.name
            tvGenricName1.text = allData!!.generic_name
            tvGenricName2.text = allData!!.brand_name
            if (allData!!.drug_form_name != null)
                tvDrugFromName.text = allData!!.drug_form_name
            else
                tvDrugFromName.visibility = View.GONE

            tvIndicationText.text = allData!!.indication_name
            if (allData!!.image.isNullOrEmpty())
                GlideApp.with(activity!!)
                    .load(R.drawable.shutterstock)
                    .into(ivTopImage)
            else
                GlideApp.with(activity!!)
                    .load(Constant.IMAGE_URL_MEDIUM + allData!!.image)
                    .error(R.drawable.shutterstock)
                    .into(ivTopImage)

            if (allData!!.instruction.isNotEmpty()) {
                try {
                    for (inst in allData!!.instruction.indices) {
                        if (allData!!.instruction[inst]!!.instruction != null && allData!!.instruction[inst]!!.instruction!!.isNotEmpty()) {
                            specialNoteLanguageList.add(resources.getStringArray(R.array.language)[inst])
                            specialNoteTextList.add(allData!!.instruction[inst]!!.instruction.toString())
                        }
                    }
                } catch (e: Exception) {
                }

            } else {
                cardViewSpecialNotes.visibility = View.GONE
            }
            if (specialNoteTextList.size <= 1)
                cvIconSpecial.visibility = View.GONE
            else
                cvIconSpecial.visibility = View.VISIBLE
            // for (i in specialNoteTextList.indices)
            if (specialNoteTextList.isNotEmpty())
                if (hasHTMLTags(specialNoteTextList[0].toString())) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        Log.i("specailnote", " in html mode")
                        tvSpecialNoteText.text = Html.fromHtml(
                            specialNoteTextList[0],
                            Html.FROM_HTML_MODE_LEGACY
                        )
                    } else {
                        Log.i("specailnote", " in html mode bt in else ")
                        tvSpecialNoteText.setText(Html.fromHtml(specialNoteTextList[0].toString()))
                    }
                } else {
                    Log.i("specailnote", "simple string   ${specialNoteTextList[0]} ")
                    tvSpecialNoteText.text = specialNoteTextList[0].toString()
                }
            else
                cardViewSpecialNotes.visibility = View.GONE
            try {
                if (allData!!.classification.isNotEmpty()) {
                    for (i in allData!!.classification.indices) {
                        if (allData!!.classification[i]!!.classification != "") {
                            classificationLanguageList.add(resources.getStringArray(R.array.language)[i])
                            classificationTextList.add(allData!!.classification[i]!!.classification!!)
                        }
                    }
//            else {
//                Log.i("Classification", "in else ${allData.classification.size}")
//                cvIconClassification.visibility = View.GONE
//            }
                } else
                    cardViewClassification.visibility = View.GONE
            } catch (e: java.lang.Exception) {
            }

            if (classificationTextList.size <= 1)
                cvIconClassification.visibility = View.GONE
            else cvIconClassification.visibility = View.VISIBLE
            if (classificationTextList.isNotEmpty())
                tvClassificationText.text = classificationTextList[0]
            else
                cardViewClassification.visibility = View.GONE
            if (CommonMethods.phoneIsOnline(activity!!))
                commonViewModel.postForHistory(allData!!.medicine_id)
        } else
            Toast.makeText(activity, "something is wrong please try again", Toast.LENGTH_LONG)
                .show()
    }
}