package com.rxhub.rxhub.fragments

import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.provider.Settings
import android.text.Spannable
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.MainActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.ForgotPass
import com.rxhub.rxhub.webServices.model.SignUpResp
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.item_forget_password_layout.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*
import kotlin.collections.HashMap

/*
* This class is fragment_login that will be used to Login to app
*
* created by GTB on 14/04/2020
*/
class LogInFragment : BaseFragment(), View.OnClickListener {
    val hashMap = HashMap<String, String>()
    var fcmId: String = ""
    lateinit var android_id: String

    //Google sign in
    val GOOGLE_SIGNIN: Int = 1
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleSignInOptions: GoogleSignInOptions
    lateinit var callbackManager: CallbackManager
    lateinit var socialId: String
    lateinit var firstName: String
    var email: String = ""
    private lateinit var firebaseAuth: FirebaseAuth
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_login
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        firebaseAuth = FirebaseAuth.getInstance()
        mGoogleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(activity!!, mGoogleSignInOptions)
        tvLogin.setPaintFlags(tvLogin.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        //click Listeners
        btnLogIn.setOnClickListener(this)
        tvForgotPass.setOnClickListener(this)
        tvFacebook.setOnClickListener(this)
        tvGoogle.setOnClickListener(this)
        //get device toked from FCM
        deviceFCMToken()
        //spannableString in bottom of layout
        val string = SpannableString(resources.getString(R.string.dontHaveAnAccountSignUp))
        val boldSpan = StyleSpan(Typeface.BOLD)
        val clickableSpan = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }

            override fun onClick(p0: View) {
                CommonMethods.replaceFragmentInSplashScreen(
                    activity!!.supportFragmentManager,
                    RegisterFragment()
                )
            }
        }
        string.setSpan(clickableSpan, string.length - 7, string.length, 0)
        string.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorGreen)),
            string.length - 7,
            string.length,
            0
        )
        string.setSpan(
            boldSpan,
            string.length - 7,
            string.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tvDontHaveAccount.movementMethod = LinkMovementMethod.getInstance()
        tvDontHaveAccount.text = string


        //this is for getting KeyHash for google api_key
        try {
            val info = activity!!.packageManager.getPackageInfo(
                activity!!.packageName, PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
        } catch (ignored: NoSuchAlgorithmException) {
        }
        // Get Unique ID of Android Device
        android_id = Settings.Secure.getString(
            getContext()!!.getContentResolver(),
            Settings.Secure.ANDROID_ID
        )
        Log.i("asdfasdf", " android id Login " + android_id)
        //this is used to facebook lognin
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    val request =
                        GraphRequest.newMeRequest(result!!.accessToken) { `object`, response ->
                            try {
                                LoginManager.getInstance().logOut()
                                AccessToken.setCurrentAccessToken(null)
                                socialId = `object`.getString("id")
                                firstName = `object`.getString("name")
                                if (!`object`.getString("email").isNullOrEmpty())
                                    email = `object`.getString("email")

//                                val im = `object`.getString("picture")
                                val profileImage =
                                    ("https://graph.facebook.com/" + socialId + "/picture?type=large")
                                Log.i("FacebookSingIn", "Response  :" + socialId + "  " + email)
                                logIn(firstName, email, "", socialId, "", profileImage)
                            } catch (e: Exception) {
                            }
                        }
                    val parameters = Bundle()
                    parameters.putString("fields", "email,name,picture")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    CommonMethods.toast(activity!!, "Login Cancel")
                }

                override fun onError(error: FacebookException?) {
                    CommonMethods.toast(activity!!, "" + error!!.message)
                }
            })
    }

    //method for all Click Listener
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.tvForgotPass -> {
                CommonMethods.avoidDoubleClicks(tvForgotPass)
                lateinit var show: AlertDialog
                activity!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                val inDialogView =
                    LayoutInflater.from(context).inflate(R.layout.item_forget_password_layout, null)
                val alertDialog = AlertDialog.Builder(context)
                    .setView(inDialogView)
                show = alertDialog.show()
                inDialogView.btnSubmit.setOnClickListener {
                    CommonMethods.avoidDoubleClicks(inDialogView.btnSubmit)
                    if (inDialogView.etEnterEmail.text.toString().trim().isEmpty()) {
                        CommonMethods.toast(activity!!, "Please Enter Email")
                    } else if (!Patterns.EMAIL_ADDRESS.matcher(inDialogView.etEnterEmail.text.trim())
                            .matches()
                    ) {
                        CommonMethods.toast(activity!!, "Enter a valid Email")
                    } else {
                        CommonMethods.hideKeyboard(activity!!)
                        CommonMethods.showProgress(activity!!)
                        hashMap.put("email", inDialogView.etEnterEmail.text.toString().trim())
                        RetrofitClient.getRetrofit().forgotPass(hashMap)
                            .enqueue(object : Callback<ForgotPass> {
                                override fun onFailure(call: Call<ForgotPass>, t: Throwable) {
                                    CommonMethods.dismissProgress()
                                    if (CommonMethods.phoneIsOnline(activity!!)) {
                                        CommonMethods.toast(activity!!, t.message!!)
                                    } else {
                                        CommonMethods.toast(
                                            activity!!,
                                            "Internet connection not available"
                                        )
                                    }
                                }

                                override fun onResponse(
                                    call: Call<ForgotPass>,
                                    response: Response<ForgotPass>
                                ) {
                                    CommonMethods.dismissProgress()
                                    show.dismiss()
                                    if (response.isSuccessful) {
                                        CommonMethods.toast(
                                            activity!!,
                                            response.body()!!.message
                                        )
                                    } else {
                                        CommonMethods.toast(
                                            activity!!,
                                            "${CommonMethods.getErrorMessage(response.errorBody()!!)}"
                                        )
                                    }
                                }
                            })
                    }
                }
            }
            R.id.btnLogIn -> {
                if (etEmail.text.trim().isEmpty()) {
                    CommonMethods.toast(activity!!, "Email is required")
                } else if (etPassword.text.toString().trim().isEmpty()) {
                    CommonMethods.toast(activity!!, "Password is required")
                } else {
                    CommonMethods.showProgress(activity!!)
                    hashMap.put("email", etEmail.text.toString().trim())
                    hashMap.put("password", etPassword.text.toString().trim())
                    hashMap.put("device_type", Constant.DEVICE_TYPE.toString().trim())
                    hashMap.put("fcm_id", fcmId)
                    hashMap.put("device_token", android_id)
                    RetrofitClient.getRetrofit().logIn(hashMap)
                        .enqueue(object : Callback<SignUpResp> {
                            override fun onFailure(call: Call<SignUpResp>, t: Throwable) {
                                CommonMethods.dismissProgress()
                                if (CommonMethods.phoneIsOnline(activity!!)) {
                                    CommonMethods.toast(activity!!, t.message!!)
                                } else {
                                    CommonMethods.toast(
                                        activity!!,
                                        "Internet connection not available"
                                    )
                                }
                            }

                            override fun onResponse(
                                call: Call<SignUpResp>,
                                response: Response<SignUpResp>
                            ) {
                                CommonMethods.dismissProgress()
                                if (response.isSuccessful) {
                                    Log.i("respoinads", "  ResponsBody    " + response.body()!!)
                                    ApplicationGlobal.preferenceManager.setSignUpResp(
                                        Gson().toJson(response.body()!!)
                                    )
                                    ApplicationGlobal.preferenceManager.setSessionId(response.body()!!.token)
                                    val intent = Intent(activity!!, MainActivity::class.java)
                                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                                    startActivity(intent)
                                    activity!!.finish()
                                    Log.i(
                                        "respoinads",
                                        " GetSignUpData   " + Gson().fromJson(
                                            ApplicationGlobal.preferenceManager.getSignUpResp(),
                                            SignUpResp::class.java
                                        )
                                    )
                                } else {
                                    CommonMethods.toast(
                                        activity!!,
                                        "" +
                                                CommonMethods.getErrorMessage(response.errorBody()!!)
                                    )
                                }
                            }
                        })
                }
            }
            R.id.tvFacebook -> {
                if (CommonMethods.phoneIsOnline(activity!!)) {
                    LoginManager.getInstance()
                        .logInWithReadPermissions(
                            this, Arrays.asList(
                                "public_profile",
                                "email"
                            )
                        )
                } else {
                    CommonMethods.toast(activity!!, "Internet connection not available")
                }
            }
            R.id.tvGoogle -> {
                if (CommonMethods.phoneIsOnline(activity!!)) {
                    val signInIntent: Intent = mGoogleSignInClient.signInIntent
                    startActivityForResult(signInIntent, GOOGLE_SIGNIN)
                } else {
                    CommonMethods.toast(activity!!, "Internet connection not available")
                }
            }
        }
    }

    //this get result from facebook and google logIN
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GOOGLE_SIGNIN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            val act = GoogleSignIn.getLastSignedInAccount(activity)
            try {
                val account = task.getResult(ApiException::class.java)!!
                val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
                }
            } catch (e: ApiException) {
                if (CommonMethods.phoneIsOnline(activity!!)) {
                    CommonMethods.toast(activity!!, "Google Sign In Cancel")
                } else {
                    CommonMethods.toast(activity!!, "Internet connection not available")
                }
            }
            if (act != null) {
                firstName = act.displayName.toString()
                email = act.email.toString()
                socialId = act.id.toString()
                val personPhoto = act.getPhotoUrl()!!.toString()
                logIn(firstName, email, "", "", socialId, personPhoto)
                CommonMethods.googleLogout(activity!!)
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    //this method is used for  getting FCM device token
    fun deviceFCMToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(
            activity!!,
            { instanceIdResult ->
                val newToken = instanceIdResult.token
                Log.i("newToken", "" + newToken)
                fcmId = newToken
                ApplicationGlobal.preferenceManager!!.setFCMID(newToken)
            })
    }


    //this method used for login button, facebook and google sign in.
    fun logIn(
        name: String,
        emailAdd: String,
        password: String,
        fbId: String,
        googleId: String,
        profileImage: String
    ) {
        CommonMethods.showProgress(activity!!)
        if (password.isNotEmpty())
            hashMap.put("password", password)
        if (fbId.isNotEmpty())
            hashMap.put("fb_id", fbId)
        if (googleId.isNotEmpty())
            hashMap.put("google_id", googleId)
        if (profileImage.isNotEmpty())
            hashMap.put("image", profileImage.trim())

        hashMap.put("name", name)
        hashMap.put("email", emailAdd)
        hashMap.put("device_type", Constant.DEVICE_TYPE.toString().trim())
        hashMap.put("fcm_id", fcmId)
        hashMap.put("device_token", android_id)
        RetrofitClient.getRetrofit().signUp(hashMap)
            .enqueue(object : Callback<SignUpResp> {
                override fun onFailure(call: Call<SignUpResp>, t: Throwable) {
                    CommonMethods.dismissProgress()
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.toast(activity!!, t.message!!)
                    } else {
                        CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                }

                override fun onResponse(
                    call: Call<SignUpResp>,
                    response: Response<SignUpResp>
                ) {
                    CommonMethods.dismissProgress()
                    if (response.isSuccessful) {
                        ApplicationGlobal.preferenceManager.setSignUpResp(
                            Gson().toJson(response.body()!!.toString())
                        )
                        ApplicationGlobal.preferenceManager.setSessionId(response.body()!!.token)
                        if (response.body()!!.user.designation_id != 1) {
                            val intent = Intent(activity!!, MainActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            startActivity(intent)
                            activity!!.finish()
                        } else {
                            val basicInforFragment = BasicInformationFragment()
                            val bundle = Bundle()
                            bundle.putString("SignUp", emailAdd)
                            bundle.putString("UserName", name)
                            basicInforFragment.arguments = bundle
                            CommonMethods.replaceFragmentInSplashScreen(
                                activity!!.supportFragmentManager,
                                basicInforFragment
                            )
                        }
                    } else {
                        CommonMethods.toast(
                            activity!!, "" +
                                    CommonMethods.getErrorMessage(response.errorBody()!!)
                        )
                    }
                }
            })
    }
}