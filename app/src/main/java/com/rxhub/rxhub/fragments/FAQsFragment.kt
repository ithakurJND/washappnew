package com.rxhub.rxhub.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.SettingContainerActivity
import com.rxhub.rxhub.adapters.FaqsAdapter
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import com.rxhub.rxhub.webServices.model.FaqsResponse
import com.rxhub.rxhub.webServices.model.ResultF
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_faqs.*
import kotlinx.android.synthetic.main.item_activity_main_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/*
* This class is show FAQ's screen
*
* created by GTB on 15/04/2020
*/
class FAQsFragment : BaseFragment(), View.OnClickListener {
    val hashMap = HashMap<String, String>()
    var pageCount = 0
    private var loading = false
    val faqList = arrayListOf<ResultF>()
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_faqs
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //click Listeners
        btnAskQuestion.setOnClickListener(this)
        ivProfile.setOnClickListener(this)
        ivMenu.setOnClickListener(this)
        //set Layout Manager for Recycler View
        val layoutManager = LinearLayoutManager(activity!!)
        recyclerFaqs.layoutManager = layoutManager
        //get Faq's
        getfaqs(0)

        //this for pagination
        recyclerFaqs.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) //check for scroll down
                {
                    Log.i("paginateiona", "befor")
                    val visibleItemCount = layoutManager.getChildCount()
                    val totalItemCount = layoutManager.getItemCount()
                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            loading = false
                            Log.v("...", "Last Item Wow !")
                            //Do pagination.. i.e. fetch new data
                            Log.i("paginateiona", "load")
                            progress_bar.visibility = View.VISIBLE
                            getfaqs(pageCount)
                        }
                    }
                }
            }
        })
        //set user Profile image
        setImage()
    }

    //method for all click Listener
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.btnAskQuestion -> {
                activity!!.inquiry.performClick()
//                val intent = Intent(activity!!, DrugsDetailsActivity::class.java)
//                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
//                intent.putExtra("whichScreen", "askQuestion")
//                startActivity(intent)
            }
            R.id.ivProfile -> {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "myProfile")
                )
            }
            R.id.ivMenu -> {
                activity!!.drawer_layout.openDrawer(GravityCompat.START)
            }
        }
    }

    //get Faq's
    fun getfaqs(page: Int) {
        hashMap.put("language_id", ApplicationGlobal.english.toString().trim())
        hashMap.put("page_count", page.toString().trim())
        RetrofitClient.getRetrofit().getFaqs(hashMap)
            .enqueue(object : Callback<FaqsResponse> {
                override fun onFailure(call: Call<FaqsResponse>, t: Throwable) {
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.toast(activity!!, t.message!!)
                    } else {
                        CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                }

                override fun onResponse(
                    call: Call<FaqsResponse>,
                    response: Response<FaqsResponse>
                ) {
                    try {
                        if (response.isSuccessful && response.body() != null) {
                            if (page == 0) {
                                if (response.body()!!.count > 0) {
                                    recyclerFaqs.visibility = View.VISIBLE
                                    tvNoDataFaqs.visibility = View.GONE
                                    try {
                                        faqList.clear()
                                        val respons = response.body()!!.result
                                        for (i in respons.indices) {
                                            faqList.add(respons[i])
                                        }
                                        recyclerFaqs.adapter =
                                            FaqsAdapter(activity!!, faqList)
                                        if (faqList.size == Constant.TOTAL_DATA) {
                                            loading = true
                                            pageCount++
                                        }
                                    } catch (e: Exception) {
                                    }
                                } else {
                                    recyclerFaqs.visibility = View.GONE
                                    tvNoDataFaqs.visibility = View.VISIBLE
                                }
                            } else {
                                Log.i("paginateiona", "resp" + response.body()!!.result)
                                progress_bar.visibility = View.GONE
                                val newData = response.body()!!.result
                                faqList.addAll(newData)
                                recyclerFaqs.adapter!!.notifyDataSetChanged()
                                if (faqList.size == (page + 1) * Constant.TOTAL_DATA) {
                                    pageCount++
                                    loading = true
                                }
                            }
                        } else {
                            CommonMethods.getErrorMessageAndSessionExpire(
                                activity!!,
                                response.errorBody()!!
                            )
//                        CommonMethods.toast(
//                            activity!!, "" +
//                                    CommonMethods.getErrorMessage(response.errorBody()!!)
//                        )
                        }
                    } catch (e: java.lang.Exception) {
                    }
                }

            })
    }

    //set user profile image method and call in two places.
    fun setImage() {
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        try {
            if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
                if (getUserDetails!!.profile_pic!!.startsWith("http")) {
                    CommonMethods.setUrlImage(
                        activity!!,
                        getUserDetails!!.profile_pic.toString(),
                        ivProfile
                    )
                } else {
                    CommonMethods.setServerImage(
                        activity!!,
                        getUserDetails!!.profile_pic.toString(),
                        ivProfile
                    )
                }
            } else {
                GlideApp.with(activity!!)
                    .load(R.drawable.placeholder_small)
                    .circleCrop()
                    .into(ivProfile)
            }
        } catch (e: Exception) {
        }
    }

    override fun onResume() {
        super.onResume()
        setImage()
    }
}