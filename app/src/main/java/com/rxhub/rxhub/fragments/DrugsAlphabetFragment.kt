package com.rxhub.rxhub.fragments

import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.DrugsAlphabetAdapter
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import kotlinx.android.synthetic.main.fragment_drugs_alphabets.*

/*
* This class fragment_drugs_alphabet show All Alphabet.
*
* create by GTB on 20/04/2020
 */
class DrugsAlphabetFragment : BaseFragment() {
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_drugs_alphabets
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val layoutManager = GridLayoutManager(activity!!, 2)
        recyclerViewAlphabet.layoutManager = layoutManager
        recyclerViewAlphabet.adapter =
            DrugsAlphabetAdapter(resources.getStringArray(R.array.alphabets), activity!!)

        ivProfile.setOnClickListener {
            CommonMethods.replaceFragmentInDrugsDetail(fragmentManager!!, MyProfileFragment())
        }

        ivBackArrow.setOnClickListener { activity!!.onBackPressed() }

        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails!!.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails!!.profile_pic.toString(),
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails!!.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
    }
}