package com.rxhub.rxhub.fragments

import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.BodyPartsSymptomAdapter
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import com.rxhub.rxhub.webServices.model.BodyPartSymptom
import com.rxhub.rxhub.webServices.model.ResultP
import kotlinx.android.synthetic.main.fragment_bodypart_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/*
* This class fragment_bodypart_details show the sysmptom by body Part.
*
* create by GTB on 17/04/2020
 */
class BodyPartDetailsFragment(
    val bodyPartName: String,
    val bodyPartId: Int,
    val bodyPartImage: String
) : BaseFragment(),
    View.OnClickListener {
    val hashMap = HashMap<String, String>()
    var pageCount = 0
    private var loading = true
    val symptomList = ArrayList<ResultP>()
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_bodypart_details
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //set body part name
        tvBodyName.text = bodyPartName

        Log.i("aadsfasdfa", "" + bodyPartImage)
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (bodyPartImage != "") {
            //set body part image
            GlideApp.with(context!!)
                .load(Constant.IMAGE_URL_SMALL + bodyPartImage)
                .into(ivBodyPartsImage)
        } else {
            GlideApp.with(context!!)
                .load(R.drawable.head)
                .into(ivBodyPartsImage)
        }
        //in this set user profile
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
        //underline the body part name
        tvBodyName.setPaintFlags(tvBodyName.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)

        //set layout Manger for recycler view
        val layoutManager = LinearLayoutManager(activity!!)
        recyclerBodyPartsSymptom.layoutManager = layoutManager

        //click Listener here
        ivArrowBack.setOnClickListener(this)
        ivProfile.setOnClickListener(this)

        Log.i("bodyId", "" + bodyPartId)
        //get Symptom list
        getSymptom(0, true)

        //this for pagination
        recyclerBodyPartsSymptom.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) //check for scroll down
                {
                    val visibleItemCount = layoutManager.getChildCount()
                    val totalItemCount = layoutManager.getItemCount()
                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            loading = false
                            Log.v("...", "Last Item Wow !")
                            //Do pagination.. i.e. fetch new data
                            progress_bar_symptom.visibility = View.VISIBLE
                            getSymptom(pageCount, false)
                        }
                    }
                }
            }
        })
        Log.i("onreusme", " On Activity  ")
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.ivArrowBack -> {
                activity!!.onBackPressed()
            }
            R.id.ivProfile -> {
                CommonMethods.replaceFragmentInDrugsDetail(
                    fragmentManager!!,
                    MyProfileFragment()
                )
            }
        }
    }

    //get Symptom by body parts
    fun getSymptom(page: Int, boolean: Boolean) {
        if (boolean) {
            CommonMethods.showProgress(activity!!)
        }
        hashMap.put("body_part_id", bodyPartId.toString().trim())
        hashMap.put("language_id", ApplicationGlobal.english.toString().trim())
        hashMap.put("page_count", page.toString().trim())
        RetrofitClient.getRetrofit().getbodypartSymptom(hashMap)
            .enqueue(object : Callback<BodyPartSymptom> {
                override fun onFailure(call: Call<BodyPartSymptom>, t: Throwable) {
                    CommonMethods.dismissProgress()
                    CommonMethods.handelInternet(activity!!, t.message!!)
                }

                override fun onResponse(
                    call: Call<BodyPartSymptom>,
                    response: Response<BodyPartSymptom>
                ) {
                    CommonMethods.dismissProgress()
                    if (response.isSuccessful && response.body() != null) {
                        Log.i("asdl;fkjasdlf", "" + response.body())
                        if (page == 0) {
                            if (response.body()!!.count > 0) {
                                recyclerBodyPartsSymptom.visibility = View.VISIBLE
                                tvNoDataSymptom.visibility = View.GONE
                                symptomList.clear()
                                val symptomResp = response.body()!!.result
                                for (i in symptomResp.indices) {
                                    symptomList.add(symptomResp[i])
                                }
                                recyclerBodyPartsSymptom.adapter =
                                    BodyPartsSymptomAdapter(activity!!, symptomList)
                                if (symptomList.size == Constant.TOTAL_DATA) {
                                    loading = true
                                    pageCount++
                                }
                            } else {
                                recyclerBodyPartsSymptom.visibility = View.GONE
                                tvNoDataSymptom.visibility = View.VISIBLE
                            }
                        } else {
                            val newData = response.body()!!.result
                            symptomList.addAll(newData)
                            recyclerBodyPartsSymptom.adapter!!.notifyDataSetChanged()
                            progress_bar_symptom.visibility = View.GONE
                            if (symptomList.size == (page + 1) * Constant.TOTAL_DATA) {
                                pageCount++
                                loading = true
                            }
                        }
                    } else {
                        CommonMethods.getErrorMessageAndSessionExpire(
                            activity!!,
                            response.errorBody()!!
                        )
//                        CommonMethods.toast(
//                            activity!!, "" +
//                                    CommonMethods.getErrorMessage(response.errorBody()!!)
//                        )
                    }
                }
            })
    }

    override fun onResume() {
        super.onResume()
        Log.i("onreusme", " On Resume  ")
    }
}