package com.rxhub.rxhub.fragments

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.util.Linkify
import android.view.Gravity
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.AcknowledgementResponse
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import kotlinx.android.synthetic.main.fragment_reference.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ReferenceFragment(val referenceAcknowledgement: String, val from: Int) : BaseFragment(),
    View.OnClickListener {
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_reference
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivBackReference.setOnClickListener(this)
        ivProfileReference.setOnClickListener(this)

//        if (referenceAcknowledgement == Constant.REFERENCE) {
//            tvScreenName.text = activity!!.resources.getString(R.string.reference)
//            tvReference.linksClickable = true
//            tvReference.autoLinkMask = Linkify.ALL
//        } else {
//            tvScreenName.text = activity!!.resources.getString(R.string.acknowledgement)
//            tvReference.gravity = Gravity.CENTER_HORIZONTAL
//        }
        when (referenceAcknowledgement) {
            Constant.REFERENCE -> {
                tvScreenName.text = activity!!.resources.getString(R.string.reference)
                tvReference.linksClickable = true
                tvReference.autoLinkMask = Linkify.ALL
            }
            Constant.ACKNOWLEDGEMENT -> {
                tvScreenName.text = activity!!.resources.getString(R.string.acknowledgement)
                tvReference.gravity = Gravity.START
            }
            Constant.TERMS -> {
                tvScreenName.text = activity!!.resources.getString(R.string.termsandConditions)
                tvReference.gravity = Gravity.START
            }
            Constant.PRIVACY -> {
                tvScreenName.text = activity!!.resources.getString(R.string.privacyPolicy)
                tvReference.gravity = Gravity.START
            }
            Constant.ABOUT -> {
                tvScreenName.text = activity!!.resources.getString(R.string.about_us)
                tvReference.gravity = Gravity.START
            }
        }

        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails.profile_pic,
                    ivProfileReference
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfileReference
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfileReference)
        }

        CommonMethods.showProgress(activity!!)
        if (referenceAcknowledgement == Constant.REFERENCE || referenceAcknowledgement == Constant.ACKNOWLEDGEMENT)
            RetrofitClient.getRetrofit().getReferenceAndAcknowledgement(referenceAcknowledgement)
                .enqueue(object : Callback<AcknowledgementResponse> {
                    override fun onFailure(call: Call<AcknowledgementResponse>, t: Throwable) {
                        CommonMethods.dismissProgress()
                        Toast.makeText(requireActivity(),"No internet connection", Toast.LENGTH_LONG).show()
                    }

                    override fun onResponse(
                        call: Call<AcknowledgementResponse>,
                        response: Response<AcknowledgementResponse>
                    ) {
                        CommonMethods.dismissProgress()
                        if (response.isSuccessful && response.body() != null) {
                            when (referenceAcknowledgement) {
                                Constant.REFERENCE -> {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        tvReference.setText(
                                            Html.fromHtml(
                                                response.body()!!.reference,
                                                Html.FROM_HTML_MODE_COMPACT
                                            )
                                        )
//                                tvReference.text =
//                                    Html.fromHtml(response.body()!!.reference, null, UlTagHandler())
                                    } else {
                                        tvReference.setText(Html.fromHtml(response.body()!!.reference))
//                                tvReference.setText(Html.fromHtml(response.body()!!.reference, null, UlTagHandler()))
                                    }
                                }
                                Constant.ACKNOWLEDGEMENT -> {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        tvReference.setText(
                                            Html.fromHtml(
                                                response.body()!!.acknowledgement,
                                                Html.FROM_HTML_MODE_COMPACT
                                            )
                                        )
//                                tvReference.text =
//                                    Html.fromHtml(response.body()!!.reference, null, UlTagHandler())
                                    } else {
                                        tvReference.setText(Html.fromHtml(response.body()!!.reference))
//                                tvReference.setText(Html.fromHtml(response.body()!!.reference, null, UlTagHandler()))
                                    }
                                }
                            }
//                        if (referenceAcknowledgement == Constant.REFERENCE) {
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                                tvReference.setText(
//                                    Html.fromHtml(
//                                        response.body()!!.reference,
//                                        Html.FROM_HTML_MODE_COMPACT
//                                    )
//                                )
////                                tvReference.text =
////                                    Html.fromHtml(response.body()!!.reference, null, UlTagHandler())
//                            } else {
//                                tvReference.setText(Html.fromHtml(response.body()!!.reference))
////                                tvReference.setText(Html.fromHtml(response.body()!!.reference, null, UlTagHandler()))
//                            }
//                        } else {
//                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                                tvReference.setText(
//                                    Html.fromHtml(
//                                        response.body()!!.acknowledgement,
//                                        Html.FROM_HTML_MODE_LEGACY
//                                    )
//                                )
////                                tvReference.setText(
////                                    Html.fromHtml(
////                                        response.body()!!.acknowledgement,
////                                        null,
////                                        UlTagHandler()
////                                    )
////                                )
//                            } else {
//                                tvReference.setText(Html.fromHtml(response.body()!!.acknowledgement))
//                            }
//                        }
                        } else {
                            Toast.makeText(requireActivity(),"No internet connection",Toast.LENGTH_LONG).show()
                            CommonMethods.getErrorMessageAndSessionExpire(
                                activity!!,
                                response.errorBody()!!
                            )
                        }
                    }
                })
        else
            RetrofitClient.getRetrofit().getDynamicData(referenceAcknowledgement)
                .enqueue(object : Callback<AcknowledgementResponse> {
                    override fun onFailure(call: Call<AcknowledgementResponse>, t: Throwable) {
                        Toast.makeText(requireActivity(),"No internet connection",Toast.LENGTH_LONG).show()
                        CommonMethods.dismissProgress()
                    }

                    override fun onResponse(
                        call: Call<AcknowledgementResponse>,
                        response: Response<AcknowledgementResponse>
                    ) {
                        CommonMethods.dismissProgress()
                        if (response.isSuccessful) {
                            when (referenceAcknowledgement) {
                                Constant.TERMS -> {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        tvReference.setText(
                                            Html.fromHtml(
                                                response.body()!!.terms_and_conditions,
                                                Html.FROM_HTML_MODE_COMPACT
                                            )
                                        )
                                    } else {
                                        tvReference.setText(Html.fromHtml(response.body()!!.terms_and_conditions))
                                    }
                                }
                                Constant.PRIVACY -> {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        tvReference.setText(
                                            Html.fromHtml(
                                                response.body()!!.privacy_and_policy,
                                                Html.FROM_HTML_MODE_COMPACT
                                            )
                                        )
                                    } else {
                                        tvReference.setText(Html.fromHtml(response.body()!!.privacy_and_policy))
                                    }
                                }
                                Constant.ABOUT -> {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                        tvReference.setText(
                                            Html.fromHtml(
                                                response.body()!!.about,
                                                Html.FROM_HTML_MODE_COMPACT
                                            )
                                        )
                                    } else {
                                        tvReference.setText(Html.fromHtml(response.body()!!.about))
                                    }
                                }
                            }
                        } else {
                            Toast.makeText(requireActivity(),"No internet connection",Toast.LENGTH_LONG).show()
                            CommonMethods.getErrorMessageAndSessionExpire(
                                activity!!,
                                response.errorBody()!!
                            )
                        }
                    }
                })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivBackReference -> {
                activity!!.onBackPressed()
            }
            R.id.ivProfileReference -> {
                if (from == 0) {
                    CommonMethods.replaceFragmentInSetting(
                        fragmentManager!!,
                        MyProfileFragment()
                    )
                } else {
                    CommonMethods.replaceToReferenceContainer(
                        fragmentManager!!,
                        MyProfileFragment()
                    )
                }
            }
        }
    }
}