package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.view.View
import androidx.core.view.marginTop
import com.rxhub.rxhub.R
import com.rxhub.rxhub.utils.CommonMethods
import kotlinx.android.synthetic.main.fragment_tutorial_screens.*

/*
* This class is used Tutorial ViewPage Fragment screen
*
* created by GTB on 14/04/2020
*/
class TutorialScreenFragment(val position: Int) : BaseFragment() {
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_tutorial_screens
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (position == 0) {
            tvMedicineHealsText.visibility=View.GONE
            ivIntro.setImageResource(R.drawable.intro_one)
            tvMedical.setText(R.string.medicalAwareness)
            tvInfo.setText(R.string.medical_awareness_msg)
            tvSkip.visibility = View.VISIBLE
            lineView.visibility = View.VISIBLE
        } else if (position == 1) {
            tvMedicineHealsText.visibility=View.GONE
            ivIntro.setImageResource(R.drawable.intro_two)
            tvMedical.setText(R.string.searchMedicine)
            tvInfo.setText(R.string.search_medicine_msg)
            tvSkip.visibility = View.VISIBLE
            lineView.visibility = View.VISIBLE
        } else {
            ivIntro.setImageResource(R.drawable.logo_intro)
            tvMedical.setText("")
            tvInfo.setText("")
            tvMedicineHealsText.visibility=View.VISIBLE
            tvMedicineHealsText.setText(R.string.third_screen_text_tutorial)
            lineView.visibility = View.GONE
            tvSkip.visibility = View.GONE
        }

        tvSkip.setOnClickListener {
            CommonMethods.replaceFragmentInSplashScreen(
                activity!!.supportFragmentManager,
                LogInFragment()
            )
        }
    }
}