package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.DrugsBySystemAdapter
import com.rxhub.rxhub.realmModels.AllData
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.fragment_drugs_a_to_z.recyclerViewDrugs
import kotlinx.android.synthetic.main.fragment_system_medicine.*
import kotlinx.android.synthetic.main.fragment_system_medicine.ivProfile
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/*
* This class fragment_system_medicine show the medicine by System
*
* create by GTB on 20/04/2020
 */
class SystemMedicineFragment : BaseFragment() {
    val hashMap = HashMap<String, String>()
    var pageCount = 0
    var getId = ""
    private var loading = false
    val list = ArrayList<AllData>()
    lateinit var realm: Realm
    lateinit var adapter: DrugsBySystemAdapter
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_system_medicine
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        realm = Realm.getDefaultInstance()
        tvDrugsTypeName.text = ApplicationGlobal.preferenceManager.getDrugsName()
        getId = ApplicationGlobal.preferenceManager.getDrugsId().toString()
        ivBackArrow.setOnClickListener {
            activity!!.onBackPressed()
        }

        ivProfile.setOnClickListener {
            CommonMethods.replaceFragmentInDrugsDetail(
                fragmentManager!!,
                MyProfileFragment()
            )
        }
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails!!.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails!!.profile_pic.toString(),
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails!!.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
        val layoutManager = GridLayoutManager(activity!!, 2)
        recyclerViewDrugs.layoutManager = layoutManager
        adapter = DrugsBySystemAdapter(list, activity!!)
        recyclerViewDrugs.adapter = adapter
        showAllBySystem()
        //getMedicine(0, true, "")
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    filter(s.toString())
                    //   getMedicine(0, false, s.toString())
                } else {
                    //getMedicine(0, false, "")
                }
            }
        })
        //this for pagination
//        recyclerViewDrugs.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                if (dy > 0) //check for scroll down
//                {
//                    val visibleItemCount = layoutManager.getChildCount()
//                    val totalItemCount = layoutManager.getItemCount()
//                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
//                    if (loading) {
//                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
//                            loading = false
//                            Log.v("...", "Last Item Wow !")
//                            //Do pagination.. i.e. fetch new data
//                            progress_barSystem.visibility = View.VISIBLE
//                            getMedicine(pageCount, false, "")
//                        }
//                    }
//                }
//            }
//        })
//        Log.i("languageID", "" + ApplicationGlobal.preferenceManager.getLanguageId())
//    }

//        fun getMedicine(page: Int, boolean: Boolean, word: String) {
//            if (boolean) CommonMethods.showProgress(activity!!)
//            hashMap.put("language_id", ApplicationGlobal.english.toString().trim())
//            hashMap.put("page_count", page.toString().trim())
//            hashMap.put("system_id", getId.trim())
//            hashMap.put("search", word.trim())
//            RetrofitClient.getRetrofit().getMedicines(hashMap)
//                .enqueue(object : Callback<SystemResp> {
//                    override fun onFailure(call: Call<SystemResp>, t: Throwable) {
//                        CommonMethods.dismissProgress()
//                        if (CommonMethods.phoneIsOnline(activity!!)) {
//                            CommonMethods.toast(activity!!, t.message!!)
//                        } else {
//                            CommonMethods.toast(activity!!, "Internet connection not available")
//                        }
//                    }
//
//                    override fun onResponse(
//                        call: Call<SystemResp>,
//                        response: Response<SystemResp>
//                    ) {
//                        CommonMethods.dismissProgress()
//                        if (response.isSuccessful && response.body() != null) {
//                            if (page == 0) {
//                                if (response.body()!!.count > 0) {
//                                    pageCount = 0
//                                    Log.i("drugsrespon", "" + response.body()!!.result)
//                                    tvNoDataSystem.visibility = View.GONE
//                                    recyclerViewDrugs.visibility = View.VISIBLE
//                                    list.clear()
//                                    val a = response.body()!!.result
//                                    for (i in a.indices) {
//                                        list.add(a[i])
//                                    }
//                                    recyclerViewDrugs.adapter =
//                                        DrugsBySystemAdapter(
//                                            list,
//                                            activity!!
//                                        )
//                                    if (list.size == Constant.TOTAL_DATA) {
//                                        loading = true
//                                        pageCount++
//                                    }
//                                } else {
//                                    recyclerViewDrugs.visibility = View.GONE
//                                    tvNoDataSystem.visibility = View.VISIBLE
//                                }
//                            } else {
//                                val newData = response.body()!!.result
//                                list.addAll(newData)
//                                recyclerViewDrugs.adapter!!.notifyDataSetChanged()
//                                progress_barSystem.visibility = View.GONE
//                                if (list.size == (page + 1) * Constant.TOTAL_DATA) {
//                                    pageCount++
//                                    loading = true
//                                }
//                            }
//                        } else {
//                            CommonMethods.getErrorMessageAndSessionExpire(
//                                activity!!,
//                                response.errorBody()!!
//                            )
////                        CommonMethods.toast(
////                            activity!!, "" +
////                                    CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
//                        }
//                    }
//                })
//        }
    }

    private fun showAllBySystem() {
        list.clear()
        val demoList = ArrayList<AllData>()
        val allData = realm.where<AllData>().findAllAsync()
        demoList.addAll(allData)
        for (data in demoList) {
            if (data.med_system_name != null)
                if (data.med_system_name!!.equals(ApplicationGlobal.preferenceManager.getDrugsName()))
                    list.add(data)
        }
        Collections.sort(list, Comparator { obj1, obj2 ->
            // ## Ascending order
            obj1.name!!.toLowerCase().compareTo(obj2.name!!.toLowerCase(), false) // To compare string values
            // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
            // ## Descending order
            // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
            // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
        })
        adapter.notifyDataSetChanged()
    }

    fun filter(word: String) {
        val temArrayList = ArrayList<AllData>()
        for (d in list) {
            word.apply {
                if (d.name!!.toLowerCase().startsWith(word.toLowerCase()))
                    temArrayList.add(d)
            }
        }
        adapter.filter(temArrayList)
    }
}