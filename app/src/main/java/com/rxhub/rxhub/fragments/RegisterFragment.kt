package com.rxhub.rxhub.fragments

import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.provider.Settings
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.util.Patterns
import android.view.View
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.MainActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.SignUpResp
import kotlinx.android.synthetic.main.fragment_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.HashMap


/*
* This class is used to SignUp/Register to app
*
* created by GTB on 14/04/2020
*/
class RegisterFragment : BaseFragment(), View.OnClickListener {
    val hashMap = HashMap<String, String>()
    lateinit var callbackManager: CallbackManager
    var fcmId: String = ""
    val TAG = "GoogleSignIn"

    //Google sign in
    val GOOGLE_SIGNIN: Int = 1
    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var mGoogleSignInOptions: GoogleSignInOptions
    private lateinit var firebaseAuth: FirebaseAuth
    lateinit var socialId: String
    lateinit var firstName: String
    lateinit var email: String
    lateinit var android_id: String
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_register
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        firebaseAuth = FirebaseAuth.getInstance()
        mGoogleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(activity!!, mGoogleSignInOptions)
        //all click listener
        ivCheckBox.setOnClickListener(this)
        btnRegister.setOnClickListener(this)
        tvFacebook.setOnClickListener(this)
        tvGoogle.setOnClickListener(this)

        //set text with under Line.
        tvregister.paintFlags = tvregister.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        //spannableString in bottom of layout
        val string = SpannableString(resources.getString(R.string.alreadyhaveanaccountLogIn))
        val clickableSpan = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false
            }

            override fun onClick(p0: View) {
                CommonMethods.replaceFragmentInSplashScreen(
                    activity!!.supportFragmentManager,
                    LogInFragment()
                )
            }
        }
        string.setSpan(clickableSpan, string.length - 7, string.length, 0)
        string.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorGreen)),
            string.length - 7,
            string.length,
            0
        )
        tvLogin.movementMethod = LinkMovementMethod.getInstance()
        tvLogin.text = string
        //this is used to facebook lognin
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    val request =
                        GraphRequest.newMeRequest(result!!.accessToken) { `object`, response ->
                            LoginManager.getInstance().logOut()
                            AccessToken.setCurrentAccessToken(null)
                            socialId = `object`.getString("id")
                            firstName = `object`.getString("name")
                            email = `object`.getString("email")
                            val im = `object`.getString("picture")
                            val profileImage =
                                ("https://graph.facebook.com/" + socialId + "/picture?type=large")
                            Log.i("FacebookSingIn", "Response  :" + socialId + "  " + email)
                            logIn(firstName, email, "", socialId, "", profileImage)
                        }
                    val parameters = Bundle()
                    parameters.putString("fields", "email,name,picture")
                    request.parameters = parameters
                    request.executeAsync()
                }

                override fun onCancel() {
                    CommonMethods.toast(activity!!, "Login Cancel")
                }

                override fun onError(error: FacebookException?) {
                    CommonMethods.toast(activity!!, "" + error!!.message)
                }
            })
        //get device FCM token
        deviceFCMToken()
        // Get Unique ID of Android Device
        android_id = Settings.Secure.getString(
            getContext()!!.getContentResolver(),
            Settings.Secure.ANDROID_ID
        )
        Log.i("asdfasdf", " android id " + android_id)


        //spannableString in bottom of layout
        val terms =
            SpannableString(resources.getString(R.string.I_am_agree_to_Terms_of_use_Disclaimer_and_General_Instructions))
        val clickableSpanTerm = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
            }

            override fun onClick(p0: View) {
                CommonMethods.replaceFragmentInSplashScreenWithBack(
                    activity!!.supportFragmentManager,
                    TermsOfUseFragment("Register", Constant.TERMS_OF_USE)
                )
            }
        }
        val clickableSpanIn = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
            }

            override fun onClick(p0: View) {
                CommonMethods.replaceFragmentInSplashScreenWithBack(
                    activity!!.supportFragmentManager,
                    TermsOfUseFragment("Register", Constant.INSTRUCTIONS)
                )
            }
        }
        val clickableDisclaimer = object : ClickableSpan() {
            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true
            }

            override fun onClick(p0: View) {
                CommonMethods.replaceFragmentInSplashScreenWithBack(
                    activity!!.supportFragmentManager,
                    TermsOfUseFragment("Register", Constant.DISCLAIMER)
                )
            }
        }
        terms.setSpan(clickableSpanIn, terms.length - 21, terms.length, 0)
        terms.setSpan(clickableSpanTerm, 14, 26, 0)
        terms.setSpan(clickableDisclaimer, 27, 38, 0)
        terms.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorBlack)),
            27,
            38,
            0
        )
        terms.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorBlack)),
            14,
            26,
            0
        )
        terms.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.colorBlack)),
            terms.length - 21,
            terms.length,
            0
        )
        tvAgreeTerms.movementMethod = LinkMovementMethod.getInstance()
        tvAgreeTerms.text = terms
    }

    //this method is used for  getting FCM device token
    private fun deviceFCMToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(
            activity!!
        ) { instanceIdResult ->
            val newToken = instanceIdResult.token
            Log.i("newToken", "" + newToken)
            fcmId = newToken
            ApplicationGlobal.preferenceManager.setFCMID(newToken)
        }
    }

    //all the clickListener
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.ivCheckBox -> {
                if (ivCheckBox.drawable.constantState ==
                    context!!.resources.getDrawable(R.drawable.check_box).constantState
                ) {
                    ivCheckBox.setImageResource(R.drawable.check_box_outline)
                } else {
                    ivCheckBox.setImageResource(R.drawable.check_box)
                }
            }
            R.id.btnRegister -> {
                if (etName.text.toString().trim().isEmpty()) {
                    CommonMethods.toast(activity!!, "Name is required")
                    return
                } else if (etName.text.toString().trim().length < 1) {
                    CommonMethods.toast(activity!!, "Enter valid Name")
                    return
                } else if (etEmailAddress.text.toString().trim().isEmpty()) {
                    CommonMethods.toast(activity!!, "Email is required")
                    return
                } else if (!Patterns.EMAIL_ADDRESS.matcher(etEmailAddress.text.trim()).matches()) {
                    CommonMethods.toast(activity!!, "Enter a valid Email")
                    return
                } else if (etPassword.text.toString().trim().isEmpty()) {
                    CommonMethods.toast(activity!!, "password is required")
                    return
                } else if (!isValidPassword(etPassword.text.toString().trim())) {
                    CommonMethods.toast(
                        activity!!, "Minimum eight characters, at least one upercase letter," +
                                " one lowercase letter, one number and one special character"
                    )
                    return
                } else if (!etConfirmPass.text.toString().trim()
                        .equals(etPassword.text.toString().trim())
                ) {
                    CommonMethods.toast(
                        activity!!,
                        "Create Password and Confirm Password does not matched"
                    )
                    return
                } else if (ivCheckBox.drawable.constantState == activity!!.resources.getDrawable(R.drawable.check_box_outline).constantState) {
                    CommonMethods.toast(
                        activity!!,
                        "Please Accept Terms of use Disclaimer and General Instructions."
                    )
                } else {
                    logIn(
                        etName.text.toString().trim(),
                        etEmailAddress.text.toString().trim(),
                        etPassword.text.toString().trim(),
                        "",
                        "",
                        ""
                    )
                }
            }
            R.id.tvFacebook -> {
                if (CommonMethods.phoneIsOnline(activity!!)) {
                    LoginManager.getInstance()
                        .logInWithReadPermissions(
                            this, Arrays.asList(
                                "public_profile",
                                "email"
                            )
                        )
                } else {
                    CommonMethods.toast(activity!!, "Internet connection not available")
                }
            }
            R.id.tvGoogle -> {
                if (CommonMethods.phoneIsOnline(activity!!)) {
                    val signInIntent: Intent = mGoogleSignInClient.signInIntent
                    startActivityForResult(signInIntent, GOOGLE_SIGNIN)
                } else {
                    CommonMethods.toast(activity!!, "Internet connection not available")
                }
            }
        }
    }

    //this get result from facebook and google logIN
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GOOGLE_SIGNIN) {
            val task: Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            val act = GoogleSignIn.getLastSignedInAccount(activity)
            try {
                val account = task.getResult(ApiException::class.java)!!
                val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                firebaseAuth.signInWithCredential(credential).addOnCompleteListener {
                }
            } catch (e: ApiException) {
                if (CommonMethods.phoneIsOnline(activity!!)) {
                    CommonMethods.toast(activity!!, "Google Sign In Cancel")
                } else {
                    CommonMethods.toast(activity!!, "Internet connection not available")
                }
            }
            if (act != null) {
                firstName = act.displayName.toString()
                email = act.email.toString()
                socialId = act.id.toString()
                val personPhoto = act.getPhotoUrl()!!.toString()
                Log.i(
                    TAG, " gmail " + socialId + "  " + email
                )
                logIn(firstName, email, "", "", socialId, personPhoto)
                CommonMethods.googleLogout(activity!!)
            }
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    // this method is used to validation in password field
    fun isValidPassword(password: String): Boolean {
        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!_])(?=\\S+$).{8,}$"
        val pattern = Pattern.compile(PASSWORD_PATTERN)
        val matcher = pattern.matcher(password)
        return matcher.matches()
    }

    //this method used for login button, facebook and google sign in.
    fun logIn(
        name: String,
        emailAdd: String,
        password: String,
        fbId: String,
        googleId: String,
        profileImage: String
    ) {
        CommonMethods.showProgress(activity!!)
        if (password.isNotEmpty())
            hashMap.put("password", password)
        if (fbId.isNotEmpty())
            hashMap.put("fb_id", fbId)
        if (googleId.isNotEmpty())
            hashMap.put("google_id", googleId)
        if (profileImage.isNotEmpty())
            hashMap.put("image", profileImage.trim())

        hashMap.put("name", name)
        hashMap.put("email", emailAdd)
        hashMap.put("device_type", Constant.DEVICE_TYPE.toString().trim())
        hashMap.put("fcm_id", fcmId)
        hashMap.put("device_token", android_id)
        Log.i(TAG, "  hashmap " + hashMap.toString())
        RetrofitClient.getRetrofit().signUp(hashMap)
            .enqueue(object : Callback<SignUpResp> {
                override fun onFailure(call: Call<SignUpResp>, t: Throwable) {
                    CommonMethods.dismissProgress()
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.toast(activity!!, t.message!!)
                    } else {
                        CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                }

                override fun onResponse(
                    call: Call<SignUpResp>,
                    response: Response<SignUpResp>
                ) {
                    CommonMethods.dismissProgress()
                    if (response.isSuccessful) {
                        ApplicationGlobal.preferenceManager.setSignUpResp(
                            Gson().toJson(response.body()!!.toString())
                        )
                        ApplicationGlobal.preferenceManager.setSessionId(response.body()!!.token)
                        Log.i(TAG, "  onResponse " + response.body()!!)
                        if (response.body()!!.user.designation_id != 1) {
                            val intent = Intent(activity!!, MainActivity::class.java)
                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                            startActivity(intent)
                            activity!!.finish()
                        } else {
                            val basicInforFragment = BasicInformationFragment()
                            val bundle = Bundle()
                            bundle.putString("SignUp", emailAdd)
                            bundle.putString("UserName", name)
                            basicInforFragment.arguments = bundle
                            CommonMethods.replaceFragmentInSplashScreen(
                                activity!!.supportFragmentManager,
                                basicInforFragment
                            )
                        }
                    } else {
                        CommonMethods.toast(
                            activity!!, "" +
                                    CommonMethods.getErrorMessage(response.errorBody()!!)
                        )
                    }
                }
            })
    }
}