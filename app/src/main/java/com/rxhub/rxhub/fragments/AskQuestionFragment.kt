package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.ListPopupWindowAdapter
import com.rxhub.rxhub.adapters.ListPopupWindowAdapter.OnClickDeleteButtonListener
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.*
import kotlinx.android.synthetic.main.fragment_ask_question.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/*
* This class fragment_ask_question used to Ask Question in app from Faq's screen
*
* create by GTB on 20/04/2020
 */

class AskQuestionFragment : BaseFragment(), View.OnClickListener {
    lateinit var listPopupWindow: ListPopupWindow
    lateinit var listPopupWindowQuestions: ListPopupWindow
    var hashMap = HashMap<String, Any>()
    var hashMapSubmit = HashMap<String, String>()
    var pageCount = 0
    val designationName = arrayListOf<String>()
    var companyDesignationList = arrayListOf<Result>()
    var questionList = ArrayList<ResultQ>()
    val questionName = arrayListOf<String>()
    var designationId = ""
    lateinit var userId: String
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_ask_question
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //Click Listeners
        tvSelectYourQuestion.setOnClickListener(this)
        tvSelectDesignation.setOnClickListener(this)
        btnSubmit.setOnClickListener(this)
        ivBackArrow.setOnClickListener(this)
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        userId = getUserDetails.id.toString()
        //this api get the questions from server
        RetrofitClient.getRetrofit().getQuestions()
            .enqueue(object : Callback<QuestionResponse> {
                override fun onFailure(call: Call<QuestionResponse>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<QuestionResponse>,
                    response: Response<QuestionResponse>
                ) {
                    if (response.isSuccessful && response.body() != null) {
                        val questions = response.body()!!.result
                        for (i in questions.indices) {
                            questionList.add(questions[i])
                            questionName.add(questions[i].question)
                        }
                    } else {

                    }
                }

            })

        //this api get the designation from the server
        hashMap.put("language_id", ApplicationGlobal.english.toString())
        hashMap.put("page_count", pageCount.toString().trim())
        RetrofitClient.getRetrofit().getDesignation(hashMap)
            .enqueue(object : Callback<Designations> {
                override fun onFailure(call: Call<Designations>, t: Throwable) {
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.toast(activity!!, t.message!!)
                    } else {
                        CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                }

                override fun onResponse(
                    call: Call<Designations>,
                    response: Response<Designations>
                ) {
                    if (response.isSuccessful) {
                        val desi = response.body()!!.result
                        Log.i("asdfalsd", "" + response.body()!!)
                        for (i in desi.indices) {
                            companyDesignationList.add(desi[i])
                            designationName.add(desi[i].name)
                        }
                    } else {
//                        CommonMethods.toast(
//                            activity!!,
//                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
//                        )
                    }
                }

            })


        //this popup window show the Designation List
        listPopupWindow = ListPopupWindow(activity!!)
        listPopupWindow.setAdapter(
            ArrayAdapter(
                activity!!,
                android.R.layout.simple_dropdown_item_1line,
                designationName
            )
        )
        listPopupWindow.anchorView = tvSelectDesignation
        listPopupWindow.isModal = true
        listPopupWindow.setOnItemClickListener { parent, view, position, id ->
            tvSelectDesignation.setText(designationName[position])
            designationId = companyDesignationList[position].id.toString()
            listPopupWindow.dismiss()
        }

        listPopupWindowQuestions = ListPopupWindow(activity!!)
        val listPopupWindowAdapter =
            ListPopupWindowAdapter(activity!!, questionName, object : OnClickDeleteButtonListener {
                override fun onClickDeleteButton(position: Int) {
                }
            })
        listPopupWindowQuestions.setAdapter(listPopupWindowAdapter)
        listPopupWindowQuestions.anchorView = tvSelectYourQuestion
        listPopupWindowQuestions.isModal = true
        listPopupWindowQuestions.setOnItemClickListener { parent, view, position, id ->
            tvSelectYourQuestion.setText(questionList[position].question)
            listPopupWindowQuestions.dismiss()
        }
    }

    //method for click Listener
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.btnSubmit -> {
                if (etEnterYourName.text.trim().isEmpty()) {
                    CommonMethods.toast(activity!!, "Please Enter Name")
                } else if (etEnterNumber.text.trim().isNotEmpty() && !CommonMethods.isValidPhone(
                        etEnterNumber.text.trim().toString()
                    )
                ) {
                    CommonMethods.toast(activity!!, "Phone number is not valid")
                } else if (etEnterEmail.text.trim().isEmpty()) {
                    CommonMethods.toast(activity!!, "Email is required")
                } else if (!Patterns.EMAIL_ADDRESS.matcher(etEnterEmail.text.trim()).matches()) {
                    CommonMethods.toast(activity!!, "Enter a valid Email")
                } else if (tvSelectDesignation.text.trim().isEmpty()) {
                    CommonMethods.toast(activity!!, "Please Select Your Designation")
                } else if (tvSelectYourQuestion.text.trim().isEmpty()) {
                    CommonMethods.toast(activity!!, "Please Select Your Question")
                } else if (etSomethingTell.text.trim().isEmpty()) {
                    CommonMethods.toast(activity!!, "Please write about question")
                } else {
                    CommonMethods.showProgress(activity!!)
                    hashMapSubmit.put("name", etEnterYourName.text.toString().trim())
                    hashMapSubmit.put("user_id", userId)
                    hashMapSubmit.put("mobile_number", etEnterNumber.text.toString().trim())
                    hashMapSubmit.put("email", etEnterEmail.text.trim().toString())
                    hashMapSubmit.put("question", tvSelectYourQuestion.text.toString().trim())
                    hashMapSubmit.put("designation_id", designationId.trim())
                    hashMapSubmit.put(
                        "question_description",
                        etSomethingTell.text.toString().trim()
                    )
                    hashMapSubmit.put("company_name", etEnterCompanyName.text.toString().trim())
                    hashMapSubmit.put("language_id", ApplicationGlobal.english.toString())
                    RetrofitClient.getRetrofit().sendAskQuestion(hashMapSubmit)
                        .enqueue(object : Callback<BasicResponse> {
                            override fun onFailure(call: Call<BasicResponse>, t: Throwable) {
                                CommonMethods.dismissProgress()
                                if (CommonMethods.phoneIsOnline(activity!!)) {
                                    CommonMethods.toast(activity!!, t.message!!)
                                } else {
                                    CommonMethods.toast(
                                        activity!!,
                                        "Internet connection not available"
                                    )
                                }
                            }

                            override fun onResponse(
                                call: Call<BasicResponse>,
                                response: Response<BasicResponse>
                            ) {
                                CommonMethods.dismissProgress()
                                if (response.isSuccessful && response.body() != null) {
                                    CommonMethods.toast(activity!!, response.body()!!.message)
//                                    activity!!.onBackPressed()
                                    //clear all text from editText
                                    etEnterYourName.text.clear()
                                    etEnterNumber.text.clear()
                                    etEnterEmail.text.clear()
                                    tvSelectDesignation.text = ""
                                    etEnterCompanyName.text.clear()
                                    tvSelectYourQuestion.text = ""
                                    etSomethingTell.text.clear()
                                    //remove focus from all editText
                                    etSomethingTell.clearFocus()
                                    etEnterYourName.clearFocus()
                                    etEnterNumber.clearFocus()
                                    etEnterEmail.clearFocus()
                                    etSomethingTell.clearFocus()
                                } else {
                                    try {
                                        CommonMethods.getErrorMessageAndSessionExpire(
                                            activity!!,
                                            response.errorBody()!!
                                        )
//                                        CommonMethods.toast(
//                                            activity!!, "" +
//                                                    CommonMethods.getErrorMessage(response.errorBody()!!)
//                                        )
                                    } catch (e: Exception) {
                                    }
                                }
                            }
                        })
                }
            }
            R.id.ivBackArrow -> {
                activity!!.onBackPressed()
            }
            R.id.tvSelectYourQuestion -> {
                CommonMethods.hideKeyboard(activity!!)
                listPopupWindowQuestions.show()
            }
            R.id.tvSelectDesignation -> {
                CommonMethods.hideKeyboard(activity!!)
                listPopupWindow.show()
            }
        }
    }
}