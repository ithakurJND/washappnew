package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.ListPopupWindow
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.DrugsAToZAdapter
import com.rxhub.rxhub.realmModels.AllData
import com.rxhub.rxhub.realmModels.Result
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import com.rxhub.rxhub.webServices.model.ResultS
import com.rxhub.rxhub.webServices.model.SystemResp
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.fragment_drugs_a_to_z.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/*
* This class fragment_drugs_a_to_z show Drugs by Selected Alphabet.
*
* created by GTB on 16/04/2020
*/
class DrugsAToZFragment : BaseFragment(), View.OnClickListener {
    lateinit var listPopupWindow: ListPopupWindow
    val hashMap = HashMap<String, String>()
    var pageCount = 0
    var listByAlphabet = ArrayList<ResultS>()
    private var loading = true
    lateinit var alphabet: String
    lateinit var realm: Realm
    private var allDataArrayList = ArrayList<AllData>()
    private var filteredArrayList = ArrayList<AllData>()

    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_drugs_a_to_z
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        realm = Realm.getDefaultInstance()
        val alpha = ApplicationGlobal.preferenceManager.getAlphapet()
        alphabet = alpha.toString()
        searchByAlpha(alphabet.toString())
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails!!.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
        val layoutManager = GridLayoutManager(activity!!, 2)
        recyclerViewDrugs.layoutManager = layoutManager
//        recyclerViewDrugs.adapter =
//            DrugsAToZAdapter(resources.getStringArray(R.array.sortedByA), activity!!)
//        alphabetList = ArrayList<String>()
//        for(i in "A".."Z")
//        {  alphabetList.add(i.toString())
//        }
//         alphabetListAToZ = ArrayList<String>(26)
//        for(i in 0..26){
//            alphabetListAToZ[i] = (97 + i).toString()
//        }

        tvAlphabet.text = alpha
        tvSelectSortedBy.setText("Sorted By " + alpha)

        //medicineWithAlphabet(0, alphabet.toString(), true)
        tvSelectSortedBy.setOnClickListener {
            listPopupWindow.show()
//            CommonMethods.showDropDownList()
        }
        ivBackArrow.setOnClickListener {
            activity!!.onBackPressed()
        }
        val alphabetList = resources.getStringArray(R.array.aToz).toList()
        val alphabetAToZ = resources.getStringArray(R.array.alphabets).toList()
        listPopupWindow = ListPopupWindow(activity!!)
        listPopupWindow.setAdapter(
            ArrayAdapter(
                activity!!,
                android.R.layout.simple_dropdown_item_1line,
                alphabetList
            )
        )
        listPopupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT)
        listPopupWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT)
        listPopupWindow.anchorView = tvSelectSortedBy
        listPopupWindow.isModal = true
        listPopupWindow.setOnItemClickListener { parent, view, position, id ->
            tvSelectSortedBy.setText(alphabetList[position])
            tvAlphabet.visibility = View.GONE
            tvSortedBy.setText(alphabetList[position])
            Log.i("asdfasfasd", "" + alphabetAToZ.get(position))
            alphabet = alphabetAToZ.get(position)
            searchByAlpha(alphabet)
            listPopupWindow.dismiss()
        }
        ivProfile.setOnClickListener(this)

        //this for pagination
//        recyclerViewDrugs.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                if (dy > 0) //check for scroll down
//                {
//                    val visibleItemCount = layoutManager.getChildCount()
//                    val totalItemCount = layoutManager.getItemCount()
//                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
//                    if (loading) {
//                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
//                            loading = false
//                            Log.v("...", "Last Item Wow !")
//                            //Do pagination.. i.e. fetch new data
//                            progress_bar_sortedBy.visibility = View.VISIBLE
//                            //medicineWithAlphabet(pageCount, alphabet, false)
//                        }
//                    }
//                }
//            }
//        })
    }

    //click Listener method
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.ivProfile -> {
                CommonMethods.replaceFragmentInDrugsDetail(fragmentManager!!, MyProfileFragment())
            }
        }
    }

    //get Medicine With tha alphabets
//    fun medicineWithAlphabet(page: Int, alphabet: String, boolean: Boolean) {
//        if (boolean) CommonMethods.showProgress(activity!!)
//        hashMap.put("language_id", ApplicationGlobal.english.toString().trim())
//        hashMap.put("page_count", page.toString().trim())
//        hashMap.put("alphabet", alphabet.trim())
//        RetrofitClient.getRetrofit().getMedicines(hashMap)
//            .enqueue(object : Callback<SystemResp> {
//                override fun onFailure(call: Call<SystemResp>, t: Throwable) {
//                    CommonMethods.dismissProgress()
//                    if (CommonMethods.phoneIsOnline(activity!!)) {
//                        CommonMethods.toast(activity!!, t.message!!)
//                    } else {
//                        CommonMethods.toast(activity!!, "Internet connection not available")
//                    }
//                }
//
//                override fun onResponse(call: Call<SystemResp>, response: Response<SystemResp>) {
//                    CommonMethods.dismissProgress()
//                    if (response.isSuccessful && response.body() != null) {
//                        try {
//                            if (response.body()!!.count > 0) {
//                                recyclerViewDrugs.visibility = View.VISIBLE
//                                tvNoDataAToZ.visibility = View.GONE
//                                if (page == 0) {
//                                    pageCount = 0
//                                    val alphabetLis = response.body()!!.result
//                                    listByAlphabet.clear()
//                                    for (i in alphabetLis.indices) {
//                                        listByAlphabet.add(alphabetLis[i])
//                                    }
//                                    recyclerViewDrugs.adapter =
//                                        DrugsAToZAdapter(listByAlphabet, activity!!)
//                                    if (listByAlphabet.size == Constant.TOTAL_DATA) {
//                                        loading = true
//                                        pageCount++
//                                    }
//                                } else {
//                                    val newData = response.body()!!.result
//                                    listByAlphabet.addAll(newData)
//                                    recyclerViewDrugs.adapter!!.notifyDataSetChanged()
//                                    progress_bar_sortedBy.visibility = View.GONE
//                                    if (listByAlphabet.size == (page + 1) * Constant.TOTAL_DATA) {
//                                        pageCount++
//                                        loading = true
//                                    }
//                                }
//                            } else {
//                                recyclerViewDrugs.visibility = View.GONE
//                                tvNoDataAToZ.visibility = View.VISIBLE
//                            }
//                        } catch (e: Exception) {
//                        }
//                    } else {
//                        CommonMethods.getErrorMessageAndSessionExpire(
//                            activity!!,
//                            response.errorBody()!!
//                        )
//                    }
//                }
//            })
//    }

    private fun searchByAlpha(alpha: String) {
        allDataArrayList.clear()
        filteredArrayList.clear()
        val allData = realm.where<AllData>().findAllAsync()
        allDataArrayList.addAll(allData)
        for (i in 0 until allDataArrayList.size)
        { if (allDataArrayList[i].name!!.toLowerCase().startsWith(alpha.toLowerCase(),false))
        {
            filteredArrayList.add(allDataArrayList[i])
        }
        }

        Collections.sort(filteredArrayList, Comparator { obj1, obj2 ->
            // ## Ascending order
            obj1.name!!.toLowerCase().compareTo(obj2.name!!.toLowerCase(), false) // To compare string values
            // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
            // ## Descending order
            // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
            // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
        })
        Log.i("FilterList","$filteredArrayList")
        recyclerViewDrugs.adapter =
            DrugsAToZAdapter(filteredArrayList, activity!!)
    }
}