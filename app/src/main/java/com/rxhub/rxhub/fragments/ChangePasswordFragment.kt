package com.rxhub.rxhub.fragments

import android.os.Bundle
import com.rxhub.rxhub.R
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.BasicResponse
import kotlinx.android.synthetic.main.fragment_change_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern

/*
* This class is used to Change Password screen
*
* created by GTB on 16/04/2020
*/
class ChangePasswordFragment : BaseFragment() {
    val hashMap = HashMap<String, String>()
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_change_password
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ivBackArrow.setOnClickListener {
            activity!!.onBackPressed()
        }
        btnUpdatePass.setOnClickListener {
            if (etOldPassword.text.trim().isEmpty()) {
                CommonMethods.toast(activity!!, "Old password is required")
            } else if (etNewPassword.text.toString().trim().isEmpty()) {
                CommonMethods.toast(activity!!, "New password is required")
            } else if (!isValidPassword(etNewPassword.text.toString().trim())) {
                CommonMethods.toast(
                    activity!!, "Minimum eight characters, at least one upercase letter," +
                            " one lowercase letter, one number and one special character"
                )
            } else if (!etConfirmPass.text.toString().trim()
                    .equals(etNewPassword.text.toString().trim())
            ) {
                CommonMethods.toast(
                    activity!!,
                    "New Password and Confirm Password does not matched"
                )
            } else {
                CommonMethods.showProgress(activity!!)
                hashMap.put("old_password", etOldPassword.text.toString().trim())
                hashMap.put("new_password", etNewPassword.text.toString().trim())
                RetrofitClient.getRetrofit().changePassword(hashMap)
                    .enqueue(object : Callback<BasicResponse> {
                        override fun onFailure(call: Call<BasicResponse>, t: Throwable) {
                            CommonMethods.dismissProgress()
                            if (CommonMethods.phoneIsOnline(activity!!)) {
                                CommonMethods.toast(activity!!, t.message!!)
                            } else {
                                CommonMethods.toast(activity!!, "Internet connection not available")
                            }
                        }

                        override fun onResponse(
                            call: Call<BasicResponse>,
                            response: Response<BasicResponse>
                        ) {
                            CommonMethods.dismissProgress()
                            if (response.isSuccessful && response.body() != null) {
                                CommonMethods.toast(activity!!, response.body()!!.message)
                                activity!!.onBackPressed()
                            } else {
                                CommonMethods.getErrorMessageAndSessionExpire(
                                    activity!!,
                                    response.errorBody()!!
                                )
//                                CommonMethods.toast(
//                                    activity!!, "" +
//                                            CommonMethods.getErrorMessage(response.errorBody()!!)
//                                )
                            }
                        }
                    })
            }
        }
    }

    // this method is used to validation in password field
    fun isValidPassword(password: String): Boolean {
        val PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=!_])(?=\\S+$).{8,}$"
        val pattern = Pattern.compile(PASSWORD_PATTERN)
        val matcher = pattern.matcher(password)
        return matcher.matches()
    }
}