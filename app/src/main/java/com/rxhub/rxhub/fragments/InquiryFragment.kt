package com.rxhub.rxhub.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.core.view.GravityCompat
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.SettingContainerActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import com.rxhub.rxhub.webServices.model.BasicResponse
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_inquiry.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/*
* This class is used to show Inquiry screen
*
* created by GTB on 15/04/2020
*/
class InquiryFragment : BaseFragment(), View.OnClickListener {
    val hashMap = HashMap<String, String>()
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_inquiry
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //click Listener
        btnSubmit.setOnClickListener(this)
        ivProfile.setOnClickListener(this)
        ivMenu.setOnClickListener(this)
        //set user profile image
        setImage()
        Log.i("fjasdlfasdasdf", "onStart")
    }

    //method for click listener
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.ivMenu -> {
                activity!!.drawer_layout.openDrawer(GravityCompat.START)
            }
            R.id.ivProfile -> {
                startActivity(
                    Intent(activity!!, SettingContainerActivity::class.java)
                        .setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                        .putExtra("SettingScreen", "myProfile")
                )
            }
            R.id.btnSubmit -> {
                if (CommonMethods.phoneIsOnline(activity!!)) {
                    if (etEnterName.text.trim().isEmpty()) {
                        CommonMethods.toast(activity!!, "Please Enter Name")
                    } else if (etEnterPhone.text.trim().isEmpty()) {
                        CommonMethods.toast(activity!!, "Please Enter Modile Number")
                    } else if (!CommonMethods.isValidPhone(etEnterPhone.text.trim().toString())) {
                        CommonMethods.toast(activity!!, "Phone number is not valid")
                    } else if (etEnterEmail.text.trim().isEmpty()) {
                        CommonMethods.toast(activity!!, "Please Enter Email")
                    } else if (!Patterns.EMAIL_ADDRESS.matcher(etEnterEmail.text.trim())
                            .matches()
                    ) {
                        CommonMethods.toast(activity!!, "Enter a valid Email")
                    } else if (etMessage.text.trim().isEmpty()) {
                        CommonMethods.toast(activity!!, "Please Enter Message")
                    } else {
                        CommonMethods.showProgress(activity!!)
                        hashMap.put("name", etEnterName.text.toString().trim())
                        hashMap.put("mobile_number", etEnterPhone.text.toString().trim())
                        hashMap.put("message", etMessage.text.toString().trim())
                        hashMap.put("email", etEnterEmail.text.toString().trim())
                        RetrofitClient.getRetrofit().sendInquiry(hashMap)
                            .enqueue(object : Callback<BasicResponse> {
                                override fun onFailure(call: Call<BasicResponse>, t: Throwable) {
                                    CommonMethods.dismissProgress()
                                    if (CommonMethods.phoneIsOnline(activity!!)) {
                                        CommonMethods.toast(activity!!, t.message!!)
                                    } else {
                                        CommonMethods.toast(
                                            activity!!,
                                            "Internet connection not available"
                                        )
                                    }
                                }

                                override fun onResponse(
                                    call: Call<BasicResponse>,
                                    response: Response<BasicResponse>
                                ) {
                                    CommonMethods.dismissProgress()
                                    if (response.isSuccessful) {
                                        CommonMethods.toast(
                                            activity!!,
                                            "" + response.body()!!.message
                                        )
                                        etEnterName.text.clear()
                                        etEnterPhone.text.clear()
                                        etEnterEmail.text.clear()
                                        etMessage.text.clear()
                                        etEnterName.clearFocus()
                                        etEnterPhone.clearFocus()
                                        etEnterEmail.clearFocus()
                                        etMessage.clearFocus()
                                    } else {
                                        CommonMethods.getErrorMessageAndSessionExpire(
                                            activity!!,
                                            response.errorBody()!!
                                        )
//                                    CommonMethods.toast(
//                                        activity!!,
//                                        "" + CommonMethods.getErrorMessage(response.errorBody()!!)
//                                    )
                                    }
                                }
                            })
                    }
                }
                else
                    Toast.makeText(activity,"No internet connection", Toast.LENGTH_LONG).show()
            }

        }
    }

    fun setImage() {
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        try {
            if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
                if (getUserDetails!!.profile_pic!!.startsWith("http")) {
                    CommonMethods.setUrlImage(
                        activity!!,
                        getUserDetails!!.profile_pic.toString(),
                        ivProfile
                    )
                } else {
                    CommonMethods.setServerImage(
                        activity!!,
                        getUserDetails!!.profile_pic.toString(),
                        ivProfile
                    )
                }
            } else {
                GlideApp.with(activity!!)
                    .load(R.drawable.placeholder_small)
                    .circleCrop()
                    .into(ivProfile)
            }
        } catch (e: Exception) {
        }
    }

    override fun onResume() {
        super.onResume()
        setImage()
        Log.i("fjasdlfasdasdf", "onResume")
    }
}