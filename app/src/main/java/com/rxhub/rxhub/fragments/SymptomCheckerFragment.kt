package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.view.View
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import kotlinx.android.synthetic.main.fragment_symptom_checker.*

/*
* This class fragment_symptom_checker show the Symptom screen, in this Men and Women are to select any one.
*
* created by GTB on 16/04/2020
*/
class SymptomCheckerFragment : BaseFragment(), View.OnClickListener {
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_symptom_checker
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //click Listener
        ivBackArrow.setOnClickListener(this)
        tvMen.setOnClickListener(this)
        tvWomen.setOnClickListener(this)
        ivProfile.setOnClickListener(this)
        //get user data
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails.profile_pic,
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
    }

    //method for all click Listener
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.ivBackArrow -> {
                activity!!.onBackPressed()
            }
            R.id.ivProfile -> {
                CommonMethods.replaceFragmentInDrugsDetail(fragmentManager!!, MyProfileFragment())
            }
            R.id.tvMen -> {
                CommonMethods.replaceFragmentInDrugsDetail(
                    activity!!.supportFragmentManager,
                    HumanBodyPartsFragment(tvMen.text.toString(), Constant.MALE)
                )
            }
            R.id.tvWomen -> {
                CommonMethods.replaceFragmentInDrugsDetail(
                    activity!!.supportFragmentManager,
                    HumanBodyPartsFragment(tvWomen.text.toString(), Constant.FEMALE)
                )
            }
        }
    }
}