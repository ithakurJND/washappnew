package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.DrugsBySystemAdapter
import com.rxhub.rxhub.realmModels.AllData
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.fragment_indication_medicine.*
import java.util.*
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/*
* This class fragment_indication_medicine show the Drug by indication Medicine List screen
*
* create by GTB on 20/04/2020
 */
class IndicationMedicineFragment : BaseFragment(), View.OnClickListener {
    val hashMap = HashMap<String, String>()
    var pageCount = 0
    var getId = ""
    private var loading = true
    lateinit var realm: Realm
    val indicationMedList = ArrayList<AllData>()
    private lateinit var adapter: DrugsBySystemAdapter
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_indication_medicine
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        realm = Realm.getDefaultInstance()
        //click Listener
        ivBackArrow.setOnClickListener(this)
        ivProfile.setOnClickListener(this)
        //set indication name
        tvDrugsTypeName.text = ApplicationGlobal.preferenceManager.getDrugsName()
        getId = ApplicationGlobal.preferenceManager.getDrugsId().toString()
        Log.i("dfsasdfasdf", "" + getId)
        //set layout manager
        val layoutManager = GridLayoutManager(activity!!, 2)
        recyclerViewDrugsDisease.layoutManager = layoutManager
        //get medicines
        //getMedicine(0, true, "")
        //for search (EditText)
        etSearchDisease.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.length > 0)
                   filter(s.toString())
                else
                    adapter.filter(indicationMedList)
                // getMedicine(0, false, s.toString())
//                else getMedicine(0, false, "")
            }
        })
        //get user data and set user profile image
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails!!.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails!!.profile_pic.toString(),
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails!!.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
        showAllByIndication()

//        //this for pagination
//        recyclerViewDrugsDisease.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                if (dy > 0) //check for scroll down
//                {
//                    val visibleItemCount = layoutManager.getChildCount()
//                    val totalItemCount = layoutManager.getItemCount()
//                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
//                    if (loading) {
//                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
//                            loading = false
//                            Log.v("...", "Last Item Wow !")
//                            //Do pagination.. i.e. fetch new data
//                            progress_bar_indication.visibility = View.VISIBLE
//                            if (etSearchDisease.text.isEmpty()) {
//                            }
//                            // getMedicine(pageCount, false, "")
//                            else {
//                            }
//                            //getMedicine(pageCount, false, etSearchDisease.text.toString())
//                        }
//                    }
//                }
//            }
//        })
    }

    //method for click Listeners
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivBackArrow -> {
                activity!!.onBackPressed()
            }
            R.id.ivProfile -> {
                CommonMethods.replaceFragmentInDrugsDetail(
                    fragmentManager!!,
                    MyProfileFragment()
                )
            }
        }
    }

    //    //get Medicines by Indication
//    fun getMedicine(page: Int, boolean: Boolean, word: String) {
//        if (boolean) CommonMethods.showProgress(activity!!)
//        hashMap.put("language_id", ApplicationGlobal.english.toString().trim())
//        hashMap.put("disease_id", getId.trim())
//        hashMap.put("page_count", page.toString())
//        hashMap.put("search", word.trim())
//        RetrofitClient.getRetrofit().getMedicines(hashMap)
//            .enqueue(object : Callback<SystemResp> {
//                override fun onFailure(call: Call<SystemResp>, t: Throwable) {
//                    CommonMethods.dismissProgress()
//                    if (CommonMethods.phoneIsOnline(activity!!)) {
//                        CommonMethods.handelInternet(activity!!, t.message!!)
//                    } else {
//                        CommonMethods.toast(activity!!, "Internet connection not available")
//                    }
//                }
//
//                override fun onResponse(call: Call<SystemResp>, response: Response<SystemResp>) {
//                    CommonMethods.dismissProgress()
//                    if (response.isSuccessful && response.body() != null) {
//                        if (response.body()!!.count > 0) {
//                            recyclerViewDrugsDisease.visibility = View.VISIBLE
//                            tvNoDataDisease.visibility = View.GONE
//                            if (page == 0) {
//                                pageCount = 0
//                                indicationMedList.clear()
//                                val a = response.body()!!.result
//                                for (i in a.indices) {
//                                    indicationMedList.add(a[i])
//                                }
//                                recyclerViewDrugsDisease.adapter =
//                                    DrugsBySystemAdapter(
//                                        indicationMedList,
//                                        activity!!
//                                    )
//                                if (indicationMedList.size == Constant.TOTAL_DATA) {
//                                    loading = true
//                                    pageCount++
//                                }
//                            } else {
//                                val newData = response.body()!!.result
//                                indicationMedList.addAll(newData)
//                                recyclerViewDrugsDisease.adapter!!.notifyDataSetChanged()
//                                progress_bar_indication.visibility = View.GONE
//                                Log.i("paginatoins", "" + indicationMedList.size)
//                                if (indicationMedList.size == (page + 1) * Constant.TOTAL_DATA) {
//                                    pageCount++
//                                    loading = true
//                                }
//                            }
//                        } else {
//                            recyclerViewDrugsDisease.visibility = View.GONE
//                            tvNoDataDisease.visibility = View.VISIBLE
//                        }
//                    } else {
//                        CommonMethods.getErrorMessageAndSessionExpire(
//                            activity!!,
//                            response.errorBody()!!
//                        )
////                        CommonMethods.toast(
////                            activity!!, "" +
////                                    CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
//                    }
//                }
//            })
//    }
    private fun showAllByIndication() {
        indicationMedList.clear()
        val demoList = ArrayList<AllData>()
        val allData = realm.where<AllData>().findAllAsync()
        demoList.addAll(allData)
        for (data in demoList) {
            if (data.med_system_name != null)
                if (data.indication_name!!.equals(ApplicationGlobal.preferenceManager.getDrugsName()))
                    indicationMedList.add(data)
        }
        Collections.sort(indicationMedList, Comparator { obj1, obj2 ->
            // ## Ascending order
            obj1.name!!.toLowerCase().compareTo(obj2.name!!.toLowerCase(), false) // To compare string values
            // return Integer.valueOf(obj1.empId).compareTo(Integer.valueOf(obj2.empId)); // To compare integer values
            // ## Descending order
            // return obj2.firstName.compareToIgnoreCase(obj1.firstName); // To compare string values
            // return Integer.valueOf(obj2.empId).compareTo(Integer.valueOf(obj1.empId)); // To compare integer values
        })
        adapter = DrugsBySystemAdapter(indicationMedList, activity!!)
        recyclerViewDrugsDisease.adapter = adapter
    }

    fun filter(word: String) {
        val temArrayList = ArrayList<AllData>()
        for (d in indicationMedList) {
            word.apply {
                if (d.name!!.toLowerCase().startsWith(word.toLowerCase()))
                    temArrayList.add(d)
            }
        }
        adapter.filter(temArrayList)
    }
}