package com.rxhub.rxhub.fragments

import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.HumanBodyPartsAdapter
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import com.rxhub.rxhub.webServices.model.BodyParts
import com.rxhub.rxhub.webServices.model.ResultB
import kotlinx.android.synthetic.main.fragment_human_bodyparts.*
import retrofit2.Call
import retrofit2.Response

/*
* This class fragment_human_bodyparts show Men and Women body parts in this screen.
*
* created by GTB on 16/04/2020
*/
class HumanBodyPartsFragment(val gender: String, val genderId: Int) : BaseFragment(),
    View.OnClickListener {
    var hashMap = HashMap<String, String>()
    var pageCount = 0
    private var loading = true
    var bodyPartList = ArrayList<ResultB>()
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_human_bodyparts
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //get body part name
        getBodyPart(0, true)
        //set click Listener
        ivBackArrow.setOnClickListener(this)
        ivProfile.setOnClickListener(this)
        val layoutManager = LinearLayoutManager(activity!!)
        recyclerBodyParts.layoutManager = layoutManager
        //set image by gender
        tvGender.text = gender
        if (gender.equals("Men")) {
            GlideApp.with(activity!!)
                .asGif()
                .load(R.drawable.skeleton_man)
                .into(ivGenderImage)
        } else {
            GlideApp.with(activity!!)
                .asGif()
                .load(R.drawable.skeleton_woman)
                .into(ivGenderImage)
        }
        //set underline the text
        tvGender.setPaintFlags(tvGender.getPaintFlags() or Paint.UNDERLINE_TEXT_FLAG)
        //this for pagination
        recyclerBodyParts.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) //check for scroll down
                {
                    val visibleItemCount = layoutManager.getChildCount()
                    val totalItemCount = layoutManager.getItemCount()
                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            loading = false
                            Log.v("...", "Last Item Wow !")
                            //Do pagination.. i.e. fetch new data
                            progress_bar_symptom.visibility = View.VISIBLE
                            getBodyPart(pageCount, false)
                        }
                    }
                }
            }
        })
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )
        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails.profile_pic,
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
    }

    //click Listener method
    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.ivBackArrow -> {
                activity!!.onBackPressed()
            }
        }
    }

    //method for getting body parts name from server
    fun getBodyPart(page: Int, boolean: Boolean) {
        if (boolean) {
            CommonMethods.showProgress(activity!!)
        }
        hashMap.put("language_id", ApplicationGlobal.english.toString())
        hashMap.put("page_count", page.toString())
        hashMap.put("gender", genderId.toString().trim())
        RetrofitClient.getRetrofit().getBodyPartCategory(hashMap)
            .enqueue(object : retrofit2.Callback<BodyParts> {
                override fun onFailure(call: Call<BodyParts>, t: Throwable) {
                    CommonMethods.dismissProgress()
                    if (CommonMethods.phoneIsOnline(activity!!)) {
                        CommonMethods.toast(activity!!, t.message!!)
                    } else {
                        CommonMethods.toast(activity!!, "Internet connection not available")
                    }
                }

                override fun onResponse(call: Call<BodyParts>, response: Response<BodyParts>) {
                    CommonMethods.dismissProgress()
                    if (response.isSuccessful && response.body() != null) {
                        if (page == 0) {
                            if (response.body()!!.count > 0) {
                                recyclerBodyParts.visibility = View.VISIBLE
                                tvNoDataHumanBody.visibility = View.GONE
                                val re = response.body()!!.result
                                bodyPartList.clear()
                                Log.i("bodyparts", "" + re)
                                for (i in re.indices) {
                                    bodyPartList.add(re[i])
                                }
                                recyclerBodyParts.adapter =
                                    HumanBodyPartsAdapter(activity!!, bodyPartList)
                                if (bodyPartList.size == Constant.TOTAL_DATA) {
                                    loading = true
                                    pageCount++
                                }
                            } else {
                                recyclerBodyParts.visibility = View.GONE
                                tvNoDataHumanBody.visibility = View.VISIBLE
                            }
                        } else {
                            val newData = response.body()!!.result
                            bodyPartList.addAll(
                                newData
                            )
                            recyclerBodyParts.adapter!!.notifyDataSetChanged()
                            progress_bar_symptom.visibility = View.GONE
                            if (bodyPartList.size == (page + 1) * Constant.TOTAL_DATA) {
                                pageCount++
                                loading = true
                            }
                        }
                    } else {
                        CommonMethods.getErrorMessageAndSessionExpire(
                            activity!!,
                            response.errorBody()!!
                        )
//                        CommonMethods.toast(
//                            activity!!, "" +
//                                    CommonMethods.getErrorMessage(response.errorBody()!!)
//                        )
                    }
                }
            })
    }
}