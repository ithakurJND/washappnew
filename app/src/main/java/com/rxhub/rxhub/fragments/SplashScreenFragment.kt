package com.rxhub.rxhub.fragments

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.MainActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods

/*
* This class is Splash screen of the app
*
* created by GTB on 14/04/2020
*/
class SplashScreenFragment : BaseFragment() {
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_splash_screen_layout
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        try {
            Handler().postDelayed({
                if (ApplicationGlobal.preferenceManager.getSessionId() != "") {
                    val intent = Intent(activity!!, MainActivity::class.java)
                    intent.putExtra("medicineId", arguments?.getString("medicineId"))
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    startActivity(intent)
                    activity!!.finish()
                } else {
                    CommonMethods.replaceFragmentInSplashScreen(
                        fragmentManager!!,
                        TutorialFragment()
                    )
                }
            }, 1500)
        } catch (e: Exception) {
        }
    }
}