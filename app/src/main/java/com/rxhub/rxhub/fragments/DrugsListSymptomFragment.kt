package com.rxhub.rxhub.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.adapters.DrugsAToZAdapter
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.utils.Constant
import com.rxhub.rxhub.utils.GlideApp
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.BasicInfoResp
import com.rxhub.rxhub.webServices.model.ResultS
import com.rxhub.rxhub.webServices.model.SystemResp
import kotlinx.android.synthetic.main.fragment_drugs_list_symptom.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/*
* This class fragment_drugs_list_symptom show the Symptoms medicines, screen
*
* created by GTB on 17/04/2020
*/
class DrugsListSymptomFragment(val problem: String, val symptomId: Int) : BaseFragment(),
    View.OnClickListener {
    val hashMap = HashMap<String, String>()
    var pageCount = 0
    private var loading = false
    val medicneList = ArrayList<ResultS>()
    override fun getLayoutToInsert(): Int {
        return R.layout.fragment_drugs_list_symptom
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        //set symptom name
        tvDrugsFor.text = "Drugs for " + problem
        //get medicine method
     //   medicineWithSymptom(0, true)
        //click Listener here
        ivBackArrow.setOnClickListener(this)
        ivProfile.setOnClickListener(this)

        val layoutManager = GridLayoutManager(activity!!, 2)
        recyclerViewDrugsList.layoutManager = layoutManager

        //this is pagination
        recyclerViewDrugsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) //check for scroll down
                {
                    val visibleItemCount = layoutManager.getChildCount()
                    val totalItemCount = layoutManager.getItemCount()
                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            loading = false
                            Log.v("...", "Last Item Wow !")
                            //Do pagination.. i.e. fetch new data
                            progress_bar_symptom_medicine1.visibility = View.VISIBLE
                           // medicineWithSymptom(pageCount, false)
                        }
                    }
                }
            }
        })
        val getUserDetails = Gson().fromJson(
            ApplicationGlobal.preferenceManager.getBasicDetails(),
            BasicInfoResp::class.java
        )

        if (getUserDetails!!.profile_pic!!.isNotEmpty()) {
            if (getUserDetails.profile_pic!!.startsWith("http")) {
                CommonMethods.setUrlImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfile
                )
            } else {
                CommonMethods.setServerImage(
                    activity!!,
                    getUserDetails.profile_pic.toString(),
                    ivProfile
                )
            }
        } else {
            GlideApp.with(activity!!)
                .load(R.drawable.placeholder_small)
                .circleCrop()
                .into(ivProfile)
        }
    }

    //method for click Listener
    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.ivProfile -> {
                CommonMethods.replaceFragmentInDrugsDetail(
                    fragmentManager!!,
                    MyProfileFragment()
                )
            }
            R.id.ivBackArrow -> {
                activity!!.onBackPressed()
            }

        }
    }

    //method for get Medicine by symptoms
//    fun medicineWithSymptom(page: Int, boolean: Boolean) {
//        if (boolean) {
//            CommonMethods.showProgress(activity!!)
//        }
//        hashMap.put("language_id", ApplicationGlobal.english.toString().trim())
//        hashMap.put("page_count", page.toString().trim())
//        hashMap.put("symptom_id", symptomId.toString().trim())
//        RetrofitClient.getRetrofit().getMedicines(hashMap)
//            .enqueue(object : Callback<SystemResp> {
//                override fun onFailure(call: Call<SystemResp>, t: Throwable) {
//                    CommonMethods.dismissProgress()
//                    CommonMethods.toast(activity!!, t.message!!)
//                }
//
//                override fun onResponse(call: Call<SystemResp>, response: Response<SystemResp>) {
//                    CommonMethods.dismissProgress()
//                    if (response.isSuccessful && response.body() != null) {
//                        if (page == 0) {
//                            if (response.body()!!.count > 0) {
//                                recyclerViewDrugsList.visibility = View.VISIBLE
//                                tvNoDataSymptom.visibility = View.GONE
//                                val alphabetLis = response.body()!!.result
//                                medicneList.clear()
//                                for (i in alphabetLis.indices) {
//                                    medicneList.add(alphabetLis[i])
//                                }
//                                recyclerViewDrugsList.adapter =
//                                    DrugsAToZAdapter(medicneList, activity!!)
//                                if (medicneList.size == Constant.TOTAL_DATA) {
//                                    loading = true
//                                    pageCount++
//                                }
//                            } else {
//                                recyclerViewDrugsList.visibility = View.GONE
//                                tvNoDataSymptom.visibility = View.VISIBLE
//                            }
//                        } else {
//                            val newData = response.body()!!.result
//                            medicneList.addAll(newData)
//                            recyclerViewDrugsList.adapter!!.notifyDataSetChanged()
//
//                            if (medicneList.size == (page + 1) * Constant.TOTAL_DATA) {
//                                pageCount++
//                                loading = true
//                            }
//                        }
//                        progress_bar_symptom_medicine1.visibility = View.GONE
//                    } else {
//                        CommonMethods.getErrorMessageAndSessionExpire(
//                            activity!!,
//                            response.errorBody()!!
//                        )
////                        CommonMethods.toast(
////                            activity!!,
////                            "" + CommonMethods.getErrorMessage(response.errorBody()!!)
////                        )
//                    }
//                }
//            })
//    }
}