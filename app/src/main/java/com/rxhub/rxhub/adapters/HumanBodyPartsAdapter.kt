package com.rxhub.rxhub.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.rxhub.rxhub.R
import com.rxhub.rxhub.fragments.BodyPartDetailsFragment
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.webServices.model.ResultB
import kotlinx.android.synthetic.main.item_human_body_parts.view.*

class HumanBodyPartsAdapter(val context: Context, val arrayList: ArrayList<ResultB>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_human_body_parts, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val bodyparts = holder as MyViewHolder
        bodyparts.bodyName.text = arrayList[position].name
        bodyparts.bodyName.setOnClickListener {
            CommonMethods.replaceFragmentInDrugsDetail(
                (context as AppCompatActivity).supportFragmentManager,
                BodyPartDetailsFragment(
                    arrayList[position].name,
                    arrayList[position].id,
                    arrayList[position].image
                )
            )
        }
    }

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val bodyName = item.tvBodyPartName
    }
}