package com.rxhub.rxhub.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.rxhub.rxhub.R
import com.rxhub.rxhub.fragments.DrugsDetailsFragment
import com.rxhub.rxhub.realmModels.AllData
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import kotlinx.android.synthetic.main.item_textview_drugs_name_layout.view.*

class HomeSearchAdapter(val context: Context, var searchList: ArrayList<AllData>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return  MyViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_textview_drugs_name_layout, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return searchList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemHolder = holder as MyViewHolder
        itemHolder.textShow.text = searchList[position].name
    }

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val textShow = item.tvShowName
       init {
            textShow.setOnClickListener {
                Log.i("mediname", "" + searchList[adapterPosition].id.toString())
                ApplicationGlobal.preferenceManager.setMedicineId(searchList[adapterPosition].id.toString())
                CommonMethods.replaceFragmentInSetting(
                    (context as AppCompatActivity).supportFragmentManager,
                    DrugsDetailsFragment()
                )
            }
        }
    }

    fun filter(filterList: ArrayList<AllData>) {
        searchList = filterList
        notifyDataSetChanged()
    }
}