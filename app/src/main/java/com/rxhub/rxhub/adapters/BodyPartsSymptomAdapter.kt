package com.rxhub.rxhub.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.rxhub.rxhub.R
import com.rxhub.rxhub.fragments.DrugsListSymptomFragment
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.webServices.model.ResultP
import kotlinx.android.synthetic.main.item_human_body_parts.view.*

class BodyPartsSymptomAdapter(val context: Context, val symptomList: ArrayList<ResultP>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_human_body_parts, parent, false)
        )
        return view
    }

    override fun getItemCount(): Int {
        return symptomList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = holder as MyViewHolder
        item.symptomText.text = symptomList[position].name
        item.symptomText.setOnClickListener {
            CommonMethods.replaceFragmentInDrugsDetail(
                (context as AppCompatActivity).supportFragmentManager,
                DrugsListSymptomFragment(
                    symptomList[position].name,
                    symptomList[position].symptom_id
                )
            )
        }
    }

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val symptomText = item.tvBodyPartName
    }
}