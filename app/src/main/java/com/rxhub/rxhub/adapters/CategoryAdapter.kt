package com.rxhub.rxhub.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.DrugsDetailsActivity
import com.rxhub.rxhub.realmModels.AllData
import com.rxhub.rxhub.realmModels.Result
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.webServices.model.ResultS
import kotlinx.android.synthetic.main.item_textview_drugs_name_layout.view.*

class CategoryAdapter(
    var realmArrayList: ArrayList<Result>?,
    val context: Context,
    val drug: String
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = MyViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_textview_drugs_name_layout, parent, false)
        )
        view.click()
        return view
    }

    override fun getItemCount(): Int {
        return realmArrayList!!.size
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemHolder = holder as MyViewHolder
        when(drug)
        {
            "Generic"->{ itemHolder.textShow.text = realmArrayList!![position].name}
            "System"->{ itemHolder.textShow.text = realmArrayList!![position].name }
            "Brand"->{ itemHolder.textShow.text = realmArrayList!![position].name}
            "Disease"->{ itemHolder.textShow.text = realmArrayList!![position].name}
        }
    }

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val textShow = item.tvShowName
        fun click() {
            textShow.setOnClickListener {
                ApplicationGlobal.preferenceManager.setDrugsId(realmArrayList!![adapterPosition].id.toString())
                ApplicationGlobal.preferenceManager.setDrugsName(realmArrayList!![adapterPosition].name!!)
                if (drug.equals("Disease")) {
                    val intent = Intent(context, DrugsDetailsActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    intent.putExtra("whichScreen", "Disease")
                    context.startActivity(intent)
                } else if (drug.equals("Brand")) {
                    val intent = Intent(context, DrugsDetailsActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    intent.putExtra("whichScreen", "Brand")
                    context.startActivity(intent)
                } else if (drug.equals("Generic")) {
                    val intent = Intent(context, DrugsDetailsActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    intent.putExtra("whichScreen", "Generic")
                    context.startActivity(intent)
                } else {
                    val intent = Intent(context, DrugsDetailsActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    intent.putExtra("whichScreen", "drugsSystem")
                    context.startActivity(intent)
                }

            }
        }
    }
    fun filter(temList:ArrayList<Result>)
    {
        realmArrayList = temList
        notifyDataSetChanged()
    }
}