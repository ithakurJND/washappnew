package com.rxhub.rxhub.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.DrugsDetailsActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import com.rxhub.rxhub.webServices.model.ResultSearchedMed
import kotlinx.android.synthetic.main.item_medicine_history.view.*

class MedicineHistoryAdapter(val context: Context, val medicineList: ArrayList<ResultSearchedMed>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_medicine_history, parent, false)
        )
        view.click()
        return view
    }

    override fun getItemCount(): Int {
        return medicineList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val items = holder as MyViewHolder
        items.indication.text = medicineList[position].indication
        items.medName.text = medicineList[position].name
        items.date.text =
            CommonMethods.getDateAndTime(CommonMethods.getLocalTimeFromUTC(medicineList[position].created_at)!!)

    }

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val medName = item.tvMedicineText
        val indication = item.tvIndicationText
        val date = item.tvDateText
        val cardViewHistory = item.cardViewHistory
        fun click() {
            cardViewHistory.setOnClickListener {
                ApplicationGlobal.preferenceManager.setMedicineId(medicineList[position].medicine_id)
                val intent = Intent(context, DrugsDetailsActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                intent.putExtra("whichScreen", "details")
                context.startActivity(intent)
            }
        }
    }
}