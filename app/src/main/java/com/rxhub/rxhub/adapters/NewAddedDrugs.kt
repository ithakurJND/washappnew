package com.rxhub.rxhub.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.DrugsDetailsActivity
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.webServices.model.Result
import com.rxhub.rxhub.webServices.model.ResultS
import kotlinx.android.synthetic.main.item_textview_drugs_name_layout.view.*

class NewAddedDrugs(val context: Context, val newDrugsList: ArrayList<com.rxhub.rxhub.realmModels.Result>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = MyViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_textview_drugs_name_layout, parent, false)
        )
        view.click()
        return view
    }

    override fun getItemCount(): Int {
        return newDrugsList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemHolder = holder as MyViewHolder
        itemHolder.textShow.text = newDrugsList[position].name
    }

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val textShow = item.tvShowName
        fun click() {
            textShow.setOnClickListener {
                ApplicationGlobal.preferenceManager.setMedicineId(newDrugsList[position].id.toString())
                val intent = Intent(context, DrugsDetailsActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
                intent.putExtra("whichScreen", "details")
                context.startActivity(intent)
            }
        }
    }
}