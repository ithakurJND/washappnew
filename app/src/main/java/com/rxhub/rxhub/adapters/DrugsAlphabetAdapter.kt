package com.rxhub.rxhub.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.rxhub.rxhub.R
import com.rxhub.rxhub.fragments.DrugsAToZFragment
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import kotlinx.android.synthetic.main.item_textview_drugs_name_layout.view.*

class DrugsAlphabetAdapter(val arrayList: Array<String>, val context: Context) :
    RecyclerView.Adapter<DrugsAlphabetAdapter.MyViewHolder>() {
    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val textShow = item.tvShowName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.item_textview_drugs_name_layout, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val itemHolder = holder as MyViewHolder
        itemHolder.textShow.text = arrayList[position]
        itemHolder.textShow.setOnClickListener {
            ApplicationGlobal.preferenceManager.setAlphabet(arrayList[position])
            CommonMethods.replaceFragmentInDrugsDetail(
                (context as AppCompatActivity).supportFragmentManager,
                DrugsAToZFragment()
            )
        }
    }
}