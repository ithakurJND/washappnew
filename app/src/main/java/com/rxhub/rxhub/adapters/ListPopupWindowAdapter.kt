package com.rxhub.rxhub.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView


class ListPopupWindowAdapter internal constructor(
    mActivity: Activity,
    dataSource: List<String>,
    clickDeleteButtonListener: OnClickDeleteButtonListener
) :
    BaseAdapter() {
    private var mDataSource: List<String> = ArrayList()
    private val layoutInflater: LayoutInflater
    private val clickDeleteButtonListener: OnClickDeleteButtonListener
    override fun getCount(): Int {
        return mDataSource.size
    }

    override fun getItem(position: Int): String {
        return mDataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var convertView: View? = convertView
        var holder: ViewHolder
        if (convertView == null) {
            holder = ViewHolder()
            convertView = layoutInflater.inflate(com.rxhub.rxhub.R.layout.item, null)
            holder.tvTitle = convertView.findViewById(com.rxhub.rxhub.R.id.text_title)
            convertView.setTag(holder)
        } else {
            holder = convertView.getTag() as ViewHolder
        }

        // bind data
        holder.tvTitle!!.text = getItem(position)
//        holder.btnDelete.setOnClickListener(object : View.OnClickListener {
//            override fun onClick(v: View?) {
//                clickDeleteButtonListener.onClickDeleteButton(position)
//            }
//        })
        return convertView
    }

    inner class ViewHolder {
        var tvTitle: TextView? = null
    }

    // interface to return callback to activity
    interface OnClickDeleteButtonListener {
        fun onClickDeleteButton(position: Int)
    }

    init {
        mDataSource = dataSource
        layoutInflater = mActivity.layoutInflater
        this.clickDeleteButtonListener = clickDeleteButtonListener
    }
}