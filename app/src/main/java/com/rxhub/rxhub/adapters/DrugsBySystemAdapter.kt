package com.rxhub.rxhub.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.rxhub.rxhub.R
import com.rxhub.rxhub.fragments.DrugsDetailsFragment
import com.rxhub.rxhub.realmModels.AllData
import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.CommonMethods
import kotlinx.android.synthetic.main.item_textview_drugs_name_layout.view.*

class DrugsBySystemAdapter(var arrayList: ArrayList<AllData>, val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = MyViewHolder(
            LayoutInflater.from(context)
                .inflate(R.layout.item_textview_drugs_name_layout, parent, false)
        )
        view.click()
        return view
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemHolder = holder as MyViewHolder
        itemHolder.textShow.text = arrayList[position].name
    }

    inner class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val textShow = item.tvShowName
        fun click() {
            textShow.setOnClickListener {
                ApplicationGlobal.preferenceManager.setMedicineId(arrayList[position].id.toString())
                CommonMethods.replaceFragmentInDrugsDetail(
                    (context as AppCompatActivity).supportFragmentManager,
                    DrugsDetailsFragment()
                )
            }
        }
    }

    fun filter(dataArrayList: ArrayList<AllData>) {
        arrayList = dataArrayList
        notifyDataSetChanged()
    }
}