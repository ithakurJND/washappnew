package com.rxhub.rxhub.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rxhub.rxhub.R
import com.rxhub.rxhub.webServices.model.ResultF
import kotlinx.android.synthetic.main.item_faqs_layout.view.*

class FaqsAdapter(val context: Context, val questionList: ArrayList<ResultF>) :
    RecyclerView.Adapter<FaqsAdapter.MyViewHolder>() {
    class MyViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val question = item.tvQuestion
        val answer = item.tvAnswer
        val showHide = item.ivOneQuestion

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_faqs_layout, parent, false)
        )

        return view
    }

    override fun getItemCount(): Int {
        return questionList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.question.text = questionList[position].question
        holder.answer.text = questionList[position].answer
        holder.showHide.setOnClickListener {
            if (holder.showHide.drawable.constantState ==
                context.resources.getDrawable(R.drawable.down_arrow).constantState
            ) {
                holder.answer.visibility = View.GONE
                holder.showHide.setImageResource(R.drawable.up_arrow)
            } else {
                holder.answer.visibility = View.VISIBLE
                holder.showHide.setImageResource(R.drawable.down_arrow)
            }
        }
    }
}