package com.rxhub.rxhub.webServices.model

data class SignUpResp(
    val token: String,
    val user: User
)

data class User(
    val age: Int,
    val apple_id: String,
    val company_name: String,
    val country_code: String,
    val created_at: String,
    val designation_id: Int?,
    val email: String,
    val fb_id: String,
    val gender: Int,
    val google_id: String,
    val id: Int,
    val is_notification_enable: Int,
    val name: String,
    val phone_no: String,
    val profile_pic: String,
    val updated_at: Any,
    val is_added: Int
)

data class ErrorBody(
    val error: String,
    val error_description: String
)


data class Designations(
    val count: Int,
    val result: List<Result>
)

data class Result(
    val id: Int,
    val name: String
)

data class BasicResponse(
    val message: String
)

data class BasicInfoResp(
    val age: Int?,
    val apple_id: String?,
    val company_name: String?,
    val country_code: String?,
    val created_at: String?,
    val designation_category_id: Int?,
    val designation_id: Int?,
    val email: String?,
    val fb_id: String?,
    val gender: Int?,
    val google_id: String?,
    val id: Int?,
    val is_notification_enable: Int?,
    val is_history_enable: Int?,
    val name: String?,
    val phone_no: String?,
    val is_active: Int,
    val profile_pic: String?,
    val updated_at: Any?
)

data class ForgotPass(
    val message: String
)

data class LogOut(
    val message: String
)

data class ErrorMessage(
    val message: String
)

data class SystemResp(
    val count: Int,
    val result: List<ResultS>
)

data class ResultS(
    val id: Int,
    val name: String,
    val medicine_id: Int
)

data class BodyParts(
    val count: Int,
    val result: List<ResultB>
)

data class ResultB(
    val id: Int,
    val image: String,
    val name: String
)

data class FaqsResponse(
    val count: Int,
    val result: List<ResultF>
)

data class ResultF(
    val answer: String,
    val id: Int,
    val question: String
)

data class QuestionResponse(
    val count: Int,
    val result: List<ResultQ>
)

data class ResultQ(
    val id: Int,
    val question: String
)

data class BodyPartSymptom(
    val count: Int,
    val result: List<ResultP>
)

data class ResultP(
    val name: String,
    val symptom_id: Int,
    val translation_id: Int
)

data class ImageResponse(
    val file_name: String
)

data class MedicineDetailsResponse(
    val brand_name: String?,
    val category: String?,
    val classification: List<Classification>,
    val designation_id: Int?,
    val drug_form: String?,
    val generic_name: String?,
    val id: Int?,
    val image: String?,
    val indication_name: String?,
    val instruction: List<Instruction>,
    val language: Int?,
    val medicine_id: Int?,
    val name: String?
)

data class Classification(
    val classification: String,
    val language: Int
)

data class Instruction(
    val instruction: String,
    val language: Int
)


//data class MedicineDetailsResponse(
//    val brand_name: String?,
//    val category: String?,
//    val classification: String?,
//    val drug_form: String?,
//    val generic_name: String?,
//    val id: Int?,
//    val indication: String?,
//    val indication_name: String?,
//    val instruction: String?,
//    val language: Int?,
//    val medicine_id: Int?,
//    val name: String?,
//    val symptom: String?,
//    val image: String?
//)

data class SearchedMedicineResponse(
    val count: Int,
    val result: List<ResultSearchedMed>
)

data class ResultSearchedMed(
    val created_at: String,
    val indication: String,
    val medicine_id: String,
    val name: String
)

data class HomeImageResp(
    val created_at: String?,
    val id: Int?,
    val image_a_z: String?,
    val image_brand: String?,
    val image_generic: String?,
    val image_indication: String?,
    val image_symtom: String?,
    val image_system: String?,
    val updated_at: String?
)

data class DesignationCategoryResponse(
    val count: Int,
    val result: List<ResultCat>
)

data class ResultCat(
    val id: Int,
    val name: String
)

data class AcknowledgementResponse(
    val acknowledgement: String,
    val reference: String,
    val about: String,
    val privacy_and_policy: String,
    val terms_and_conditions: String,
    val how_it_works: String,
    val terms_of_use: String,
    val disclaimer: String,
    val general_instructions: String
)


data class AllDataList(
    val brand_name: String,
    val category: String,
    val classification: List<Classification>,
    val designation_id: Int,
    val drug_form: String,
    val generic_name: String,
    val id: Int,
    val image: String,
    val indication_name: String,
    val instruction: List<Instruction>,
    val language: Int,
    val medicine_id: Int,
    val name: String
)

class SyncAllDbData : ArrayList<SyncDbData>()
data class SyncDbData(
    val id: Int,
    val table_name: String,
    val updated_at: String
)

data class Message(
    val message: String
)