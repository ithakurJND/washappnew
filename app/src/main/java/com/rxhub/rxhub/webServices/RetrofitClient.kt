package com.rxhub.rxhub.webServices

import com.rxhub.rxhub.utils.ApplicationGlobal
import com.rxhub.rxhub.utils.Constant
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient {
    companion object {
        lateinit var retrofitPlace: Retrofit
        fun getRetrofit(): Api {
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            val client = OkHttpClient.Builder()
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
            client.addInterceptor(Interceptor { chain: Interceptor.Chain ->
                val request = chain.request()
                val newRequest = request.newBuilder().header(
                    "Authorization", "Bearer  " +
                            ApplicationGlobal.preferenceManager.getSessionId().toString()
                )
                chain.proceed(newRequest.build())
            })
            retrofitPlace = Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .client(client.addInterceptor(interceptor).build())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofitPlace.create(Api::class.java)
        }

        fun getInstence(): Retrofit {
            return retrofitPlace
        }
    }
}