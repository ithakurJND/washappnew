package com.rxhub.rxhub.webServices

import com.rxhub.rxhub.realmModels.*
import com.rxhub.rxhub.webServices.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface Api {
    @POST("signup")
    fun signUp(@Body data: HashMap<String, String>): Call<SignUpResp>

    @PUT("profile")
    fun setBasicInfo(@Body data: HashMap<String, Any>): Call<BasicResponse>

    @GET("profile")
    fun getUserData(): Call<BasicInfoResp>

    @GET("designations")
    fun getDesignation(@QueryMap data: HashMap<String, Any>): Call<Designations>

    @POST("login")
    fun logIn(@Body data: HashMap<String, String>): Call<SignUpResp>

    @POST("password/forgot")
    fun forgotPass(@Body data: HashMap<String, String>): Call<ForgotPass>

    @GET("systems")
    fun getSystemCategory(@QueryMap data: HashMap<String, String>): Call<SystemCategory>

    @GET("brands")
    fun getBrandCategory(@QueryMap data: HashMap<String, String>): Call<BrandCategory>

    @GET("diseases")
    fun getDiseaseCategory(@QueryMap data: HashMap<String, String>): Call<DiseaseCategory>

    @GET("generic-names")
    fun getGenericDrugs(@QueryMap data: HashMap<String, String>): Call<GenericName>

    @GET("body-parts")
    fun getBodyPartCategory(@QueryMap data: HashMap<String, String>): Call<BodyParts>

    @GET("medicines")
    fun getMedicines(@QueryMap data: HashMap<String, String>): Call<NewAddedDrugs>

    @GET("faqs")
    fun getFaqs(@QueryMap data: HashMap<String, String>): Call<FaqsResponse>

    @POST("inquiry")
    fun sendInquiry(@Body data: HashMap<String, String>): Call<BasicResponse>

    @POST("password/change")
    fun changePassword(@Body data: HashMap<String, String>): Call<BasicResponse>

    @GET("questions")
    fun getQuestions(): Call<QuestionResponse>

    @POST("faq")
    fun sendAskQuestion(@Body data: HashMap<String, String>): Call<BasicResponse>

    @GET("body-part/symptoms")
    fun getbodypartSymptom(@QueryMap data: HashMap<String, String>): Call<BodyPartSymptom>

    @GET("medicine/{medicine_id}")
    fun getMedicineDetails(
        @Path("medicine_id") path: Int,
        @Query("language_id") data: Int
    ): Call<MedicineDetailsResponse>

    @Multipart
    @POST("upload-do")
    fun uploadImage(
        @Part file: MultipartBody.Part,
        @Part("description") description: RequestBody
    ): Call<ImageResponse>

    @GET("logout")
    fun logout(): Call<LogOut>

    @GET("searched-medicines")
    fun getSearchedMedicines(@QueryMap data: HashMap<String, String>): Call<SearchedMedicineResponse>

    @GET("home-screen-images")
    fun getHomeImages(): Call<HomeImageResp>

    @GET("designation/categories")
    fun getDesignationCategory(): Call<DesignationCategoryResponse>

    @GET("static-content")
    fun getReferenceAndAcknowledgement(@Query("name") data: String): Call<AcknowledgementResponse>

    @GET("medicines_all")
    fun getAllData(): Call<List<AllData>>

    @GET("sync-db")
    fun getSyncData(): Call<ArrayList<SyncDbData>>

    @POST("medicine/{medicine_id}")
    fun postForHistory(
        @Path("medicine_id") id: Int
    ): Call<Message>

    @PUT("history/status")
    fun setHistoryStatus(@Body data: HashMap<String, Int>): Call<BasicResponse>

    @GET("dynamic/content")
    fun getDynamicData(@Query("name") data: String): Call<AcknowledgementResponse>
}