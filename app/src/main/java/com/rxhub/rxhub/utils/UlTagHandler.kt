package com.rxhub.rxhub.utils

import android.text.Editable
import android.text.Html.TagHandler
import org.xml.sax.XMLReader


class UlTagHandler : TagHandler {
    var first = true
    var parent: String? = null
    var index = 1
    override fun handleTag(
        opening: Boolean, tag: String,
        output: Editable, xmlReader: XMLReader
    ) {
        if (tag == "ul") {
            parent = "ul"
            index = 1
        } else if (tag == "ol") {
            parent = "ol"
            index = 1
        }
        if (tag == "li") {
            var lastChar = 0.toChar()
            if (output.isNotEmpty()) {
                lastChar = output[output.length - 1]
            }
            if (parent == "ul") {
                first = if (first) {
                    if (lastChar == '\n') {
                        output.append("\t•  ")
                    } else {
                        output.append("\n\t•  ")
                    }
                    false
                } else {
                    true
                }
            } else {
                if (first) {
                    if (lastChar == '\n') {
                        output.append("\t$index. ")
                    } else {
                        output.append("\n\t$index. ")
                    }
                    first = false
                    index++
                } else {
                    first = true
                }
            }
        }
    }
}