package com.rxhub.rxhub.utils

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.SplashActivity
import com.rxhub.rxhub.webServices.RetrofitClient
import com.rxhub.rxhub.webServices.model.ErrorBody
import okhttp3.ResponseBody
import java.text.SimpleDateFormat
import java.util.*


/*
* This class is have all common methods that is used in this app
*
* created by GTB on 14/04/2020
*/
class CommonMethods {
    companion object {
        lateinit var listPopupWindow: ListPopupWindow

        fun addFragmentToSplashScreen(fragmentManager: FragmentManager, fragment: Fragment) {
            fragmentManager.beginTransaction()
                .add(R.id.splashScreenContainer, fragment)
                .commit()
        }

        fun replaceFragmentInSplashScreen(fragmentManager: FragmentManager, fragment: Fragment) {
            fragmentManager.beginTransaction()
                .replace(R.id.splashScreenContainer, fragment)
                .commit()
        }

        fun replaceFragmentInSplashScreenWithBack(
            fragmentManager: FragmentManager,
            fragment: Fragment
        ) {
            fragmentManager.beginTransaction()
                .replace(R.id.splashScreenContainer, fragment)
                .addToBackStack(null)
                .commit()
        }

        fun toast(context: Context, message: String) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }

        fun addFragmentToReferenceContainer(fragmentManager: FragmentManager, fragment: Fragment) {
            fragmentManager!!.beginTransaction()
                .add(R.id.referenceContainer, fragment)
                .commit()
        }

        fun replaceToReferenceContainer(fragmentManager: FragmentManager, fragment: Fragment) {
            fragmentManager.beginTransaction()
                .replace(R.id.referenceContainer, fragment)
                .addToBackStack(null)
                .commit()
        }

        fun dropDownList(context: Activity, list: List<String>, textView: TextView) {
            listPopupWindow = ListPopupWindow(context)
            listPopupWindow.setAdapter(
                ArrayAdapter(
                    context,
                    android.R.layout.simple_dropdown_item_1line,
                    list
                )
            )
            listPopupWindow.anchorView = textView
            listPopupWindow.isModal = true
            listPopupWindow.setOnItemClickListener { parent, view, position, id ->
                textView.setText(list[position])
                listPopupWindow.dismiss()
            }
        }

        fun showDropDownList() {
            listPopupWindow.show()
        }

        fun addFragmentToSetting(fragmentManager: FragmentManager, fragment: Fragment) {
            fragmentManager.beginTransaction()
                .add(R.id.settingContainer, fragment)
                .commit()
        }

        fun replaceFragmentInSetting(fragmentManager: FragmentManager, fragment: Fragment) {
            fragmentManager.beginTransaction()
                .replace(R.id.settingContainer, fragment)
                .addToBackStack(null)
                .commit()
        }

        fun underDevelopToast(context: Activity) {
            Toast.makeText(context, "Under Development", Toast.LENGTH_SHORT).show()
        }

        fun addFragmentInDrugsDetail(fragment: Fragment, fragmentManager: FragmentManager) {
            fragmentManager.beginTransaction()
                .add(R.id.drugsContainer, fragment)
                .commit()
        }

        fun replaceFragmentInDrugsDetail(fragmentManager: FragmentManager, fragment: Fragment) {
            fragmentManager.beginTransaction()
                .replace(R.id.drugsContainer, fragment)
                .addToBackStack(null)
                .commit()
        }

        fun replaceFragmentNavigationViewStack(
            fragment: Fragment,
            fragmentManager: FragmentManager
        ) {
            fragmentManager.beginTransaction()
                .replace(R.id.coordinatorLayout, fragment)
                .addToBackStack(null)
                .commit()
        }

        //method to hide the KeyBoard in activity
        fun hideKeyboard(activity: Activity) {
            val view = activity.currentFocus
            if (view != null) {
                val inputMethodManager =
                    activity.getSystemService(android.content.Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                inputMethodManager?.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }

        //this method return the server error messages
        fun getErrorMessage(responseBody: ResponseBody): String? {
            val errorConverter = RetrofitClient.getInstence()
                .responseBodyConverter<ErrorBody>(
                    ErrorBody::class.java,
                    arrayOfNulls<Annotation>(0)
                )
            val error = errorConverter.convert(responseBody)
            return error!!.error_description
        }

        //this method return the server error messages
        fun getErrorMessageAndSessionExpire(context: Activity, responseBody: ResponseBody) {
            val errorConverter = RetrofitClient.getInstence()
                .responseBodyConverter<ErrorBody>(
                    ErrorBody::class.java,
                    arrayOfNulls<Annotation>(0)
                )
            val error = errorConverter.convert(responseBody)

//            return error!!.message
            when {
                error!!.error_description == "Login Required" -> {
                    toast(context, context.getString(R.string.session_expired))
                    Handler().postDelayed({
                        ApplicationGlobal.preferenceManager.logOut()
                        val intent = Intent(context, SplashActivity::class.java)
                            .putExtra("Logout", "logout")
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(intent)
                        context.finish()
                    }, 1000)
                }
                error!!.error_description == "Your Account has been blocked!" -> {
                    toast(context, "" + error.error_description)
                    Handler().postDelayed({
                        ApplicationGlobal.preferenceManager.logOut()
                        val intent = Intent(context, SplashActivity::class.java)
                            .putExtra("Logout", "logout")
                        intent.flags =
                            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                        context.startActivity(intent)
                        context.finish()
                    }, 1000)
                }
                else -> {
                    toast(context, "" + error!!.error_description)
                }
            }
        }

        //logOut method for Gmail Login
        fun googleLogout(context: Context) {
            val googleSignInOptions =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build()
            val googleSignInClient = GoogleSignIn.getClient(context, googleSignInOptions)
            googleSignInClient.signOut()
        }


        private var progress: ProgressDialog? = null
        fun showLoadingDialog(context: Activity) {
            if (progress == null) {
                progress = ProgressDialog(context)
                progress!!.setMessage(context.getString(R.string.pleasewaitwhileloading))
                progress!!.setCancelable(false)
            }
            progress!!.show()
        }

        fun dismissLoadingDialog() {
            if (progress != null && progress!!.isShowing()) {
                progress!!.dismiss()
            }
        }

        //this method for Phone Number is Valid or Not
        fun isValidPhone(phone: String): Boolean {
            if (TextUtils.isEmpty(phone)) {
                return false
            } else {
                return Patterns.PHONE.matcher(phone).matches()
            }
        }


        //this is used to show progress bar when getting data from server
        var dialog: Dialog? = null
        fun showProgress(activity: Context) {
            if (dialog != null) {
                if (dialog!!.isShowing) {
                    return
                }
            }
            try {
                dialog = Dialog(activity)
                dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
                dialog!!.setContentView(R.layout.item_progress_bar)
                dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//                val wmlp: WindowManager.LayoutParams = dialog!!.getWindow()!!.getAttributes()
//                wmlp.gravity = Gravity.CENTER or Gravity.CENTER
//                wmlp.width = ViewGroup.LayoutParams.MATCH_PARENT
//                wmlp.height = ViewGroup.LayoutParams.MATCH_PARENT
                dialog!!.setCanceledOnTouchOutside(false)
                dialog!!.setCancelable(false)
                dialog!!.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        //this is used to dismiss running Progress Bar
        fun dismissProgress() {
            try {
                if (dialog != null) {
                    if (dialog!!.isShowing) {
                        dialog!!.dismiss()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun getGreetingText(): String {
            val c: Calendar = Calendar.getInstance()
            val timeOfDay: Int = c.get(Calendar.HOUR_OF_DAY)
            if (timeOfDay >= 0 && timeOfDay < 12) {
                return "Good Morning,"
            } else if (timeOfDay >= 12 && timeOfDay < 16) {
                return "Good Afternoon,"
            } else if (timeOfDay >= 16 && timeOfDay < 21) {
                return "Good Evening,"
            } else {
                return "Good Night,"
//             if (timeOfDay >= 21 && timeOfDay < 24)
            }
        }

        //this method return the 0 to 100 ArrayList
        fun ageTextList(): ArrayList<String> {
            val ageList = ArrayList<String>()
            for (i in 0..100) {
                ageList.add(i.toString())
            }
            return ageList
        }

        //this method for show image from URL
        fun setUrlImage(context: Context, url: String, imageView: ImageView) {
            GlideApp.with(context)
                .load(url)
                .circleCrop()
                .into(imageView)
        }

        //this method for show image from server
        fun setServerImage(context: Context, path: String, imageView: ImageView) {
            GlideApp.with(context)
                .load(
                    Constant.IMAGE_URL_MEDIUM
                            + path
                )
                .circleCrop()
                .into(imageView)
        }

        fun setHomeImageDrug(context: Context, path: String, imageView: ImageView) {
            GlideApp.with(context)
                .load(Constant.IMAGE_URL_MEDIUM + path)
                .centerCrop()
                .into(imageView)
        }

        fun handelInternet(activity: Context, t: String) {
            //mydoti.com/18.203.60.110:8002
            //Failed to connect to /mydoti.com:8002
//            "Unable to resolve host \"167.71.197.68:3000\": No address associated with hostname"
//            "Unable to resolve host \"Failed to connect to /167.71.197.68\": No address associated with hostname"
            if (t == "Failed to connect to \"167.71.197.68\": No address associated with hostname") {
                Toast.makeText(activity, "Internet connection not available", Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(activity, t, Toast.LENGTH_SHORT).show()
            }
        }

        //this method return the phone is Connected with internet or Not
        fun phoneIsOnline(context: Context): Boolean {
            val cm =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val netInfo = cm.activeNetworkInfo
            return if (netInfo != null && netInfo.isConnectedOrConnecting) {
                true
            } else {
                false
            }
        }

        //this method convert UTC to local time stamp
        fun getLocalTimeFromUTC(ourDate: String): String? {
            var ourDate: String? = ourDate
            val formatter =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            formatter.timeZone = TimeZone.getTimeZone("UTC")
            val value = formatter.parse(ourDate)
            val dateFormatter =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'") //this format changeable
            dateFormatter.timeZone = TimeZone.getDefault()
            ourDate = dateFormatter.format(value)
            return ourDate
        }

        //this method return the datem i.e.(01/05/2020)
        fun getDateAndTime(zone: String): String {
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            val date =
                dateFormat.parse(zone)!!
            val formatter =
                SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val dateStr: String = formatter.format(date)// for complete date with time
            val day =
                SimpleDateFormat("EE", Locale.ENGLISH).format(date.time)// for day like mon ,tue etc
            val month = SimpleDateFormat(
                "MMM",
                Locale.ENGLISH
            ).format(date.time)// for month lik jan ,feb etc
            val dates =
                SimpleDateFormat("dd", Locale.ENGLISH).format(date.time)// for date like 01,02,03
            val mon = SimpleDateFormat(
                "MM",
                Locale.ENGLISH
            ).format(date.getTime())// for month in like 01,02,03
            val year = SimpleDateFormat("YYYY", Locale.ENGLISH).format(date.time)
            val time = SimpleDateFormat("hh:mm aa", Locale.ENGLISH).format(date.time)
            //System.out.println(" C DATE is  "+dates+" "+day+" "+month+" "+mon)
            return "" + dates + "/" + mon + "/" + year
        }

        //manually logout method if session is expired
        fun sessionExpire(context: Activity, msg: String?) {
            if (msg == "Login Required") {
                toast(context, context.getString(R.string.session_expired))
                Handler().postDelayed({
                    ApplicationGlobal.preferenceManager.logOut()
                    val intent = Intent(context, SplashActivity::class.java)
                        .putExtra("Logout", "logout")
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                    context.startActivity(intent)
                    context.finish()
                }, 1000)
            } else {
                toast(context, msg.toString())
                Log.i("Authorized", "" + msg)
            }
        }

        //this method for avoid Double Click , in forgot password.
        fun avoidDoubleClicks(view: View) {
            if (!view.isClickable) {
                return
            }
            view.isClickable = false
            view.postDelayed({ view.isClickable = true }, 2000)
        }
         val ALLOWED_CHARACTERS = "0123456789qwertyuiopasdfghjklzxcvbnm"
         fun getRandomString(sizeOfRandomString: Int): String {
            val random = Random()
            val sb = StringBuilder(sizeOfRandomString)
            for (i in 0 until sizeOfRandomString)
                sb.append(ALLOWED_CHARACTERS[random.nextInt(ALLOWED_CHARACTERS.length)])
            return sb.toString()
        }
    }

}