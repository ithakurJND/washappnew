package com.rxhub.rxhub.utils

import android.content.Context
import android.content.SharedPreferences

class PreferenceManager {

    private var sharedPreferences: SharedPreferences
    private var editor: SharedPreferences.Editor
    val insertefile = "Insert"
    var context: Context

    constructor(context: Context) {
        this.context = context
        sharedPreferences = context.getSharedPreferences(insertefile, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
    }

    fun setSignUpResp(data: String) {
        editor.putString("SignUp", data)
        editor.apply()
    }

    fun getSignUpResp(): String? {
        return sharedPreferences.getString("SignUp", "")
    }

    fun setSessionId(sessionid: String) {
        editor.putString("access_token", sessionid)
        editor.apply()
    }

    fun getSessionId(): String? {
        return sharedPreferences.getString("access_token", "")
    }

    fun logOut() {
        editor.clear()
        editor.commit()
    }

    fun setFCMID(id: String) {
        editor.putString("fcmId", id)
        editor.commit()
    }

    fun getFCMID(): String? {
        return sharedPreferences.getString("fcmId", "")
    }

    fun setBasicDetails(userId: String) {
        editor.putString("UserId", userId)
        editor.commit()
    }

    fun getBasicDetails(): String? {
        return sharedPreferences.getString("UserId", "")
    }

    fun setDrugsId(string: String) {
        editor.putString("MedicineId", string)
        editor.commit()
    }

    fun getDrugsId(): String? {
        return sharedPreferences.getString("MedicineId", "")
    }

    fun setDrugsName(string: String) {
        editor.putString("DrugsName", string)
        editor.commit()
    }

    fun getDrugsName(): String? {
        return sharedPreferences.getString("DrugsName", "")
    }

    fun setAlphabet(string: String) {
        editor.putString("Alphabet", string)
        editor.commit()
    }

    fun getAlphapet(): String? {
        return sharedPreferences.getString("Alphabet", "")
    }

    fun setEnglishSelected(boolean: Boolean, context: Context) {
        editor.putBoolean("English", boolean)
        editor.commit()
    }

    fun isEnglishSelected(): Boolean {
        return sharedPreferences.getBoolean("English", true)
    }

    fun setLanguageChanged(boolean: Boolean) {
        editor.putBoolean("LanguageChange", boolean)
            .commit()
    }

    fun getLanguageChanged(): Boolean {
        return sharedPreferences.getBoolean("LanguageChange", false)
    }

    fun setMedicineId(id: String) {
        editor.putString("MedId", id)
        editor.commit()
    }

    fun getMedicineId(): String? {
        return sharedPreferences.getString("MedId", "")
    }

    fun setHomeSearchText(string: String) {
        editor.putString("HomeSearch", string)
        editor.commit()
    }

    fun getHomeSearchText(): String? {
        return sharedPreferences.getString("HomeSearch", "")
    }

    fun setLanguageId(id: Int) {
        editor.putInt("LanguageId", id)
    }

    fun getLanguageId(): Int {
        return sharedPreferences.getInt("LanguageId", Constant.ENGLISH)
    }

    fun saveBannerImages(images: String) {
        editor.putString("BannerImages", images)
    }

    fun getBannerImages(): String? {
        return sharedPreferences.getString("BannerImages", "")
    }

    fun saveDbTimes(data: String) {
        editor.putString("Times", data)
    }

    fun getDbTimes(): String? {
        return sharedPreferences.getString("Times", "")
    }
}