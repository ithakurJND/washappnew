package com.rxhub.rxhub.utils

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.rxhub.rxhub.R
import com.rxhub.rxhub.activities.MainActivity
import kotlin.random.Random

/*
* This class is used to show push notification(server) in the phone. with the help of
* FireBase Messaging Service
*
* created by GTB on 06/05/2020
*/
@SuppressLint("MissingFirebaseInstanceTokenRefresh")
class MyFirebaseMessagingService : FirebaseMessagingService() {
    lateinit var messageText: String
    lateinit var title: String
    lateinit var notificationIntent: Intent

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.i(
            "pushNotification", Gson().toJson(remoteMessage.data)
        )
        showNotification(remoteMessage)
    }

    //    subject: subject,
    //    message: msg,
    //    type:'admin_notification'
    fun showNotification(remoteMessage: RemoteMessage) {
        //(remoteMessage.data["type"] == "admin_notification")
        messageText = remoteMessage.data["message"]!!
        title = remoteMessage.data["subject"]!!

        notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent =
            PendingIntent.getActivity(
                this,
                0,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )

        val notificationBuilder = NotificationCompat.Builder(this, "channel_id")
            .setContentTitle(title)
            .setContentText(messageText)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setStyle(NotificationCompat.BigTextStyle())
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setLargeIcon(
                BitmapFactory.decodeResource(
                    this.resources,
                    R.drawable.ic_stat_notification_icon
                )
            )
            .setSmallIcon(R.drawable.ic_stat_notification_icon)
            .setColor(ContextCompat.getColor(this, R.color.colorGreen))
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(getString(R.string.app_name))
        }
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(
                getString(R.string.app_name),
                getString(R.string.app_name),
                importance
            )
            mChannel.enableLights(true)
            mChannel.enableVibration(true)
            mChannel.sound
            notificationManager.createNotificationChannel(mChannel)
        }
        val notificationId = Random.nextInt()
        notificationManager.notify(notificationId, notificationBuilder.build())
    }
}