package com.rxhub.rxhub.utils

class Constant {
    companion object {
        //Live
       // const val BASE_URL = "https://app.rxhub.io:3000/api/"
        //Staging
       const val BASE_URL = "http://167.99.244.219:3002/api/"

        //  "https://app.rxhub.io:3002/api/"
        //        "https://app.rxhub.io:3000/api-docs/"
        const val IMAGE_URL_MEDIUM =
            "https://sgp1.digitaloceanspaces.com/rxhubspace/Uploads/Images/Medium/"
        const val IMAGE_URL_SMALL =
            "https://sgp1.digitaloceanspaces.com/rxhubspace/Uploads/Images/Small/"
        const val NEW_IMAGE_URL = "https://sgp1.digitaloceanspaces.com/rxhubspace/Uploads/Images/"

        //1-English, //2-Arabic, //3-Urdu, //4-Hindi
        const val ENGLISH = 1
        const val ARABIC = 2
        const val URDU = 3
        const val HINDI = 4

        //1 = ANDROID, //0 = IOS
        val DEVICE_TYPE = 1
        val LANGUAGE_ID = "language_id"
        val PAGE_COUNT = "page_count"
        const val OPEN_CAMERA = 2
        const val SELECT_IMAGE = 1

        //1-Female, // 2-Male
        val GENDER_LIST = arrayListOf("Male", "Female")
        const val MALE = 2
        const val FEMALE = 1
        const val TOTAL_DATA = 20

        //        if set 1 will get new medicines Listing
        const val NEW_ADDED_DRUGS = 1

        //        0 = pe active, 1 = pe inactive USER
        const val ACTIVE = 0

        const val REFERENCE = "reference"
        const val ACKNOWLEDGEMENT = "acknowledgement"
        const val ABOUT = "about"
        const val PRIVACY = "privacy_and_policy"
        const val TERMS = "terms_and_conditions"
        const val WORK = "how_it_works"
        const val TERMS_OF_USE = "terms_of_use"
        const val DISCLAIMER = "disclaimer"
        const val INSTRUCTIONS = "general_instructions"
    }
}