package com.rxhub.rxhub.utils

import android.app.Application
import io.branch.referral.Branch
import io.realm.Realm
import io.realm.RealmConfiguration

class ApplicationGlobal : Application() {
    companion object {
        lateinit var preferenceManager: PreferenceManager
        lateinit var sessionId: String

        //1-English
        //2-Arabic
        //3-Urdu
        //4-Hindi
        var english = Constant.ENGLISH
        var isLanguageChanged = false
        var hitAllApis = false
        var profile_pic = ""
        var isProfileUpdated = false
    }

    override fun onCreate() {
        super.onCreate()
        preferenceManager = PreferenceManager(this)
        sessionId = preferenceManager.getSessionId()!!

        // Branch object initialization
        Branch.getAutoInstance(this)

        //Realm initialization
        Realm.init(this)
        val config = RealmConfiguration.Builder()
            .name("realmOne")
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(config)
    }

//    override fun attachBaseContext(base: Context?) {
//        preferenceManager = PreferenceManager(base!!)
//        if (!preferenceManager.isEnglishSelected())
//            super.attachBaseContext(Localization.applyLanguage(base, "ar_AE"))
//        else
//            super.attachBaseContext(base)
//    }
}